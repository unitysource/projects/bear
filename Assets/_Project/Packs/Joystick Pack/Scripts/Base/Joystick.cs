﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services.Input;
using _Project.Scripts.Infrastructure;
using _Project.Scripts.Services.InputStuff;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class Joystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IJoystick
{
    public float Horizontal => (snapX) ? SnapFloat(input.x, AxisOptions.Horizontal) : input.x;

    public float Vertical => (snapY) ? SnapFloat(input.y, AxisOptions.Vertical) : input.y;

    public Vector2 Direction => new Vector2(Horizontal, Vertical);
    
    public bool CanUse { get; set; }

    private float HandleRange
    {
        set => handleRange = Mathf.Abs(value);
    }

    private float DeadZone
    {
        set => deadZone = Mathf.Abs(value);
    }

    public AxisOptions AxisOptions
    {
        get => AxisOptions;
        set => axisOptions = value;
    }

    public bool SnapX
    {
        set => snapX = value;
    }

    public bool SnapY
    {
        set => snapY = value;
    }

    [SerializeField] private float handleRange = 1;
    [SerializeField] private float deadZone = 0;
    [SerializeField] private AxisOptions axisOptions = AxisOptions.Both;
    [SerializeField] private bool snapX = false;
    [SerializeField] private bool snapY = false;

    [SerializeField] protected RectTransform background = null;
    [SerializeField] private RectTransform handle = null;
    private RectTransform baseRect = null;

    private Canvas canvas;
    private Camera cam;

    private Vector2 input = Vector2.zero;
    private GameObject _joystickBG;
    private InputBehaviour _inputBehaviour;

    [Inject]
    private void Construct(InputBehaviour inputBehaviour)
    {
        _inputBehaviour = inputBehaviour;
        _joystickBG = transform.GetChild(0).gameObject;

        _inputBehaviour.ObjectMouseClick += OnAnyClickedCorrectMaskObject;
    }

    private void OnAnyClickedCorrectMaskObject(GameObject obj)
    {
        _joystickBG.SetActive(false);
    }

    protected virtual void Start()
    {
        HandleRange = handleRange;
        DeadZone = deadZone;
        baseRect = GetComponent<RectTransform>();
        canvas = GetComponentInParent<Canvas>();
        if (canvas == null)
            Debug.LogError("The Joystick is not placed inside a canvas");

        Vector2 center = new Vector2(0.5f, 0.5f);
        background.pivot = center;
        handle.anchorMin = center;
        handle.anchorMax = center;
        handle.pivot = center;
        handle.anchoredPosition = Vector2.zero;
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!CanUse)
        {
            input = Vector2.zero;
            handle.anchoredPosition = Vector2.zero;
            return;
        }
        
        cam = null;
        if (canvas.renderMode == RenderMode.ScreenSpaceCamera)
            cam = canvas.worldCamera;

        Vector2 position = RectTransformUtility.WorldToScreenPoint(cam, background.position);
        Vector2 radius = background.sizeDelta / 2;
        input = (eventData.position - position) / (radius * canvas.scaleFactor);
        FormatInput();
        HandleInput(input.magnitude, input.normalized, radius, cam);
        handle.anchoredPosition = input * radius * handleRange;
    }

    protected virtual void HandleInput(float magnitude, Vector2 normalised, Vector2 radius, Camera cam)
    {
        if (magnitude > deadZone)
        {
            if (magnitude > 1)
                input = normalised;
        }
        else
            input = Vector2.zero;
    }

    private void FormatInput()
    {
        input = axisOptions switch
        {
            AxisOptions.Horizontal => new Vector2(input.x, 0f),
            AxisOptions.Vertical => new Vector2(0f, input.y),
            _ => input
        };
    }

    private float SnapFloat(float value, AxisOptions snapAxis)
    {
        if (value == 0)
            return value;

        if (axisOptions == AxisOptions.Both)
        {
            float angle = Vector2.Angle(input, Vector2.up);
            if (snapAxis == AxisOptions.Horizontal)
            {
                if (angle < 22.5f || angle > 157.5f)
                    return 0;
                return (value > 0) ? 1 : -1;
            }

            if (snapAxis == AxisOptions.Vertical)
            {
                if (angle > 67.5f && angle < 112.5f)
                    return 0;

                return value > 0 ? 1 : -1;
            }

            return value;
        }

        if (value > 0)
            return 1;
        if (value < 0)
            return -1;
        return 0;
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        input = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;
    }

    protected Vector2 ScreenPointToAnchoredPosition(Vector2 screenPosition)
    {
        Vector2 localPoint = Vector2.zero;
        
        cam = null;
        if (canvas.renderMode == RenderMode.ScreenSpaceCamera)
            cam = canvas.worldCamera;
        
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(baseRect, screenPosition, cam, out localPoint))
        {
            Vector2 pivotOffset = baseRect.pivot * baseRect.sizeDelta;
            return localPoint - (background.anchorMax * baseRect.sizeDelta) + pivotOffset;
        }

        return Vector2.zero;
    }

    public Vector3 GetInput() => new Vector3(Direction.x, 0, Direction.y);

    public bool IsInputAboveZero() => Direction.magnitude > 0.01f;
}

public enum AxisOptions
{
    Both,
    Horizontal,
    Vertical
}