﻿using Assets._Project.Scripts.Utilities.Constants;
using DG.Tweening;
using UnityEngine;

namespace _Project.Packs.HexPlanet.Scripts
{
    public class TileModel : MonoBehaviour
    {
        [HideInInspector] public Collider Collider;
        [HideInInspector] public MeshRenderer MeshRenderer;
        [HideInInspector] public MeshFilter MeshFilter;

        private const float _activateDuration = 0.2f;

        public void Activate(bool smooth)
        {
            gameObject.layer = LayerMask.NameToLayer(Layers.Ground);
            DOVirtual.DelayedCall(smooth ? 0.3f : 0, InstantiateAnimation);
        }

        public void Deactivate()
        {
            gameObject.layer = LayerMask.NameToLayer(Layers.Default);
            gameObject.SetActive(false);
        }


        private void InstantiateAnimation()
        {
            gameObject.SetActive(true);
            transform.DOScale(1f, _activateDuration).From(0f).SetEase(Ease.OutQuad).Play();
        }

        public void ReInit() => MeshFilter = GetComponent<MeshFilter>();
    }
}