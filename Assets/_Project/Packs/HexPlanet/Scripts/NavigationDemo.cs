﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Packs.HexPlanet.Scripts;
using UnityEngine;

public class NavigationDemo : MonoBehaviour
{
    public MobileUnit Unit;
    [SerializeField] private int _tileID;

    // Use this for initialization
	void Start ()
    {
        Tile.OnTileClickedAction += OnTileClicked;
	}

    public void OnTileClicked(Tile tile)
    {
        if(!Unit.moving)
        {
            if(Hexsphere.planetInstances[0].navManager.findPath(Unit.currentTile, tile, out var path))
                Unit.moveOnPath(path);
        }
    }
}
