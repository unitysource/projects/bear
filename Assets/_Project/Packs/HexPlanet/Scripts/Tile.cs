﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts;
using _Project.Scripts.Services.InputStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using Assets._Project.Scripts.Utilities.Extensions;
using UnityEngine;
using Zenject;

namespace _Project.Packs.HexPlanet.Scripts
{
    public enum TileDisplayOptions
    {
        None,
        GroupID,
        NavWeight,
        Navigable
    }

    [Serializable, SelectionBase]
    public class Tile : MonoBehaviour, IEquatable<Tile>
    {
        public static float planetScale;
        private static int ID;

        public static Action<Tile> OnTileClickedAction;

        [Tooltip("The instance of the hexsphere which constructed this tile")]
        public Hexsphere parentPlanet;

        [ReadOnly] public int id;

        public List<Tile> neighborTiles;

        //Tile Attributes
        [HideInInspector, Tooltip("Whether or not navigation will consider this tile as a valid to move over")]
        public bool navigable = true;

        [HideInInspector, Tooltip(
             "The cost of moving across this tile in terms of pathfinding weight.  Pathfinding will prioritize the lowest cost path.")]
        [Range(1, 100)]
        public int pathCost = 1;

        // The center of this tile when initially generated.  Does not account for extrusion.
        public Vector3 center => tileRenderer.bounds.center;

        // The current center of the tiles face accounting for extrusion.
        public Vector3 FaceCenter
        {
            get
            {
                float heightMult = IsInverted ? -1f : 1f;
                var cashedTransform = transform;
                return cashedTransform.position +
                       cashedTransform.up * (ExtrudedHeight * heightMult * parentPlanet.planetScale);
            }
        }

        //The position of this tile as reported by the renderer in world space.  More strict than the above center.
        public Vector3 centerRenderer => tileRenderer.bounds.center;

        public TileModel TileModel { get; private set; }

        [HideInInspector] public int GroupID;

        [HideInInspector] public Renderer tileRenderer;

        [HideInInspector] public float ExtrudedHeight;

        [HideInInspector] public bool isHexagon;

        [HideInInspector] public bool IsInverted;

        public List<GameObject> PlacedObjects = new List<GameObject>();

        [HideInInspector] public TileDisplayOptions InfoDisplayOption;

        //Used to specify which tile is currently selected so that any tile can query the selected tile or assign themselves as selected.
        private static Tile selectedTile;

        //The center of the tile in worldspace as assigned by the hexsphere during generation.  Not affected by the scale of the planet.
        [SerializeField, HideInInspector] private bool hasBeenExtruded;

        [SerializeField, HideInInspector] private Material TileMaterial;

        private Color HilightColor = new Color(0.2f, 0.2f, 0.2f, 0.5f);

        private TilesMeshSaveLogic _tilesMeshSaveLogic;

        public float _absoluteHeight;
        [HideInInspector] public float _exHeight;

        private void Awake() => SetModel();

        private void OnValidate()
        {
        }

        public void SetTilesMeshSaveLogic(TilesMeshSaveLogic tilesMeshSaveLogic) =>
            _tilesMeshSaveLogic = tilesMeshSaveLogic;

        public void Initialize()
        {
            SetModel();
            id = ID;
            ID++;

            gameObject.name = $"Tile [{id.ToString()}]";
            TileModel.name = "Tile Child";
        }

        private void SetModel()
        {
            TileModel = GetComponentInChildren<TileModel>();
            tileRenderer = TileModel.MeshRenderer;
        }

        public Vector2 GetCoordinates()
        {
            Vector2 latLong = Vector2.zero;
            if (parentPlanet)
            {
                var transform1 = parentPlanet.transform;
                Vector3 ToTile = transform.position - transform1.position;
                float Latitude = 90.0f - Vector3.Angle(ToTile, transform1.up);
                Vector3 ToTileHoriz = Vector3.ProjectOnPlane(ToTile, parentPlanet.transform.up);
                var transform2 = parentPlanet.transform;
                float Longitude =
                    Vector3.SignedAngle(ToTileHoriz, transform2.forward, transform2.up);
                latLong.x = Latitude;
                latLong.y = Longitude;
            }

            return latLong;
        }

        void OnMouseEnter()
        {
            Pointer.instance.setPointer(PointerStatus.TILE, FaceCenter, transform.up);
        }

        void OnMouseExit()
        {
            Pointer.instance.unsetPointer();
        }

        /*
        private InputBehaviour _inputBehaviour;

        [Inject]
        private void Construct(InputBehaviour inputBehaviour)
        {
            _inputBehaviour = inputBehaviour;
            _inputBehaviour.ObjectMouseClick += OnTileClicked;
        }

        private void OnDestroy()
        {
            _inputBehaviour.ObjectMouseClick -= OnTileClicked;
        }

        private void OnTileClicked(GameObject clickedObj)
        {
            if (clickedObj.transform.parent.gameObject.GetHashCode() != gameObject.GetHashCode()) return;
            if (CommonTools.IsPointerOverUIObjectIgnoreJoystick()) return;

            //Demo function
            pathfindingDrawDemo();
            if (OnTileClickedAction != null)
            {
                OnTileClickedAction.Invoke(this);
            }
        }
        */

        /// <summary>
        /// Just a simple demo function that allows you to click on two tiles and draw the shortest path between them.
        /// </summary>
        public void pathfindingDrawDemo()
        {
            if (selectedTile == null)
            {
                selectedTile = this;
            }
            else if (selectedTile != this)
            {
                Stack<Tile> path = new Stack<Tile>();
                if (parentPlanet.navManager.findPath(selectedTile, this, out path))
                {
                    parentPlanet.navManager.drawPath(path);
                    selectedTile = null;
                }
            }
        }

        public void PlaceObject(GameObject obj)
        {
            if (TileModel == null)
                SetModel();
            PlacePosition(obj);
            obj.transform.SetParent(TileModel.transform);
            PlacedObjects.Add(obj);
        }

        public void FakePlaceObject(GameObject obj, Vector3 offset)
        {
            if (TileModel == null)
                SetModel();
            PlacePosition(obj);
            var objTransform = obj.transform;
            objTransform.SetParent(TileModel.transform);
            objTransform.localPosition = offset * objTransform.localScale.x;
            objTransform.SetParent(null);
        }

        public void PlacePosition(GameObject obj)
        {
            obj.transform.position = FaceCenter;
            obj.transform.up = transform.up;
        }

        public void PlacePositionWithOffset(GameObject obj, Vector3 offset)
        {
            obj.transform.up = transform.up;
            obj.transform.position = FaceCenter;
        }

        public void DeleteLastPlacedObject()
        {
            if (PlacedObjects.Count > 0)
            {
                DestroyImmediate(PlacedObjects[PlacedObjects.Count - 1]);
                PlacedObjects.RemoveAt(PlacedObjects.Count - 1);
            }
        }

        public void DeletePlacedObjects()
        {
            for (int i = 0; i < PlacedObjects.Count; i++)
            {
                if (PlacedObjects[i] != null)
                {
                    DestroyImmediate(PlacedObjects[i]);
                }
            }

            PlacedObjects.Clear();
        }

        public void SetExtrusionHeight()
        {
            _exHeight = ExtrudedHeight;
            ExtrudedHeight = _absoluteHeight;

            float delta = _absoluteHeight - _exHeight;
            Extrude(delta);
        }

        public void SetExtrusionHeight(float height)
        {
            float delta = height - ExtrudedHeight;
            Extrude(delta);
        }

        public void Extrude(float heightDelta)
        {
            MeshTileProperties meshTileProperties = _tilesMeshSaveLogic.GetMeshTilePropertiesByTileID(id);

            // ExtrudedHeight += heightDelta;

            TileModel.ReInit();

            Mesh mesh = TileModel.MeshFilter.sharedMesh;
            Vector3[] verts = mesh.vertices;

            MeshCollider meshCollider = TileModel.gameObject.GetComponent<MeshCollider>();
            MeshCollider mCollider = meshCollider;

            Transform tileModelTransform = TileModel.transform;

            //Check if this tile has already been extruded
            if (hasBeenExtruded)
            {
                int sides = isHexagon ? 6 : 5;
                //Apply extrusion heights
                for (int i = 0; i < sides; i++)
                {
                    var position = parentPlanet.transform.position;
                    Vector3 worldV = (tileModelTransform.TransformPoint(verts[i]) - position);
                    worldV += heightDelta * worldV.normalized * parentPlanet.planetScale;
                    verts[i] = tileModelTransform.InverseTransformPoint(worldV + position);
                }

                for (int i = sides + 2; i < sides + sides * 4; i += 4)
                {
                    var position = parentPlanet.transform.position;
                    Vector3 worldV = (tileModelTransform.TransformPoint(verts[i]) - position);
                    worldV += heightDelta * worldV.normalized * parentPlanet.planetScale;
                    verts[i] = tileModelTransform.InverseTransformPoint(worldV + position);

                    worldV = (tileModelTransform.TransformPoint(verts[i + 1]) - position);
                    worldV += heightDelta * worldV.normalized * parentPlanet.planetScale;
                    verts[i + 1] = tileModelTransform.InverseTransformPoint(worldV + position);
                }

                mesh.vertices = verts;

                // If this has a mesh collider, update the mesh
                if (mCollider != null) meshCollider.sharedMesh = mesh;

                FinishExtrude(heightDelta, mesh, meshTileProperties);
                return;
            }

            //Sort vertices clockwise
            Array.Sort(verts, new ClockwiseComparer(tileModelTransform.InverseTransformPoint(center)));
            List<int> tris = new List<int>(mesh.triangles);
            //List<Vector3> normals = new List<Vector3> (mesh.normals);

            //Duplicate the existing vertices
            List<Vector3> faceVerts = new List<Vector3>(verts);
            //Translate duplicated verts along local up
            for (int i = 0; i < faceVerts.Count; i++)
            {
                Transform transform1;
                Vector3 worldV = (tileModelTransform.TransformPoint(faceVerts[i]) -
                                  (transform1 = parentPlanet.transform).position);
                worldV += heightDelta * worldV.normalized * parentPlanet.planetScale;
                faceVerts[i] = tileModelTransform.InverseTransformPoint(worldV + transform1.position);
            }

            //Set triangles for extruded face
            tris[0] = 0;
            tris[1] = 1;
            tris[2] = 2;

            tris[3] = 0;
            tris[4] = 2;
            tris[5] = 3;

            tris[6] = 0;
            tris[7] = 3;
            tris[8] = 4;

            //Only set the last triangle if this is a hexagon
            if (verts.Length == 6)
            {
                tris[9] = 0;
                tris[10] = 4;
                tris[11] = 5;
            }

            int t = 0;
            //Create side triangles
            for (int i = 0; i < verts.Length - 1; i++, t += 4)
            {
                faceVerts.Add(verts[i]);
                faceVerts.Add(verts[i + 1]);

                faceVerts.Add(faceVerts[i]);
                faceVerts.Add(faceVerts[i + 1]);

                tris.Add(t + verts.Length);
                tris.Add(t + verts.Length + 1);
                tris.Add(t + verts.Length + 2);

                tris.Add(t + verts.Length + 1);
                tris.Add(t + verts.Length + 3);
                tris.Add(t + verts.Length + 2);
            }

            //Manually create last two triangles
            faceVerts.Add(verts[verts.Length - 1]);
            faceVerts.Add(verts[0]);

            faceVerts.Add(faceVerts[verts.Length - 1]);
            faceVerts.Add(faceVerts[0]);

            tris.Add(faceVerts.Count - 4);
            tris.Add(faceVerts.Count - 3);
            tris.Add(faceVerts.Count - 2);

            tris.Add(faceVerts.Count - 3);
            tris.Add(faceVerts.Count - 1);
            tris.Add(faceVerts.Count - 2);


            mesh.vertices = faceVerts.ToArray();
            mesh.triangles = tris.ToArray();
            mesh.RecalculateNormals();
            //Reassign UVs
            mesh.uv = isHexagon ? generateHexUvs() : generatePentUvs();

            // MeshCollider meshCollider = TileModel.gameObject.GetComponent<MeshCollider>();
            //Assign meshes to Mesh Collider and Mesh Filter
            if (mCollider != null)
            {
                mCollider.sharedMesh = mesh;
            }


            hasBeenExtruded = true;
            //Assign Tile Material
            SetGroupID(GroupID);

            FinishExtrude(heightDelta, mesh, meshTileProperties);
        }

        private void FinishExtrude(float heightDelta, Mesh mesh, MeshTileProperties meshTileProperties)
        {
            SaveMesh(mesh, meshTileProperties);
#if UNITY_EDITOR
            _tilesMeshSaveLogic.SaveSettings();
#endif
            UpdatePositionToPlacedObjects(heightDelta);
        }

        private void UpdatePositionToPlacedObjects(float heightDelta)
        {
            if (TileModel == null) SetModel();
            TileModel.gameObject.ActionAllChildren(tr => tr.SetLocalAxis(Axis.Y, tr.localPosition.y + heightDelta));
        }

        private void SaveMesh(Mesh mesh, MeshTileProperties meshTileProperties)
        {
            TileModel.MeshFilter.sharedMesh = mesh;

            meshTileProperties.Vertices = mesh.vertices;
            meshTileProperties.UVs = mesh.uv;
            meshTileProperties.Triangles = mesh.triangles;
        }

        public Vector2[] generateHexUvs()
        {
            Vector2[] uvs = new Vector2[30];
            uvs[0] = new Vector2(0.293f, 0.798f);
            uvs[1] = new Vector2(0.397f, 0.977f);
            uvs[2] = new Vector2(0.604f, 0.977f);
            uvs[3] = new Vector2(0.707f, 0.798f);
            uvs[4] = new Vector2(0.604f, 0.619f);
            uvs[5] = new Vector2(0.397f, 0.619f);

            float h = 6f;
            float y = 0.6f;
            for (int i = 6; i < 28; i += 4)
            {
                uvs[i] = new Vector2(h / 6f, 0f);
                uvs[i + 1] = new Vector2((h - 1) / 6f, 0f);

                uvs[i + 2] = new Vector2(h / 6f, y);
                uvs[i + 3] = new Vector2((h - 1) / 6f, y);
                h--;
            }

            return uvs;
        }

        public Vector2[] generatePentUvs()
        {
            Vector2[] uvs = new Vector2[25];
            uvs[0] = new Vector2(0.389f, 0.97f);
            uvs[1] = new Vector2(0.611f, 0.97f);
            uvs[2] = new Vector2(0.68f, 0.758f);
            uvs[3] = new Vector2(0.5f, 0.627f);
            uvs[4] = new Vector2(0.32f, 0.758f);

            float h = 5f;
            float y = 0.6f;
            for (int i = 5; i < 22; i += 4)
            {
                uvs[i] = new Vector2(h / 5f, 0f);
                uvs[i + 1] = new Vector2((h - 1) / 5f, 0f);

                uvs[i + 2] = new Vector2(h / 5f, y);
                uvs[i + 3] = new Vector2((h - 1) / 5f, y);
                h--;
            }

            return uvs;
        }

        public void SetGroupID(int groupId)
        {
            GroupID = groupId;

            if (isHexagon)
            {
                if (hasBeenExtruded && GroupID < parentPlanet.GroupMaterials_HexExtruded.Length)
                {
                    tileRenderer.sharedMaterial = parentPlanet.GroupMaterials_HexExtruded[GroupID];
                    TileMaterial = tileRenderer.sharedMaterial;
                }
                else if (!hasBeenExtruded && GroupID < parentPlanet.GroupMaterials_Hex.Length)
                {
                    tileRenderer.sharedMaterial = parentPlanet.GroupMaterials_Hex[GroupID];
                    TileMaterial = tileRenderer.sharedMaterial;
                }
            }
            else
            {
                if (hasBeenExtruded && GroupID < parentPlanet.GroupMaterials_PentExtruded.Length)
                {
                    tileRenderer.sharedMaterial = parentPlanet.GroupMaterials_PentExtruded[GroupID];
                    TileMaterial = tileRenderer.sharedMaterial;
                }
                else if (!hasBeenExtruded && GroupID < parentPlanet.GroupMaterials_Pent.Length)
                {
                    tileRenderer.sharedMaterial = parentPlanet.GroupMaterials_Pent[GroupID];
                    TileMaterial = tileRenderer.sharedMaterial;
                }
            }
        }

        public void SetColor(Color col)
        {
            Material tempMaterial = new Material(GetComponent<Renderer>().sharedMaterial);
            tempMaterial.color = col;
            tileRenderer.sharedMaterial = tempMaterial;
        }

        public void SetMaterial(Material mat)
        {
            TileMaterial = mat;
            tileRenderer.sharedMaterial = mat;
        }

        public void SetHighlight(bool hilighted)
        {
            if (hilighted)
            {
                Material tempMaterial = new Material(TileMaterial);
                tempMaterial.color = HilightColor + TileMaterial.color;
                tileRenderer.sharedMaterial = tempMaterial;
            }
            else
            {
                tileRenderer.sharedMaterial = TileMaterial;
            }
        }

        /// <summary>
        /// Gets all tiles reachable from this tile that share the same group id.
        /// </summary>
        /// <returns>A list of connected tiles with the same group id.</returns>
        public List<Tile> GetConnectedGroup()
        {
            List<Tile> connectedRegion = new List<Tile>();
            Stack<Tile> s = new Stack<Tile>();
            s.Push(this);

            while (s.Count > 0)
            {
                Tile t = s.Pop();

                if (!connectedRegion.Contains(t))
                {
                    connectedRegion.Add(t);

                    foreach (Tile v in t.neighborTiles)
                    {
                        if (v.GroupID == this.GroupID)
                        {
                            s.Push(v);
                        }
                    }
                }
            }

            return connectedRegion;
        }

        public int getID()
        {
            return id;
        }

        public void SaveMeshData()
        {
            MeshFilter mf = TileModel.MeshFilter;

            var sharedMesh = mf.sharedMesh;
            _tilesMeshSaveLogic.TilesMeshSaveSettings.MeshTilePropertiesList
                .Add(new MeshTileProperties(id, sharedMesh.vertices, sharedMesh.triangles, sharedMesh.uv));
        }

        public void RestoreMesh()
        {
            if (TileModel == null)
            {
                TileModel = GetComponentInChildren<TileModel>();
            }

            MeshFilter mf = TileModel.MeshFilter;
            MeshTileProperties meshTileProperties = _tilesMeshSaveLogic.GetMeshTilePropertiesByTileID(id);
            Mesh mesh = new Mesh
            {
                vertices = meshTileProperties.Vertices,
                triangles = meshTileProperties.Triangles,
                uv = meshTileProperties.UVs
            };

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            mf.sharedMesh = mesh;

            // If this has a mesh collider, update the mesh
            MeshCollider meshCollider = TileModel.gameObject.GetComponent<MeshCollider>();
            MeshCollider mCollider = meshCollider;
            if (mCollider != null)
            {
                mCollider.sharedMesh = mesh;
            }
        }

        public bool Equals(Tile other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && id == other.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Tile) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ id;
            }
        }

        public void ConnectObject(GameObject objToConnect)
        {
            objToConnect.transform.parent = null;
            SetModel();
            objToConnect.transform.SetParent(TileModel.transform);
        }
    }

    public class ClockwiseComparer : IComparer
    {
        private Vector3 mOrigin;

        public ClockwiseComparer(Vector3 origin)
        {
            mOrigin = origin;
        }

        public int Compare(object first, object second)
        {
            Vector3 v1 = (Vector3) first;
            Vector3 v2 = (Vector3) second;

            return IsClockwise(v2, v1, mOrigin);
        }

        public static int IsClockwise(Vector3 first, Vector3 second, Vector3 origin)
        {
            if (first == second)
            {
                return 0;
            }

            Vector3 firstOffset = first - origin;
            Vector3 secondOffset = second - origin;

            float angle1 = Mathf.Atan2(firstOffset.x, firstOffset.z);
            float angle2 = Mathf.Atan2(secondOffset.x, secondOffset.z);

            if (angle1 < angle2)
            {
                return 1;
            }

            if (angle1 > angle2)
            {
                return -1;
            }

            return (firstOffset.sqrMagnitude < secondOffset.sqrMagnitude) ? 1 : -1;
        }
    }

    public class ClockwiseComparer2D : IComparer
    {
        private Vector2 mOrigin;

        public ClockwiseComparer2D(Vector2 origin)
        {
            mOrigin = origin;
        }

        public int Compare(object first, object second)
        {
            Vector2 v1 = (Vector2) first;
            Vector2 v2 = (Vector2) second;

            return IsClockwise(v2, v1, mOrigin);
        }

        public static int IsClockwise(Vector2 first, Vector2 second, Vector2 origin)
        {
            if (first == second)
            {
                return 0;
            }

            Vector2 firstOffset = first - origin;
            Vector2 secondOffset = second - origin;

            float angle1 = Mathf.Atan2(firstOffset.x, firstOffset.y);
            float angle2 = Mathf.Atan2(secondOffset.x, secondOffset.y);

            if (angle1 < angle2)
            {
                return 1;
            }

            if (angle1 > angle2)
            {
                return -1;
            }

            return (firstOffset.sqrMagnitude < secondOffset.sqrMagnitude) ? 1 : -1;
        }
    }
}