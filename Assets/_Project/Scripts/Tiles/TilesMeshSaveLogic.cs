using System.Linq;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Tiles
{
    public class TilesMeshSaveLogic : MonoBehaviour
    {
        public TilesMeshSaveSettings TilesMeshSaveSettings;
        
        public MeshTileProperties GetMeshTilePropertiesByTileID(int id) =>
            TilesMeshSaveSettings.MeshTilePropertiesList.FirstOrDefault(properties => properties.TileID == id);

#if UNITY_EDITOR
        public void SaveSettings() => EditorUtility.SetDirty(TilesMeshSaveSettings);
#endif
    }
}