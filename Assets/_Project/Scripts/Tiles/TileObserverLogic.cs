﻿using System;
using _Project.Packs.HexPlanet.Scripts;
using Assets._Project.Scripts.Utilities.Extensions;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Tiles
{
    [SelectionBase]
    public class TileObserverLogic : MonoBehaviour
    {
        [SerializeField] private int tileIDToConnect;
        
        private int PositionIndex;
        const int pointsAmount = 6;

        public Tile Tile { get; private set; }

        [Button("Next point")]
        public void NextPoint()
        {
            SetNextIndex();

            Hexsphere planet = transform.parent.GetComponentInParent<Tile>().parentPlanet;
            ChangePosition(planet.YOffsetForObservers);
        }

        [Button("Previous point")]
        public void PreviousPoint()
        {
            SetPreviousIndex();

            Hexsphere planet = transform.parent.GetComponentInParent<Tile>().parentPlanet;
            ChangePosition(planet.YOffsetForObservers);
        }

        private void Awake()
        {
            Tile = transform.parent.GetComponentInParent<Tile>();
        }

        public void ChangePosition(float offset)
        {
            Tile = transform.parent.GetComponentInParent<Tile>();

            const float circleRadius = 0.05f;
            Vector3 center = new Vector3(0, Tile.ExtrudedHeight * Tile.parentPlanet.planetScale + offset, 0);

            var radians = 2 * Mathf.PI / pointsAmount * PositionIndex;
            var vertical = Mathf.Sin(radians);
            var horizontal = Mathf.Cos(radians);
            var spawnDir = new Vector3(horizontal, center.y, vertical);
            var spawnPos = spawnDir * circleRadius;
            transform.localPosition = spawnPos;
            transform.localRotation = Quaternion.Euler(0, Mathf.Rad2Deg * radians / 2, 0);
        }

        private void SetNextIndex() =>
            PositionIndex.SetNextIndex(pointsAmount);

        private void SetPreviousIndex() =>
            PositionIndex.SetPreviousIndex(pointsAmount);

#if UNITY_EDITOR
        [Button("ReConnect")]
        public void ReConnect()
        {
            Hexsphere planet = transform.parent.GetComponentInParent<Tile>().parentPlanet;
            Tile tile = planet.GetTileByID(tileIDToConnect);
            tile.ConnectObject(gameObject);
        }
#endif
    }

    // [CustomEditor(typeof(TileObserverLogic))]
    // [CanEditMultipleObjects]
    // public class TileObserverEditor : Editor
    // {
    //     private TileObserverLogic _tileObserverLogic;
    //     private int _tileIDToConnect;
    //
    //     private void OnEnable()
    //     {
    //         _tileObserverLogic = (TileObserverLogic)target;
    //     }
    //
    //     public override void OnInspectorGUI()
    //     {
    //         base.OnInspectorGUI();
    //
    //         EditorGUILayout.BeginHorizontal();
    //         _tileIDToConnect =
    //             EditorGUILayout.IntField("Re-Connect to Tile:",
    //                 _tileIDToConnect);
    //
    //         if (GUILayout.Button("Connect"))
    //         {
    //             // if (targets.Length > 1)
    //             // {
    //             //     for (int i = 0; i < targets.Length; i++)
    //             //     {
    //             //         Tile t = targets[i] as Tile;
    //             //         t.SetExtrusionHeight(absoluteExtrusionHeight);
    //             //         _heightProperty.floatValue = absoluteExtrusionHeight;
    //             //
    //             //         Debug.Log("ddddd" + absoluteExtrusionHeight);
    //             //     }
    //             // }
    //             // else
    //             // {
    //             _tileObserverLogic.ReConnect(_tileIDToConnect);
    //             // }
    //         }
    //
    //         EditorGUILayout.EndHorizontal();
    //
    //         /*EditorGUILayout.BeginHorizontal();
    //         if (GUILayout.Button("Next point"))
    //         {
    //             _tileObserverLogic.NextPoint();
    //         }
    //         
    //         if (GUILayout.Button("Previous point"))
    //         {
    //             _tileObserverLogic.PreviousPoint();
    //         }
    //         EditorGUILayout.EndHorizontal();*/
    //
    //         // serializedObject.ApplyModifiedProperties();
    //
    //         /*EditorGUILayout.BeginHorizontal();
    //         if (GUILayout.Button("Reconnect in same position"))
    //         {
    //             _tileObserverLogic.ChangePosition();
    //         }
    //         EditorGUILayout.EndHorizontal();*/
    //     }
    // }
}