using Assets._Project.Scripts.Utilities.Extensions;
using NaughtyAttributes;
using UnityEngine;

namespace _Project.Scripts.Tiles
{
    public class RoadController : MonoBehaviour
    {
        [SerializeField] private float _scale;
        [SerializeField] private float _offset;

        [Button("Rotate To Start Point")]
        public void RotateAndScaleRoad()
        {
            // transform.parent.GetComponentInParent<TileLogic>();
            Transform tileTransform = transform.parent.parent;
            Vector3 tileAngles = tileTransform.localEulerAngles;
            transform.localEulerAngles = new Vector3(90f, tileAngles.y + _offset, 0f);
            ScaleRoad();
        }

        // [Button("Scale")]
        public void ScaleRoad()
        {
            transform.localScale = Vector3.one * _scale;
            transform.SetLocalAxis(Axis.Y, 0.085f);
        }
    }
}