using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Packs.HexPlanet.Scripts;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.FactoriesStuff;
using _Project.Scripts.GameTools;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Utilities.Tools;
using _Project.Scripts.WorldUIStuff;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;
using Zenject;
using static _Project.Scripts.Tiles.OpenTileProperties;

namespace _Project.Scripts.Tiles
{
    public class TileLogic : MonoBehaviour, IHeroInteractable
    {
        [SerializeField] private TileTriggerObserver _tileObserver;

        [SerializeField] private bool _haveAdditionalTiles;

        [SerializeField, Tooltip("Tiles to open, when this Tile will be open"), ShowIf("_haveAdditionalTiles")]
        private int[] _additionalTileIndexes;

        // private float _increaseSpeed;
        private Tile _tile;
        private int _id;
        private bool _isHeroInsideTrigger = true;
        private TileWorldUI _tileWorldUI;
        private PlanetSettings _planetSettings;
        private WorldSettings _worldSettings;
        private UISettings _uiSettings;
        private OpenPlanetTiles _openPlanetTiles;
        private GameResourcesService _resourcesService;
        private HeroInventory _heroInventory;
        private DiContainer _diContainer;
        private readonly MinMaxFloat _openDelay = new MinMaxFloat(1f, 7f);

        public List<PropsValue> MaxPropsesValues { get; private set; }
        public List<PropsValue> CurrentPropsesValues = new List<PropsValue>();
        private readonly Stack<(GameObject, EntityProperty)> _entities = new();

        private IEntityFactory _entityPoolFactory;
        private IAdsService _adsService;
        private AudioService _audioService;

        private bool _isOpened => _openPlanetTiles.IsTileCashed(_id);

        public void Setup(OpenPlanetTiles openPlanetTiles, HeroInventory heroInventory, DataService dataService,
            DiContainer diContainer, GameResourcesService resourcesService, IEntityFactory entityPoolFactory,
            IAdsService adsService, AudioService audioService)
        {
            _audioService = audioService;
            _adsService = adsService;
            _entityPoolFactory = entityPoolFactory;
            _diContainer = diContainer;
            _resourcesService = resourcesService;

            _worldSettings = dataService.WorldSettings;
            _uiSettings = dataService.UISettings;
            _tile = GetComponent<Tile>();
            _id = _tile.id;
            _openPlanetTiles = openPlanetTiles;
            _planetSettings = _openPlanetTiles.PlanetSettings;
            _heroInventory = heroInventory;

            bool haveObserver = _tileObserver != null;

            switch (_planetSettings.TilesConfiguration)
            {
                case StartTilesConfiguration.Customizable:
                    if (_isOpened)
                    {
                        _tile.TileModel.Activate(false);
                    }
                    else
                        _tile.TileModel.Deactivate();

                    if (haveObserver) _tileObserver.Activate(!_isOpened);

                    if (_openPlanetTiles.HavePropsValueListByID(_id) && !_isOpened)
                    {
                        SetupResourcesValues();

                        if (haveObserver)
                            _tileObserver.Setup(this);

                        SetupUI();
                    }

                    _openPlanetTiles.TileBeOpened += OnTileBeOpened;
                    break;
                case StartTilesConfiguration.All:
                    _tile.TileModel.Activate(false);
                    if (haveObserver) _tileObserver.Activate(false);
                    break;
            }

            if (_openPlanetTiles.HaveMobsByID(_id))
            {
                var entities = _openPlanetTiles.GetEntitiesByID(_id);
                for (int i = 0; i < entities.Amount; i++)
                {
                    var entity = _entityPoolFactory.Create(entities.EntityType.ToString());
                    _entities.Push((entity, entities));
                }
            }

            TrySpawnEntities();
        }

        private void OnTileBeOpened(int tileID)
        {
            if (_tileObserver != null && _tileObserver.transform.parent.GetComponentInParent<Tile>().id == tileID)
            {
                _tileWorldUI.ShowAllBlocks();

                FillEmptyResources();
                _openPlanetTiles.RefreshFollowingTiles(CreateOpenTileProperties(_id, CurrentPropsesValues));
            }
        }

        private void SaveOpenProgress(int tileID)
        {
            _openPlanetTiles.RefreshFollowingTiles(CreateOpenTileProperties(tileID, CurrentPropsesValues));
        }

        private void OnDestroy()
        {
            _openPlanetTiles.TileBeOpened -= OnTileBeOpened;
        }

        private void SetupUI()
        {
            TileWorldUI propsCanvasPrefab = _uiSettings.TileWorldUI;
            PropsBlock propsBlockPrefab = _uiSettings.PropsOpenTileBlockUI;
            var tileWorldUI = SpawnTileWorldUI(propsCanvasPrefab);

            foreach (PropsValue propsValue in MaxPropsesValues)
            {
                PropsBlock propsBlock = SpawnPropsBlock(propsBlockPrefab, tileWorldUI);
                propsBlock.Setup(this, propsValue.GameResourceType);
                if (_openPlanetTiles.IsFollowingTileContains(_id))
                {
                    var currentAmount = CurrentPropsesValues.GetPropsValueByType(propsValue.GameResourceType).Amount;
                    propsBlock.UpdateAmount(currentAmount);
                }
            }

            if (!_openPlanetTiles.IsFollowingTileContains(_id))
                tileWorldUI.HideAllBlocks();
        }

        private TileWorldUI SpawnTileWorldUI(TileWorldUI propsCanvasPrefab)
        {
            var cashedTransform = transform;
            _tileWorldUI = _diContainer.InstantiatePrefab(propsCanvasPrefab.gameObject).GetComponent<TileWorldUI>();

            PlaceCanvasNearTileTrigger(propsCanvasPrefab, cashedTransform);

            return _tileWorldUI;
        }

        private void PlaceCanvasNearTileTrigger(TileWorldUI propsCanvasPrefab, Transform cashedTransform)
        {
            Transform uiTransform = _tileWorldUI.transform;
            uiTransform.SetParent(cashedTransform);
            uiTransform.localPosition = propsCanvasPrefab.transform.localPosition;

            float nearestDistance = 999f;
            TileTriggerObserver nearestTileTrigger = null;
            foreach (var tileTrigger in _tile.parentPlanet.TileTriggers)
            {
                float distance = Vector3.Distance(tileTrigger.transform.position, uiTransform.position);
                if (distance < nearestDistance && distance >= 1.6f && distance < 3f)
                {
                    nearestDistance = distance;
                    nearestTileTrigger = tileTrigger;
                }
            }

            if (nearestTileTrigger != null)
            {
                Vector3 direction = (nearestTileTrigger.transform.position - uiTransform.position).normalized;
                // Debug.Log($"dist {Vector3.Distance(nearestTileTrigger.transform.position, uiTransform.position)}");
                // Debug.DrawLine(uiTransform.position, nearestTileTrigger.transform.position, Color.magenta, 1000f);

                uiTransform.position += direction * 0.8f;
                uiTransform.SetLocalAxis(Axis.Y, propsCanvasPrefab.transform.localPosition.y);
            }
        }

        private PropsBlock SpawnPropsBlock(PropsBlock propsBlockPrefab, TileWorldUI tileWorldUI)
        {
            PropsBlock tilePropsBlock =
                _diContainer.InstantiatePrefab(propsBlockPrefab.gameObject).GetComponent<PropsBlock>();
            tileWorldUI.AddPropsBlock(tilePropsBlock);
            tilePropsBlock.transform.localPosition = Vector3.zero;
            return tilePropsBlock;
        }

        public void HeroTriggerEnter()
        {
            _isHeroInsideTrigger = true;
            PutRequiredPropses(_heroInventory);
        }

        public void HeroTriggerExit()
        {
            _isHeroInsideTrigger = false;
        }

        private void TryActivateNextTile()
        {
            if (IsGotAllPropses())
            {
                ActivateNextTile();
                HideUI();
                _openPlanetTiles.RemoveOldFollowingTiles(_id);
                _openPlanetTiles.AddOpenedTile(_id);
                _openPlanetTiles.TileBeOpened(_id);

                _tileObserver.Activate(false);
                _audioService.PlaySound(AudioClipId.tile_opened);

                TrySpawnEntities();
                ActivateAdditionalTiles();

                if (_tile.parentPlanet.IsGameComplete)
                    AnalyticsManager.GameCompleted(CompleteType.CompleteOpenAllTiles);

                DOVirtual.DelayedCall(1f, () => _adsService.ShowInterstitial());
            }
        }

        private void TrySpawnEntities()
        {
            if (!_isOpened) return;

            int amount = _entities.Count;
            Vector3 upOffset = Vector3.up * 1.8f;
            const float circleRadius = 0.5f;

            switch (amount)
            {
                case 1:
                    StartCoroutine(SpawnEntityRoutine(_entities.Pop(), upOffset));
                    break;
                case 2:
                case 3:
                {
                    for (int i = 0; i < amount; i++)
                    {
                        Vector3 offset = GetOffset(amount, i, circleRadius, upOffset);
                        StartCoroutine(SpawnEntityRoutine(_entities.Pop(), offset));
                    }

                    break;
                }
                default:
                    return;
            }
        }

        private static Vector3 GetOffset(int amount, float index, float circleRadius, Vector3 offset)
        {
            Vector3 center = offset;
            var radians = 2 * Mathf.PI / amount * index;
            var vertical = Mathf.Sin(radians);
            var horizontal = Mathf.Cos(radians);
            return new Vector3(horizontal * circleRadius, center.y, vertical * circleRadius);
        }

        private IEnumerator SpawnEntityRoutine((GameObject go, EntityProperty entityProperty) entity, Vector3 offset)
        {
            yield return new WaitForSeconds(0.9f);

            var (go, entityProperty) = entity;
            _tile.FakePlaceObject(go, offset);
            AIBase ai = go.GetComponent<AIBase>();
            ai.SetData(entityProperty.SavePath);
            ai.Initialize();
            ai.InitializeStateMachine();
            ai.gameObject.SetActive(true);
        }

        private void HideUI()
        {
            _tileWorldUI.HideAllBlocks();
        }

        private void ActivateNextTile()
        {
            _tile.TileModel.Activate(true);
        }

        private void SetupResourcesValues()
        {
            MaxPropsesValues = _openPlanetTiles.GetPropsValueListByID(_id);
            if (_openPlanetTiles.IsFollowingTileContains(_id))
                CurrentPropsesValues = _openPlanetTiles.FindFollowingTile(_id).PropsesValues;
        }

        private void FillEmptyResources()
        {
            foreach (PropsValue propsesValue in MaxPropsesValues)
                CurrentPropsesValues.Add(new PropsValue(propsesValue.GameResourceType));
        }

        private void PutRequiredPropses(HeroInventory heroInventory)
        {
            foreach (PropsValue inventoryPropsValue in heroInventory.PropsesValues)
            {
                GameResourceType gameResourceType = inventoryPropsValue.GameResourceType;
                if (IsRequiredPropsType(gameResourceType))
                {
                    PropsValue tilePropsValue = CurrentPropsesValues.GetPropsValueByType(gameResourceType);
                    int requiredAmount = GetRequiredPropsTypeAmount(gameResourceType);

                    if (heroInventory.HaveRequiredProps(gameResourceType))
                    {
                        if (_worldSettings.IsSmoothly)
                        {
                            var visualAmount = requiredAmount;
                            float logDelay = Mathf.Log(requiredAmount, 3.2f);
                            logDelay = Mathf.Clamp(logDelay, _openDelay.MinValue, _openDelay.MaxValue);
                            if (requiredAmount >= 35)
                                visualAmount = (int) (logDelay * 10f);

                            StartCoroutine(IncreaseAmountRoutine(tilePropsValue, heroInventory, requiredAmount,
                                logDelay / requiredAmount));
                            StartCoroutine(FlyRoutine(tilePropsValue, visualAmount, logDelay / visualAmount, heroInventory));
                        }
                        else
                            IncreaseAmountHigh(tilePropsValue, heroInventory, requiredAmount);
                    }
                }
            }
        }

        private IEnumerator FlyRoutine(PropsValue tilePropsValue, int requiredAmount, float delay, HeroInventory heroInventory)
        {
            while (_isHeroInsideTrigger)
            {
                if (tilePropsValue.Amount >= requiredAmount)
                    yield break;
                
                if (!heroInventory.HaveRequiredProps(tilePropsValue.GameResourceType))
                {
                    yield break;
                }

                _resourcesService.AnimateFly(tilePropsValue.GameResourceType, _heroInventory.ResourcesPoint,
                    transform, 0.3f,
                    new MinMaxFloat(1.4f, 1.6f), new MinMaxFloat(1.2f, 1.3f));

                yield return new WaitForSeconds(delay);
            }
        }

        private IEnumerator IncreaseAmountRoutine(PropsValue tilePropsValue, HeroInventory heroInventory,
            int requiredAmount, float delay)
        {
            while (_isHeroInsideTrigger)
            {
                if (tilePropsValue.Amount >= requiredAmount)
                {
                    TryActivateNextTile();
                    yield break;
                }

                if (!heroInventory.HaveRequiredProps(tilePropsValue.GameResourceType))
                {
                    yield break;
                }

                tilePropsValue.Amount++;
                ChangePropsAmount?.Invoke(tilePropsValue);
                heroInventory.TakeProps(tilePropsValue.GameResourceType, 1);
                SaveOpenProgress(_id);
                
                _audioService.PlaySound(AudioClipId.user_buy);

                yield return new WaitForSeconds(delay);
            }
        }

        private void IncreaseAmountHigh(PropsValue tilePropsValue, HeroInventory heroInventory,
            int requiredAmount)
        {
            while (_isHeroInsideTrigger)
            {
                if (tilePropsValue.Amount >= requiredAmount)
                {
                    ChangePropsAmount?.Invoke(tilePropsValue);
                    TryActivateNextTile();
                    return;
                }

                tilePropsValue.Amount++;
                heroInventory.TakeProps(tilePropsValue.GameResourceType, 1);
            }
        }

        private float FindDelay(int requiredAmount)
        {
            float logDelay = Mathf.Log(requiredAmount, 2.7f);
            float delay = Mathf.Clamp(logDelay / requiredAmount, _openDelay.MinValue, _openDelay.MaxValue);
            return delay;
        }

        private int GetRequiredPropsTypeAmount(GameResourceType gameResourceType)
        {
            PropsValue maxPropsValue = MaxPropsesValues.GetPropsValueByType(gameResourceType);
            return maxPropsValue.Amount;
        }

        private bool IsGotAllPropses()
        {
            return !(from propsValue in MaxPropsesValues
                let type = propsValue.GameResourceType
                where propsValue.Amount != CurrentPropsesValues.GetPropsValueByType(type).Amount
                select propsValue).Any();
        }

        private bool IsRequiredPropsType(GameResourceType gameResourceType) =>
            CurrentPropsesValues.Any(propsValue => propsValue.GameResourceType == gameResourceType);

        private void ActivateAdditionalTiles()
        {
            if (!_haveAdditionalTiles || _additionalTileIndexes.Length <= 0) return;

            foreach (var tileID in _additionalTileIndexes)
            {
                Tile tile = _tile.parentPlanet.GetTileByID(tileID);
                tile.TileModel.Activate(true);
            }
        }

#if UNITY_EDITOR

        [Button("Extrude")]
        private void OnExtrude()
        {
            foreach (var tr in Selection.transforms)
                if (tr.TryGetComponent(out Tile tile))
                    tile.SetExtrusionHeight();
        }

        #region Gizmos

        private void OnDrawGizmosSelected()
        {
            if (!Selection.gameObjects.Contains(transform.gameObject)) return;

            Vector3 center = GetComponent<Tile>().FaceCenter;

            Gizmos.color = Color.magenta;
            Gizmos.DrawCube(center, Vector3.one * 0.02f);
            Gizmos.color = Color.white;
        }

        #endregion

#endif


        public event Action<PropsValue> ChangePropsAmount;

        public Sprite GetSpriteByResourceType(GameResourceType resourceType)
        {
            // Debug.Log($"t = {resourceType}");
            foreach (var spriteProperty in _worldSettings.ResourceSpriteProperties)
            {
                if (spriteProperty.GameResourceType == resourceType)
                    return spriteProperty.Sprite;
            }

            throw new IndexOutOfRangeException($"Sprite for {resourceType} type isn't find");
        }
    }

    public static class TileLogicExtensions
    {
        public static PropsValue GetPropsValueByType(this IEnumerable<PropsValue> propsesValues,
            GameResourceType gameResourceType) =>
            propsesValues.FirstOrDefault(propsValue => propsValue.GameResourceType == gameResourceType);
    }
}