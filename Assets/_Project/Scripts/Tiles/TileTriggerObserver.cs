﻿using System;
using _Project.Scripts.Behaviours;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using UnityEngine;

namespace _Project.Scripts.Tiles
{
    public class TileTriggerObserver : MonoBehaviour
    {
        private TileLogic _tileLogic;
        [HideInInspector] public bool IsHeroInside;

        public TileLogic TileLogic => _tileLogic;

        public void Setup(TileLogic tileLogic)
        {
            _tileLogic = tileLogic;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<HeroInventory>(out var heroInventory))
            {
                _tileLogic.HeroTriggerEnter();
                // IsHeroInside = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent<HeroInventory>(out var heroInventory))
            {
                _tileLogic.HeroTriggerExit();
                // IsHeroInside = false;
            }
        }

        public void Activate(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}