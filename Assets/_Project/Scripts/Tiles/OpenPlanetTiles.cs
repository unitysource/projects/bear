﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.SaveLoadService.Data;
using _Project.Scripts.FactoriesStuff;
using _Project.Scripts.SettingsStuff;
using KamaliDebug;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;
using Zenject;
using static _Project.Scripts.Tiles.OpenTileProperties;

namespace _Project.Scripts.Tiles
{
    public class OpenPlanetTiles : MonoBehaviour
    {
        [SerializeField] private OpenTilesSettings _openTilesSettings;
        [SerializeField] private TileAssetsSettings _tileAssetsSettings;

        [SerializeField] private PlanetSettings _planetSettings;

        public PlanetSettings PlanetSettings => _planetSettings;
        private ISaveLoadService _saveLoadService;
        private PlanetData _planetData;

        [Inject]
        private void Construct(ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
        }

        public void Initialize()
        {
            _planetData = _saveLoadService.PlanetData;
            FillStartTiles();
            FillFollowingTiles();
        }

        public List<PropsValue> GetPropsValueListByID(int id)
        {
            var propsValues = _openTilesSettings.OpenTilesPropses.Where(openTile => openTile.TileID == id)
                .Select(openTile => openTile.PropsesValues).FirstOrDefault();
            if (propsValues == null)
                DebugX.Log($"Check props values for tile: [{id.ToString()}:yellow:b;]");
            return propsValues;
        }

        public bool HavePropsValueListByID(int id) =>
            _openTilesSettings.OpenTilesPropses.Any(openTile => openTile.TileID == id);

        public bool HaveMobsByID(int id) =>
            _tileAssetsSettings.EntityProperties.Any(entityProperty => entityProperty.TileID == id);

        public EntityProperty GetEntitiesByID(int id)
        {
            var entitiesProperty = _tileAssetsSettings.EntityProperties.First(property => property.TileID == id);
            return entitiesProperty;
        }

        public Action<int> TileBeOpened = _ => { };

        public void AddOpenedTile(int id)
        {
            if (!IsTileCashed(id))
                _planetData.CashedOpenTiles.Add(id);
        }

        public void RefreshFollowingTiles(OpenTileProperties openTileProperties)
        {
            if (!IsFollowingTileContains(openTileProperties.TileID))
                _planetData.FollowingTiles.Add(openTileProperties);
            else
                FindFollowingTile(openTileProperties.TileID).PropsesValues = openTileProperties.PropsesValues;
        }

        public OpenTileProperties FindFollowingTile(int tileID) =>
            _planetData.FollowingTiles.Find(properties => properties.TileID == tileID);

        public void RemoveOldFollowingTiles(int tileID)
        {
            if (IsFollowingTileContains(tileID))
                _planetData.FollowingTiles.Remove(FindFollowingTile(tileID));
        }

        private void FillStartTiles()
        {
            foreach (var tileID in _planetSettings.OpenTileIDs)
                if (!IsTileCashed(tileID))
                    AddOpenedTile(tileID);
        }

        private void FillFollowingTiles()
        {
            foreach (var tileID in _planetSettings.CanBeOpenedInStartTileIDs)
            {
                if (IsFollowingTileContains(tileID) || IsTileCashed(tileID)) break;
                var propsValues = GetPropsValueListByID(tileID);
                var zeroValuesList = propsValues.Select(propsValue =>
                    new PropsValue(propsValue.GameResourceType)).ToList();
                RefreshFollowingTiles(CreateOpenTileProperties(tileID, zeroValuesList));
            }
        }

        public bool IsTileCashed(int id) =>
            _planetData.CashedOpenTiles.Contains(id);

        public bool IsFollowingTileContains(int id) =>
            _planetData.FollowingTiles.Exists(properties => properties.TileID == id);

        #region Editor Tools

#if UNITY_EDITOR

        [SerializeField] private TextAsset _openTilesResources;


        [Button("Update Tiles Data")]
        public void UpdateTilesData()
        {
            string json = _openTilesResources.text;
            TileInfo[] tiles = JsonHelper.FromJson<TileInfo>(json);

            _openTilesSettings.OpenTilesPropses.Clear();
            // _tileAssetsSettings.EntityProperties.Clear();

            foreach (TileInfo info in tiles)
            {
                _openTilesSettings.OpenTilesPropses.Add(info.GetResources());
                // _tileAssetsSettings.EntityProperties.Add(info.GetEntityProperty());
            }
            
            EditorUtility.SetDirty(_openTilesSettings);
            Debug.Log($"Tiles Data is up to date");
        }

        [Serializable]
        public class TileInfo
        {
            public int Tile;
            public int Wood;
            public int Stone;
            public int Plank;
            public int Iron;
            public int Gold;
            public int K_131;
            public int K_132;
            public int K_133;
            public int K_134;
            public int K_135;

            public int Mob;

            public OpenTileProperties GetResources()
            {
                List<PropsValue> resources = new List<PropsValue>();

                if (Wood != 0) resources.Add(new PropsValue(GameResourceType.Wood, Wood));
                if (Stone != 0) resources.Add(new PropsValue(GameResourceType.Stone, Stone));
                if (Plank != 0) resources.Add(new PropsValue(GameResourceType.Plank, Plank));
                if (Iron != 0) resources.Add(new PropsValue(GameResourceType.Iron, Iron));
                if (Gold != 0) resources.Add(new PropsValue(GameResourceType.Gold, Gold));
                if (K_131 != 0) resources.Add(new PropsValue(GameResourceType.K_131, K_131));
                if (K_132 != 0) resources.Add(new PropsValue(GameResourceType.K_132, K_132));
                if (K_133 != 0) resources.Add(new PropsValue(GameResourceType.K_133, K_133));
                if (K_134 != 0) resources.Add(new PropsValue(GameResourceType.K_134, K_134));
                if (K_135 != 0) resources.Add(new PropsValue(GameResourceType.K_135, K_135));

                return new OpenTileProperties(Tile, resources);
            }

            /*public EntityProperty GetEntityProperty()
            {
                EntityType type = EntityType.None;
                if (Mob == 1) type = EntityType.Cryper;

                return new EntityProperty(Tile, type);
            }*/
        }
#endif

        #endregion
    }
}