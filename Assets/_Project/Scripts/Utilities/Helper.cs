using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Behaviours
{
    public class Helper : MonoBehaviour
    {
        [SerializeField] private KeyCode _restartKey;
        
        private void Update()
        {
            RestartScene();
        }

        private void RestartScene()
        {
            if (Input.GetKeyDown(_restartKey))
            {
                CommonTools.RestartScene();
            }
        }
    }
}