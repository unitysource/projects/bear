using UnityEngine;

namespace Assets._Project.Scripts.Utilities.Constants
{
    public static class Constants
    {
        public static readonly Color ErrorColor = Color.black;
    }
}