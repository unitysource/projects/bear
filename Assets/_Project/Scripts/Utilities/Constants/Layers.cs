namespace Assets._Project.Scripts.Utilities.Constants
{
    public static class Layers
    {
        public const string Default = "Default";
        public const string Ground = "Ground";
        public const string Water = "Water";
        public const string UnTamedPet = "UnTamedPet";
    }
}