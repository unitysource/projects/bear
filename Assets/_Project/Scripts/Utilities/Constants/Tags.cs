
namespace Assets._Project.Scripts.Utilities.Constants
{
    public static class Tags
    {
        public const string Node = "Node";
        public const string Enemy = "Enemy";
        public const string Monster = "Monster";
        public const string ToDestroy = "ToDestroy";
    }
}