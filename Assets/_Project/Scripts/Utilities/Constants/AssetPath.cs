using UnityEngine;

namespace Assets._Project.Scripts.Utilities.Constants
{
    public static class AssetPath
    {
        public const string GlobalSettingsPath = "Settings/GlobalSettings";
        public const string PlanetsPath = "Settings/Planets";
        public const string Instruments = "Settings/Instruments";
        public const string Portals = "Settings/Portals";
        public const string WindowsPath = "UI/Windows";
    }
}