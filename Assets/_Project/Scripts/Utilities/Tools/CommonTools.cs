using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using _Project.Scripts;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Tiles;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Assets._Project.Scripts.Utilities
{
    public static class CommonTools
    {
        private static readonly string[] names = {"", "K", "M", "B", "T"};

        public static string FormatAmount(float digit)
        {
            int n = 0;
            while (n + 1 < names.Length && digit >= 1000)
            {
                digit /= 1000;
                n++;
            }

            return $"{Math.Round(digit, 1)}{names[n]}";
        }

        public static bool IsPointerOverUIObject()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            return results.Any(result => result.gameObject.layer == 5);
        }

        public static bool IsPointerOverUIObjectIgnoreJoystick()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            return !results.Where(result => result.gameObject.layer == 5)
                .Any(res => res.gameObject.TryGetComponent(out Joystick _));
        }

        public static void SaveCurrentTime() =>
            SaveManager.SetString(SaveKey.LastQuitGameTime, CurrentTime);

        public static string CurrentTime => DateTime.Now.ToString(CultureInfo.InvariantCulture);

        public static DateTime GetLastQuitGameTime =>
            DateTime.Parse(SaveManager.GetString(SaveKey.LastQuitGameTime));

        public static double GetQuitedGameSeconds() =>
            (DateTime.Now - GetLastQuitGameTime).TotalSeconds;

        public static void UpdateText(TMP_Text tmpText, string message) => tmpText.text = message;

        public static void UpdateText(TMP_Text tmpText, int message, bool withRound = true)
        {
            tmpText.text = withRound ? $"{FormatAmount(message)}" : $"{message.ToString()}";
        }

        public static int BoolToInt(bool val) => val ? 1 : 0;

        public static bool IntToBool(int val) => val != 0;

        public static void ChangeIntValue(ref int valueToChange) => 
            valueToChange = valueToChange == 1 ? 0 : 1;

        public static int GetRandomIndex(int maxValue) => UnityEngine.Random.Range(0, maxValue);

        public static int[] GetRandomIndexes(int maxValue, int amount)
        {
            var rand = new System.Random();
            List<int> listNumbers = new List<int>();
            listNumbers.AddRange(Enumerable.Range(0, maxValue)
                .OrderBy(i => rand.Next()).Take(amount));
            return listNumbers.ToArray();
        }

        public static bool IsOnlyOneTouch()
        {
#if UNITY_EDITOR
            return true;
#else
        return Input.touchCount == 1;
#endif
        }

        public static void RestartScene() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        public static void CountdownInt(TMP_Text textTMP, int timerStart, int timerEnd, float duration) =>
            DOVirtual.Float(timerStart, timerEnd, duration, value => textTMP.text = ((int) value).ToString());

        public static GameObject GetObjectByEnumName(Type enumType, string enumElement, Func<GameObject> func)
        {
            if (Enum.GetNames(enumType).Any(name => name == enumElement))
            {
                return func?.Invoke();
            }

            throw new ArgumentOutOfRangeException(nameof(enumElement), enumElement, "Resource is out of range");
        }

        public static void EnumNamesLoopAction(Type enumType, Action<string> action)
        {
            foreach (var name in Enum.GetNames(enumType))
                action.Invoke(name);
        }

        public static void DictionaryLoopAction(Dictionary<string, GameObject> dictionary, Action<string> action)
        {
            foreach (var name in dictionary.Keys)
                action.Invoke(name);
        }

        // public static T[] GetAllEnumElements<T>(Type enumType)
        // {
        //     return Enum.GetValues()
        // }
        public static void TimerActionLooped(ref float currentValue, float maxValue, Action action)
        {
            if (currentValue >= maxValue)
            {
                action?.Invoke();
                currentValue = 0f;
            }

            currentValue += Time.fixedDeltaTime;
        }

        public static void TimerActionOnce(ref float currentValue, float maxValue, Action action)
        {
            if (currentValue >= maxValue) 
                action?.Invoke();
            else 
                currentValue += Time.fixedDeltaTime;
        }

        public static int GetNextValue(int oldValue, int maxValue) => 
            (oldValue + 1) % maxValue;
    }
}