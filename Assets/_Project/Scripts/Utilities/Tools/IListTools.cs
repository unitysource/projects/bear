using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Utilities.Tools
{
    public static class IListTools
    {
        public static T GetNearestElement<T>(IEnumerable<T> toArray, Transform fromTransform) where T : MonoBehaviour
        {
            float nearestDistance = 999f;
            T nearestElement = null;
            foreach (var element in toArray)
            {
                float distance = Vector3.Distance(element.transform.position, fromTransform.position);
                if (distance < nearestDistance)
                {
                    nearestDistance = distance;
                    nearestElement = element;
                }
            }

            return nearestElement;
        }
    }
}