﻿using System;
using System.ComponentModel;
using _Project.Scripts.EntitiesStuff;
using Assets._Project.Scripts.Utilities.Extensions;

namespace _Project.Scripts.Mechanics
{
    public static class EnumTools
    {
        public static int GetLength(Type type) => Enum.GetValues(type).Length;
        public static T GetRandom<T>(Type type) => ((T[]) Enum.GetValues(type)).GetRandomElement();

        public static T GetElementByName<T>(Type type, string name)
        {
            var values = (T[]) Enum.GetValues(type);
            foreach (var value in values)
            {
                if (value.ToString() == name)
                {
                    return value;
                }
            }

            throw new InvalidEnumArgumentException(name);
        }
    }
}