using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Assets._Project.Scripts.Utilities.Extensions
{
    public enum Axis
    {
        X,
        Y,
        Z
    }

    public static class Extensions
    {
        public static T GetRandomElement<T>(this T[] array) =>
            array[UnityEngine.Random.Range(0, array.Length)];

        public static object GetFirstValue(this Hashtable hashtable) =>
            (from DictionaryEntry de in hashtable select de.Value).FirstOrDefault();

        public static void SetLocalAxis(this Transform transform, Axis axis, float value)
        {
            Vector3 position = transform.localPosition;

            transform.localPosition = axis switch
            {
                Axis.X => new Vector3(value, position.y, position.z),
                Axis.Y => new Vector3(position.x, value, position.z),
                Axis.Z => new Vector3(position.x, position.y, value),
                _ => throw new ArgumentOutOfRangeException(nameof(axis), axis, null)
            };
        }

        public static void SetAxis(this Transform transform, Axis axis, float value)
        {
            Vector3 position = transform.position;

            transform.position = axis switch
            {
                Axis.X => new Vector3(value, position.y, position.z),
                Axis.Y => new Vector3(position.x, value, position.z),
                Axis.Z => new Vector3(position.x, position.y, value),
                _ => throw new ArgumentOutOfRangeException(nameof(axis), axis, null)
            };
        }

        public static List<Transform> GetAllChildren(this GameObject go)
        {
            List<Transform> children = new List<Transform>();
            Transform parentTransform = go.transform;
            for (int i = 0; i < parentTransform.childCount; i++)
                children.Add(parentTransform.GetChild(i));

            return children;
        }
        
        // public static void InvokeActionForElements(this GameObject[] array, Action<Transform> action)
        // {
        //     List<Transform> children = new List<Transform>();
        //     Transform parentTransform = go.transform;
        //     for (int i = 0; i < parentTransform.childCount; i++)
        //     {
        //         Transform child = parentTransform.GetChild(i);
        //         children.Add(child);
        //         action?.Invoke(child);
        //     }
        //
        //     return children;
        // }

        public static List<Transform> ActionAllChildren(this GameObject go, Action<Transform> action)
        {
            List<Transform> children = new List<Transform>();
            Transform parentTransform = go.transform;
            for (int i = 0; i < parentTransform.childCount; i++)
            {
                Transform child = parentTransform.GetChild(i);
                children.Add(child);
                action?.Invoke(child);
            }

            return children;
        }

        public static void SetNextIndex(this ref int index, int maxIndex) => index = (index + 1) % maxIndex;
        public static void SetPreviousIndex(this ref int index, int maxIndex)
        {
            if (index <= 0)
                index = maxIndex - 1;
            else
                index--;
        }
        
        /*public static void Move<T>(this List<T> list, int oldIndex, int newIndex)
        {
            var item = list[oldIndex];

            list.RemoveAt(oldIndex);

            if (newIndex > oldIndex) newIndex--;
            // the actual index could have shifted due to the removal

            list.Insert(newIndex, item);
        }*/

        public static void Move<T>(this List<T> list, T item, int newIndex)
        {
            if (item != null)
            {
                var oldIndex = list.IndexOf(item);
                if (oldIndex > -1)
                {
                    list.RemoveAt(oldIndex);

                    if (newIndex > oldIndex) newIndex--;
                    // the actual index could have shifted due to the removal

                    list.Insert(newIndex, item);
                }
            }

        }
        
        public static void Move<T>(this List<T> list, int oldIndex, int newIndex)
        {
            // exit if positions are equal or outside array
            if ((oldIndex == newIndex) || (0 > oldIndex) || (oldIndex >= list.Count) || (0 > newIndex) ||
                (newIndex >= list.Count)) return;
            // local variables
            var i = 0;
            T tmp = list[oldIndex];
            // move element down and shift other elements up
            if (oldIndex < newIndex)
            {
                for (i = oldIndex; i < newIndex; i++)
                {
                    list[i] = list[i + 1];
                }
            }
            // move element up and shift other elements down
            else
            {
                for (i = oldIndex; i > newIndex; i--)
                {
                    list[i] = list[i - 1];
                }
            }
            // put element from position 1 to destination
            list[newIndex] = tmp;
        }

        public static bool ToBool(this int intValue) => 
            CommonTools.IntToBool(intValue);

        public static int ToInt(this bool boolValue) =>
            CommonTools.BoolToInt(boolValue);
    }
}