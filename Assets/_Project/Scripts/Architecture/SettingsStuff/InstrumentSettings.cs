using System;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "InstrumentSettings", menuName = "Instrument/InstrumentSettings")]
    public class InstrumentSettings : ScriptableObject
    {
        [SerializeField] private Sprite _sprite;
        [SerializeField] private GameObject _prefab;
        [SerializeField] private float _damage;
        [SerializeField] private int[] _increaseAmounts;
        [SerializeField] private PropsValue[] _resourceAmountProperties;
        [SerializeField] private Color _trailColor;
        

        public float Damage => _damage;
        public GameObject Prefab => _prefab;
        public Sprite Sprite => _sprite;
        public int[] IncreaseAmounts => _increaseAmounts;
        public PropsValue[] ResourceAmountProperties => _resourceAmountProperties;

        public Color TrailColor => _trailColor;
    }
}