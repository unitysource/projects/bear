using System;
using System.Security.AccessControl;
using _Project.Scripts.Tiles;
using _Project.Scripts.WorldUIStuff;
using NaughtyAttributes;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "WorldSettings", menuName = "GlobalSettings/WorldSettings")]
    public class WorldSettings : ScriptableObject
    {
        [SerializeField] private bool _isSmoothly;
        [ShowIf("_isSmoothly"), Tooltip("Speed of fill up resources to open new tile"), Range(10f, 60)] [SerializeField]
        private float _increaseAmountOpenTileSpeed;

        [Tooltip("Time to restore landscape (tree, stone, eg.)"), Range(2f, 15f)]
        [SerializeField] private float _restoreLandscapeDelay = 3f;
        
        [SerializeField] private ResourceSpriteProperty[] _resourceSpriteProperties;
        
        [SerializeField] private int _maxFlyResourcesAmount = 30;

        [SerializeField] private float _reloadAdDuration;
        [SerializeField] private float _animAdDelay;
        
        public float IncreaseAmountOpenTileSpeed => _increaseAmountOpenTileSpeed;
        public float RestoreLandscapeDelay => _restoreLandscapeDelay;
        public bool IsSmoothly => _isSmoothly;

        public ResourceSpriteProperty[] ResourceSpriteProperties => _resourceSpriteProperties;

        public int MAXFlyResourcesAmount => _maxFlyResourcesAmount;

        public float ReloadAdDuration => _reloadAdDuration;

        public float AnimAdDelay => _animAdDelay;
    }

    [Serializable]
    public class ResourceSpriteProperty
    {
        [SerializeField] private GameResourceType _resourceType;
        [SerializeField] private Sprite _sprite;
        [SerializeField] private int _tmpSpriteIndex;
        [SerializeField] private int _hudPriorityIndex;

        public Sprite Sprite => _sprite;

        public GameResourceType GameResourceType => _resourceType;

        public int TMPSpriteIndex => _tmpSpriteIndex;

        public int HUDPriorityIndex => _hudPriorityIndex;
    }
}