using UnityEngine;

namespace _Project.Scripts.Architecture.SettingsStuff
{
    [CreateAssetMenu(fileName = "DeathSettings", menuName = "Settings/DeathSettings")]
    public class DeathSettings : ScriptableObject
    {
        [SerializeField] private ParticleSystem _deathParticlesPrefab;
        [SerializeField] private Color[] _particlesColor;
        [SerializeField] private float _radius;
        [SerializeField] private float _deathDuration = 1f;
        [SerializeField] private AnimationCurve _animationCurve;

        public Color[] ParticlesColor => _particlesColor;

        public float Radius => _radius;

        public float DeathDuration => _deathDuration;

        public ParticleSystem DeathParticlesPrefab => _deathParticlesPrefab;

        public AnimationCurve MoveCurve => _animationCurve;
    }
}