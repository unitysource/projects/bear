using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Tiles
{
    [CreateAssetMenu(fileName = "OpenTilesSettings", menuName = "Data/OpenTilesSettings")]
    public class OpenTilesSettings : ScriptableObject
    {
        [SerializeField] private List<OpenTileProperties> _openTilesPropses;

        public List<OpenTileProperties> OpenTilesPropses => _openTilesPropses;
    }

    [Serializable]
    public class OpenTileProperties
    {
        [SerializeField] private int _tileID;
        [SerializeField] private List<PropsValue> _propsesValues;

        public OpenTileProperties(int tileID, List<PropsValue> propsesValues)
        {
            _tileID = tileID;
            _propsesValues = propsesValues;
        }

        public List<PropsValue> PropsesValues
        {
            get => _propsesValues;
            set => _propsesValues = value;
        }

        public int TileID => _tileID;

        public static OpenTileProperties CreateOpenTileProperties(int id, List<PropsValue> resourcesValue) =>
            new OpenTileProperties(id, resourcesValue);
    }

    [Serializable]
    public class PropsValue
    {
        [SerializeField] private GameResourceType gameResourceType;
        [SerializeField] private int _amount;

        public PropsValue(GameResourceType gameResourceType, int amount = 0)
        {
            this.gameResourceType = gameResourceType;
            _amount = amount;
        }

        public int Amount
        {
            get => _amount;
            set => _amount = value;
        }

        public GameResourceType GameResourceType => gameResourceType;
    }
}