using UnityEngine;

namespace _Project.Scripts.Entities
{
    [CreateAssetMenu(fileName = "HealthSettings", menuName = "Settings/HealthSettings")]
    public class HealthSettings : ScriptableObject
    {
        [SerializeField] protected int _maxHealth = 1;

        public int MAXHealth
        {
            get => _maxHealth;
            set => _maxHealth = value;
        }
    }
}