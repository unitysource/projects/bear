using _Project.Scripts.WorldUIStuff;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "GlobalSettings/UISettings")]
    public class UISettings : ScriptableObject
    {
        [SerializeField] private TileWorldUI _tileWorldUI;
        [SerializeField] private PropsBlock _propsOpenTileBlockUI;

        [SerializeField] private float _openDuration = 1f;
        [SerializeField] private float _closeDuration = 1f;

        [SerializeField] private Color _enoughResourceColor;
        [SerializeField] private Color _deficiencyResourceColor;

        public TileWorldUI TileWorldUI => _tileWorldUI;

        public PropsBlock PropsOpenTileBlockUI => _propsOpenTileBlockUI;

        public float OpenDuration => _openDuration;

        public float CloseDuration => _closeDuration;

        public Color EnoughResourceColor => _enoughResourceColor;

        public Color DeficiencyResourceColor => _deficiencyResourceColor;
    }
}