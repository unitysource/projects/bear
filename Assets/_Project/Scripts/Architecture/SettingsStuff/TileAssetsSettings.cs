using System;
using System.Collections.Generic;
using _Project.Scripts.FactoriesStuff;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "TileAssetsSettings", menuName = "Settings/TileAssetsSettings")]
    public class TileAssetsSettings : ScriptableObject
    {
        [SerializeField] private List<EntityProperty> _entityProperties;

        public List<EntityProperty> EntityProperties => _entityProperties;
    }

    [Serializable]
    public class EntityProperty
    {
        [SerializeField] private int _tileID;
        [SerializeField] private EntityType _entityType;
        [SerializeField] private int _amount;
        [SerializeField] private string _savePath;

        public EntityProperty(int tileID, EntityType entityType, int amount)
        {
            _entityType = entityType;
            _tileID = tileID;
            _amount = amount;
        }

        public EntityType EntityType => _entityType;

        public int TileID => _tileID;

        public int Amount => _amount;

        public string SavePath => _savePath;
    }
}