using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Services.InputStuff;
using Assets._Project.Scripts.Managers;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Infrastructure
{
    public class BehavioursInstaller : MonoInstaller
    {
        [SerializeField] private Canvas _joystickCanvasPrefab;
        [SerializeField] private Camera _joystickCamera;
        
        [SerializeField] private InputBehaviour _inputBehaviour;
        [SerializeField] private AudioService _audioService;
        [SerializeField] private VibrationService _vibrationService;

        public override void InstallBindings()
        {
            BindInputBehaviour();
            BindJoystick();
            BindAudio();
            BindVibrations();
        }

        private void BindVibrations()
        {
            Container.Bind<VibrationService>().FromInstance(_vibrationService).AsSingle();
        }

        private void BindAudio()
        {
            Container.Bind<AudioService>().FromInstance(_audioService).AsSingle();
        }

        private void BindJoystick()
        {
            var joystickCanvas = Container.InstantiatePrefabForComponent<Canvas>(_joystickCanvasPrefab);
            joystickCanvas.worldCamera = _joystickCamera;
            var joystick = joystickCanvas.GetComponentInChildren<Joystick>();
            Container.Bind<Joystick>().FromInstance(joystick).AsSingle();
        }
        
        private void BindInputBehaviour()
        {
            Container.Bind<InputBehaviour>().FromInstance(_inputBehaviour).AsSingle();
        }
    }
}