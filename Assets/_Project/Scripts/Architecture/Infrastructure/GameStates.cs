// using System.Collections;
// using UnityEngine;
//
// namespace _Project.Scripts.Infrastructure
// {
//     public class GameStates : MonoBehaviour
//     {
//         public LoadLevelState LoadLevelState;
//         public SettingsState settingState;
//         public LoopState LoopState;
//
//         public void ChangeState(GameStateBase stateBase)
//         {
//             stateBase.EnterState();
//         }
//     }
//
//     public abstract class GameStateBase : MonoBehaviour
//     {
//         public abstract void EnterState();
//     }
//
//     public class LoadLevelState : GameStateBase
//     {
//         public override void EnterState()
//         {
//             StartCoroutine(nameof(LoadRoutine));
//         }
//
//         private IEnumerator LoadRoutine()
//         {
//             yield return null;
//         }
//     }
//     
//     public class SettingsState : GameStateBase
//     {
//         public override void EnterState()
//         {
//             
//         }
//     }
//     
//     public class LoopState : GameStateBase
//     {
//         public override void EnterState()
//         {
//             
//         }
//     }
// }