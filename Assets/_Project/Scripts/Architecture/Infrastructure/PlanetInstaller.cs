using _Project.Packs.HexPlanet.Scripts;
using _Project.Scripts.Tiles;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Infrastructure
{
    public class PlanetInstaller : MonoInstaller
    {
        [SerializeField] private Hexsphere _planetPrefab;
        [SerializeField] private Transform _startPoint;
        private Hexsphere _planetInstance;

        public override void InstallBindings()
        {
            if (FindObjectOfType<Hexsphere>())
            {
                Debug.LogWarning("You already have one planet in scene");
                Container.Bind<GravityAttractor>().FromInstance(FindObjectOfType<GravityAttractor>());
            }
            else
            {
                BindPlanet();
                // BindOpenTilesLogic();
                BindGravity();
            }
        }

        private void BindPlanet()
        {
            _planetInstance = Container.InstantiatePrefabForComponent<Hexsphere>(_planetPrefab,
                _startPoint.position, Quaternion.identity, null);
        }

        private void BindOpenTilesLogic()
        {
            OpenPlanetTiles planetTiles = _planetInstance.GetComponent<OpenPlanetTiles>();
            Container.Bind<OpenPlanetTiles>().FromInstance(planetTiles).AsSingle();
        }

        private void BindGravity()
        {
            GravityAttractor gravityAttractor = _planetInstance.GetComponent<GravityAttractor>();
            Container.Bind<GravityAttractor>().FromInstance(gravityAttractor).AsSingle();
        }
    }
}