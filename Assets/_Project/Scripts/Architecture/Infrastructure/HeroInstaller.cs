using _Project.Scripts.Behaviours;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Infrastructure
{
    public class HeroInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _heroPrefab;
        [SerializeField] private Transform _startPoint;

        private GameObject _heroInstance;

        public override void InstallBindings()
        {
            BindHero();
            BindHeroMovement();
            BindHeroInventoryLogic();
        }

        private void BindHero()
        {
            _heroInstance = Container.InstantiatePrefab(_heroPrefab, _startPoint.position, 
                Quaternion.identity, null);
        }

        private void BindHeroMovement()
        {
            HeroMovement heroMovement = _heroInstance.GetComponent<HeroMovement>();
            Container.Bind<HeroMovement>().FromInstance(heroMovement).AsSingle();
        }

        private void BindHeroInventoryLogic()
        {
            HeroInventory inventory = _heroInstance.GetComponent<HeroInventory>();
            Container.Bind<HeroInventory>().FromInstance(inventory).AsSingle();
        }
    }
}