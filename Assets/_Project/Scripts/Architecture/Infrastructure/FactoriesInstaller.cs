using _Project.Scripts.FactoriesStuff;
using _Project.Scripts.ResourcesStuff;
using Zenject;

namespace _Project.Scripts.Infrastructure
{
    public class FactoriesInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindGameResourcesFactory();
            BindEntitiesFactory();
            
            BindResourcesService();
        }

        private void BindEntitiesFactory()
        {
            Container.Bind<IEntityFactory>().To<EntityPoolFactory>().AsSingle();
        }

        private void BindGameResourcesFactory()
        {
            Container.Bind<IResourcesFactory>().To<ResourcesPoolFactory>().AsSingle();
        }
        
        private void BindResourcesService()
        {
            Container.Bind<GameResourcesService>().AsSingle();
        }
    }
}