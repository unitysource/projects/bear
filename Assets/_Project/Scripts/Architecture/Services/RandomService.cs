using _Project.Scripts.SettingsStuff;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Architecture.Services
{
    public class RandomService
    {
        public Vector3 GetOffsetFor3Axis(MinMaxFloat xOffset, MinMaxFloat yOffset, MinMaxFloat zOffset)
        {
            float randX = GetValue(xOffset);
            float randY = GetValue(yOffset);
            float randZ = GetValue(zOffset);
            return new Vector3(randX, randY, randZ);
        }
        
        public Vector3 GetOffsetFor3Axis(MinMaxFloat offset)
        {
            float randX = GetValue(offset);
            float randY = GetValue(offset);
            float randZ = GetValue(offset);
            return new Vector3(randX, randY, randZ);
        }
        
        public float GetValue(MinMaxFloat offset) => 
            Random.Range(offset.MinValue, offset.MaxValue);
        
        public float GetValue(float min, float max)
        {
            MinMaxFloat offset = new MinMaxFloat(min, max);
            return Random.Range(offset.MinValue, offset.MaxValue);
        }

        public int GetValue(MinMaxInt minMaxValue) => 
            Random.Range(minMaxValue.MinValue, minMaxValue.MaxValue);
        
        public int GetValue(int min, int max) => 
            Random.Range(min, max);
        
        public Vector3 GetRandomDirection()
        {
            return new Vector3(GetValue(-1f, 1f), 0f, GetValue(-1f, 1f)).normalized;
        }

        public Vector2 GetRandomPointInsideCircle(float patrolRadius) => 
            Random.insideUnitCircle * patrolRadius;
    }
}