using UnityEngine;

namespace _Project.Scripts.Architecture.Services.StaticData
{
    public interface IStaticDataService
    {
        T GetStaticData<T>(string path) where T : ScriptableObject;
    }
}