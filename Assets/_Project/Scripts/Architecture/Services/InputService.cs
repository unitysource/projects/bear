using UnityEngine;

namespace _Project.Scripts.Architecture.Services
{
    public class InputService
    {
        private Joystick _joystick;
        
        public Vector3 GetInput()
        {
            return new Vector3(_joystick.Direction.x, 0f, _joystick.Direction.y);
        }

    }
}