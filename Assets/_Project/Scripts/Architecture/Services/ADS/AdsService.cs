using System;
using _Project.Scripts;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.Advertisements;
using Zenject;

public class AdsService : IUnityAdsListener, IAdsService
{
    private const string AndroidGameId = "4716089";
    private const string IOSGameId = "4716088";
    private const string RewardVideoPlacementId = "Rewarded_Android";

    private string _gameId;
    private Action _onVideoFinished;
    private ISaveLoadService _saveLoadService;

    private float _interTimer;
    private DataService _dataService;

    public event Action OnRewardVideoReady;

    [Inject]
    private void Construct(ISaveLoadService saveLoadService, DataService dataService)
    {
        _dataService = dataService;
        _saveLoadService = saveLoadService;
    }

    public void Initialize()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                _gameId = AndroidGameId;
                break;
            case RuntimePlatform.IPhonePlayer:
                _gameId = IOSGameId;
                break;
            case RuntimePlatform.OSXEditor:
                _gameId = AndroidGameId;
                break;
            case RuntimePlatform.WindowsEditor:
                _gameId = AndroidGameId;
                break;
            default:
                Debug.LogError("Unsupported platform for ads");
                break;
        }

        Advertisement.AddListener(this);
        Advertisement.Initialize(_gameId);


        Debug.Log("Initialize ads");
    }

    public void ShowRewardedVideo(Action onVideoFinished)
    {
        Advertisement.Show(RewardVideoPlacementId);
        _onVideoFinished = onVideoFinished;
    }

    public void ShowInterstitial(Action onVideoFinished)
    {
        if (!CanShowInterstitial() || !TimerIsReady) return;

        Advertisement.Show(RewardVideoPlacementId);
        _onVideoFinished = onVideoFinished;
    }

    private void ReloadInterTimer()
    {
        _interTimer = _dataService.WorldSettings.ReloadAdDuration;
    }

    public bool CanShowInterstitial() =>
        CommonTools.IntToBool(PlayerPrefs.GetInt(SaveKey.CanShowAd.ToString(), CommonTools.BoolToInt(true)));

    public void DisableAd()
    {
        PlayerPrefs.SetInt(SaveKey.CanShowAd.ToString(), CommonTools.BoolToInt(false));
    }

    private bool TimerIsReady =>
        _interTimer <= 0f;

    public bool IsRewardedVideoReady()
    {
        return Advertisement.IsReady(RewardVideoPlacementId);
    }

    public void OnUnityAdsReady(string placementId)
    {
        Debug.Log($"OnUnityAdsReady {placementId}");

        if (placementId == RewardVideoPlacementId)
            OnRewardVideoReady?.Invoke();
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.Log($"OnUnityAdsDidError {message}");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.Log($"OnUnityAdsDidStart {placementId}");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        switch (showResult)
        {
            case ShowResult.Failed:
                Debug.Log($"OnUnityAdsDidFinish {showResult}");
                AnalyticsManager.InterstitialFailed(_gameId);

                break;
            case ShowResult.Skipped:
                Debug.Log($"OnUnityAdsDidFinish {showResult}");
                break;
            case ShowResult.Finished:
                _onVideoFinished?.Invoke();
                AnalyticsManager.InterstitialShowed(_gameId);

                ReloadInterTimer();
                break;
            default:
                Debug.Log($"OnUnityAdsDidFinish {showResult}");
                break;
        }

        _onVideoFinished = null;
    }

    public void Tick()
    {
        if (_interTimer >= 0f)
        {
            _interTimer -= Time.deltaTime;
            // Debug.Log("_interTimer = " + _interTimer);
        }
    }
}