using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public interface IAdsService : ITickable
{
    event Action OnRewardVideoReady;
    void Initialize();
    void DisableAd();
    void ShowRewardedVideo(Action onVideoFinished);
    void ShowInterstitial(Action onVideoFinished = null);
    bool IsRewardedVideoReady();
    bool CanShowInterstitial();
}
