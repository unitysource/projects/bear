
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.Input
{
    internal interface IInputService
    {
        Vector3 GetInput();
        bool IsInputAboveZero();
    }
}