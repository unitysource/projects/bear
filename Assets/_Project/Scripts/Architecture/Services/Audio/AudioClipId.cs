﻿using UnityEngine;

namespace _Project.Scripts.Architecture.Services.Audio
{
    public enum AudioClipId
    {
        [Header("Interactive")] button_clicked = 0,
        level_done = 1,
        level_lose = 2,
        start_round = 3,
        portal_final = 4,
        portal_key = 5,
        user_buy = 6,
        tile_opened = 7,
        tool_work = 8,

        [Header("Hero")] coin_receive = 15,
        unit_placement = 16,
        // ranged_attack = 17,
        // ranged_attack_2 = 18,
        // melee_attack_1 = 19,
        // melee_attack_2 = 20,
        // melee_attack_3 = 21,

        hero_death = 22,
        entity_run_0 = 23,
        entity_run_1 = 24,
        entity_swim_0 = 25,
        entity_swim_1 = 26,
        crystal_hit = 27,
        rock_hit = 28,
        tree_hit = 29,
        entity_hit = 30,
        gold_hit = 31,

        [Header("Soundtracks")] soundtrack_menu = 50,
        soundtrack_gameplay = 51,
    }
}