using System;
using System.Collections.Generic;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Architecture.Services.Audio
{
    public class LoopedAudio : MonoBehaviour
    {
        [SerializeField] private int _id;
        [SerializeField] private AudioLinkType _audioLinkType;
        
        [SerializeField] private float _audioDelay;
        [SerializeField] private AudioClipId[] _clips;

        private float _audioTimer;
        private AudioService _audioService;

        public int ID => _id;

        [Inject]
        private void Construct(AudioService audioService)
        {
            _audioService = audioService;
        }

        public void FixedPlayAudio()
        {
            CommonTools.TimerActionLooped(ref _audioTimer, _audioDelay, PlayAudio);
        }

        private void PlayAudio()
        {
            switch (_audioLinkType)
            {
                case AudioLinkType.Loop:
                    _audioService.PlayLoopSound(_clips);
                    break;
                case AudioLinkType.Random:
                    _audioService.PlayRandomSound(_clips);
                    break;
            }
        }
    }

    public enum AudioLinkType
    {
        Loop,
        Random
    }
}