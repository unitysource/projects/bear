﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using Assets._Project.Scripts.Utilities;
using Assets._Project.Scripts.Utilities.Extensions;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace _Project.Scripts.Architecture.Services.Audio
{
    public class Sound
    {
        private Sound(AudioSource audioSource, AudioClipId clipId)
        {
            this.audioSource = audioSource;
            this.clipId = clipId;
        }

        public static Sound CreateInstance(AudioSource audioSource, AudioClipId clipId) =>
            new Sound(audioSource, clipId);

        public readonly AudioSource audioSource;
        public readonly AudioClipId clipId;
    }

    public class AudioService : MonoBehaviour
    {
        [SerializeField] private List<AudioClipEntity> _clips;

        [SerializeField] private AudioClip _background;
        [Range(0f, 1f)] [SerializeField] private float _backgroundVolume = 1f;
        private AudioSource _musicSource;
        private readonly List<Sound> _soundSources = new List<Sound>();
        private readonly List<AudioSource> _registeredAudioSources = new List<AudioSource>();
        private Coroutine _coroutine;

        private bool _isSoundsOn, _isMusicOn;
        private float _soundsVolumeMultiplier, _musicVolumeMultiplier;
        private ISaveLoadService _saveLoadService;

        [Inject]
        private void Construct(ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
        }

        private void Start()
        {
            Initialize();

            SceneManager.sceneLoaded += OnStartGame;
        }

        public void RegisterAudioSource(AudioSource audioSource)
        {
            _registeredAudioSources.Add(audioSource);
        }

        public void UnRegisterAudioSource(AudioSource audioSource)
        {
            _registeredAudioSources.Remove(audioSource);
        }

        private void Initialize()
        {
            _isMusicOn = _saveLoadService.GameConfig.MusicOn.ToBool();
            _isSoundsOn = _saveLoadService.GameConfig.SoundsOn.ToBool();

            UpdateSoundsMute();
            UpdateSoundsVolume();
        }

        public void PlayMusic()
        {
            _isMusicOn = CommonTools.IntToBool(_saveLoadService.GameConfig.MusicOn);
            _musicVolumeMultiplier = _saveLoadService.GameConfig.MusicVolumeMultiplier;

            _musicSource = gameObject.AddComponent<AudioSource>();
            _musicSource.loop = true;
            _musicSource.clip = _background;
            _musicSource.volume = _backgroundVolume * _musicVolumeMultiplier;
            _musicSource.mute = !_isMusicOn;
            _musicSource.Play();
        }

        public void UpdateMusicMute()
        {
            _isMusicOn = CommonTools.IntToBool(_saveLoadService.GameConfig.MusicOn);
            _musicSource.mute = !_isMusicOn;
        }

        public void UpdateMusicVolume()
        {
            _musicVolumeMultiplier = _saveLoadService.GameConfig.MusicVolumeMultiplier;
            float musicVolumeValue = _backgroundVolume * _musicVolumeMultiplier;
            _musicSource.volume = musicVolumeValue;
        }

        public void UpdateSoundsVolume()
        {
            _soundsVolumeMultiplier = _saveLoadService.GameConfig.SoundsVolumeMultiplier;
            foreach (var sound in _soundSources)
            {
                int index = _clips.FindIndex(a => (int) a.Id == (int) sound.clipId);
                sound.audioSource.volume = _clips[index].Volume * _soundsVolumeMultiplier;
            }

            foreach (var source in _registeredAudioSources)
            {
                var index = _clips.FindIndex(a => a.TargetAudioClip == source.clip);
                source.volume = _clips[index].Volume * _soundsVolumeMultiplier;
            }
        }

        public void UpdateSoundsMute()
        {
            _isSoundsOn = CommonTools.IntToBool(_saveLoadService.GameConfig.SoundsOn);
            _soundSources.ForEach(s => s.audioSource.mute = !_isSoundsOn);
        }

        public void PlayRandomSound(AudioClipId[] sounds)
        {
            var i = Random.Range(0, sounds.Length);
            var clipId = sounds[i];
            PlaySound((int) clipId);
        }

        public void PlayRandomSound(List<AudioClipEntity> clips)
        {
            var sounds = new AudioClipId[clips.Count];
            for (var x = 0; x < clips.Count; x++)
                sounds[x] = clips[x].Id;

            var i = Random.Range(0, sounds.Length);
            var clipId = sounds[i];
            PlaySound((int) clipId);
        }

        public void PlayLoopSound(AudioClipId[] clipIDs)
        {
            string key = clipIDs.Aggregate("", (current, id) => current + id);
            int oldValue = PlayerPrefs.GetInt(key);
            var i = CommonTools.GetNextValue(oldValue, clipIDs.Length);
            PlayerPrefs.SetInt(key, i);

            var clipId = clipIDs[i];
            PlaySound((int) clipId);
        }

        public void PlaySound(int clipId)
        {
            var index = _clips.FindIndex(a => (int) a.Id == clipId);
            bool isAdd = false;
            AudioSource audioSource = FindAudioSource();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
                isAdd = true;
            }

            SetupAudioSource(audioSource, _clips, index);

            Sound sound = Sound.CreateInstance(audioSource, (AudioClipId) clipId);
            if (isAdd)
                _soundSources.Add(sound);
            StartCoroutine(DeletedClip(audioSource, _clips[index].TargetAudioClip.length));
        }

        public void PlaySound(AudioClipId clipId, List<AudioClipEntity> clips = null)
        {
            clips ??= _clips;

            var index = clips.FindIndex(a => (int) a.Id == (int) clipId);
            bool isAdd = false;
            AudioSource audioSource = FindAudioSource();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
                isAdd = true;
            }

            SetupAudioSource(audioSource, clips, index);

            Sound sound = Sound.CreateInstance(audioSource, (AudioClipId) clipId);

            if (isAdd)
                _soundSources.Add(sound);
            StartCoroutine(DeletedClip(audioSource, clips[index].TargetAudioClip.length));
        }

        public void PlaySound(AudioClipId clipId, AudioSource audioSource, List<AudioClipEntity> clips = null)
        {
            clips ??= _clips;

            var index = clips.FindIndex(a => (int) a.Id == (int) clipId);

            SetupAudioSource(audioSource, clips, index);

            StartCoroutine(DeletedClip(audioSource, clips[index].TargetAudioClip.length));
        }

        private void SetupAudioSource(AudioSource audioSource, IReadOnlyList<AudioClipEntity> clips, int index)
        {
            audioSource.volume = clips[index].Volume * _soundsVolumeMultiplier;
            audioSource.clip = clips[index].TargetAudioClip;
            audioSource.mute = !_isSoundsOn;
            audioSource.Play();
        }

        public void StopSound(AudioClipId clipId, List<AudioClipEntity> clips = null)
        {
            clips ??= _clips;
            var index = clips.FindIndex(a => (int) a.Id == (int) clipId);
            AudioSource audioSource = FindAudioSourceByClipId(index, clips);
            if (audioSource != null)
                audioSource.clip = null;
        }

        public void StopSound(AudioSource audioSource)
        {
            audioSource.clip = null;
        }

        private AudioSource FindAudioSourceByClipId(int index, List<AudioClipEntity> clips)
        {
            clips ??= _clips;
            AudioSource audio = null;

            Sound sound =
                _soundSources.FirstOrDefault(source => source.audioSource.clip == clips[index].TargetAudioClip);
            return sound?.audioSource;
        }

        private static IEnumerator DeletedClip(AudioSource audioSource, float time)
        {
            yield return new WaitForSeconds(time);
            audioSource.clip = null;
        }

        private AudioSource FindAudioSource()
        {
            AudioSource audio = null;
            foreach (var source in _soundSources.Where(source => source.audioSource.clip == null))
            {
                audio = source.audioSource;
            }

            return audio;
        }

        private void OnStartGame(Scene scene, LoadSceneMode mode)
        {
            for (int i = 0; i < _soundSources.Count; i++)
                _soundSources.Remove(_soundSources[i]);
        }
    }
}