﻿using System;
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.Audio
{
    [Serializable]
    public class AudioClipEntity
    {
        [SerializeField] private AudioClipId id;
        [SerializeField] private AudioClip targetAudioClip;
        [SerializeField] private float volume = 1;

        public AudioClipId Id => id;

        public AudioClip TargetAudioClip => targetAudioClip;

        public float Volume => volume;
    }
}