using System;

namespace _Project.Scripts.Architecture.Services.SaveLoadService.Data
{
    [Serializable]
    public class PlayerData
    {
        public Action<int> OnCoinsUpdated;
        public Action<int, float> OnExperienceUpdated;

        public int Coins
        {
            get => _coins;
            set
            {
                _coins = value;
                OnCoinsUpdated?.Invoke(_coins);
            }
        }
        
        public int Level
        {
            get => _level;
            set
            {
                _level = value;
                OnExperienceUpdated?.Invoke(_level, _levelProgress);
            }
        }
        
        public float LevelProgress
        {
            get => _levelProgress;
            set
            {
                _levelProgress = value;
                OnExperienceUpdated?.Invoke(_level, _levelProgress);
            }
        }
        
        public int _coins = 1000;
        public int _level;
        public float _levelProgress;
    }
}