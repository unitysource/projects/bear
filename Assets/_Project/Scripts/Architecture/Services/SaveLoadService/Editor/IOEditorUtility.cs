using System.IO;
using _Project.Scripts.Architecture.Services.Save;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.SaveLoadService
{
    public static class CustomEditorMenuItem
    {
        [MenuItem("Data/Open data folder &o")]
        static void OpenDataFolder()
        {
            Debug.Log($"{Application.persistentDataPath}Open folder: ");
            if (!Directory.Exists(Application.persistentDataPath)) return;

            EditorUtility.RevealInFinder(Application.persistentDataPath);

            // Win
            /*System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
            {
                Arguments = string.Format("/C start {0}", Application.persistentDataPath),
                FileName = "explorer.exe"
            };*/

            // Mac
            System.Diagnostics.Process.Start(Application.persistentDataPath);
        }

        [MenuItem("Data/Clear Json data &;")]
        public static void ClearJsonData()
        {
            // PlayerPrefs.DeleteAll();

            string path = Path.Combine(Application.persistentDataPath, "JsonData");

            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);

                foreach (var file in files)
                {
                    File.Delete(file);
                    
                    // if(file )
                    // Debug.Log($"File '{file}' has been deleted.");
                }

                Debug.Log("<color=cyan>All json data is cleared</color>");
            }
        }

        /*
     [MenuItem("Data/Clear all data &;")]
        public static void ClearAllData()
        {
            PlayerPrefs.DeleteAll();

            if (Directory.Exists(Application.persistentDataPath))
            {
                string[] files = Directory.GetFiles(Application.persistentDataPath);

                foreach (var file in files)
                {
                    File.Delete(file);
                    Debug.Log($"File '{file}' has been deleted.");
                }
                Debug.Log("<color=cyan>All  data is cleared</color>");
            }
        }
         */

        [MenuItem("Custom Shortcuts/Clear Static Data &'")]
        public static void ClearData()
        {
            SaveManager.DeleteSave();
            Debug.Log("<color=cyan>All static data is cleared</color>");
        }
    }
}