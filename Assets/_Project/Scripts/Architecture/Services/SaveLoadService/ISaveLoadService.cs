using System;
using _Project.Scripts.Architecture.Services.SaveLoadService.Data;

namespace _Project.Scripts.Architecture.Services.SaveLoadService
{
    public interface ISaveLoadService
    {
        Action OnDataUpdated { get; set; }
        PlayerData PlayerData { get; }
        GameConfig GameConfig { get; }
        PlanetData PlanetData { get; }
        bool IsSaveFileExist(string path);
        void SaveAllData();
        void LoadAllData();
        void DeleteAll();
    }
}