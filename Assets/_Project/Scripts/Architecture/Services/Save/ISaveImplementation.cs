using System.Collections;

namespace _Project.Scripts.Architecture.Services.Save
{
    public interface ISave
    {
        void Save(bool isCleared);
        void Load();
        void DeleteSave();
        void ClearHashTable(Hashtable hashtable);
        bool HasKey(string key, out Hashtable dataHashtable);
        void SetValue(string key, string value, bool isCleared = true);
        int GetInt(string key, Hashtable dataHashtable);
        long GetInt64(string key, Hashtable dataHashtable);
        string GetString(string key, Hashtable dataHashtable);
        bool GetBool(string key, Hashtable dataHashtable);
        double GetDouble(string key, Hashtable dataHashtable);
        float GetFloat(string key, Hashtable dataHashtable);
    }
}