using System.Collections;
using System.Globalization;
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.Save
{
    public enum SaveKey
    {
        None,
        [Header("Cleared")] SoundActivation,
        SearchedCard,
        IsFirstGame,
        OpenedTilesAmount,

        [Header("Uncleared")] Default,
        LastQuitGameTime,
        OpenedTiles,
        CanShowAd,
        AllTimeInGame,
        OpenGameDateTime,
    }

    public static class SaveManager
    {
        private static readonly ISave saver = new XmlSave();

        // private static void Setup()
        // {
        //     // saver = new XmlSave();
        //     saver.Load();
        // }

        // private static bool IsSaverNull => saver == null;

        public static void SetInt(SaveKey key, int value, bool isCleared = true) =>
            saver.SetValue(key.ToString(), value.ToString(), isCleared);

        public static int GetInt(SaveKey key, int defaultValue = 0)
        {
            saver.Load();
            return saver.HasKey(key.ToString(), out var hashtable) ? saver.GetInt(key.ToString(), hashtable) : defaultValue;
        }

        public static void SetFloat(SaveKey key, float value, bool isCleared = true) =>
            saver.SetValue(key.ToString(), value.ToString(CultureInfo.InvariantCulture));

        public static float GetFloat(SaveKey key, float defaultValue = 0.0f)
        {
            saver.Load();
            return saver.HasKey(key.ToString(), out var hashtable) ? saver.GetFloat(key.ToString(), hashtable) : defaultValue;
        }

        public static void SetString(SaveKey key, string value, bool isCleared = true) => saver.SetValue(key.ToString(), value);

        public static string GetString(SaveKey key, string defaultValue = "")
        {
            saver.Load();
            return saver.HasKey(key.ToString(), out var hashtable) ? saver.GetString(key.ToString(), hashtable) : defaultValue;
        }

        public static void SetBool(SaveKey key, bool value, bool isCleared = true) => saver.SetValue(key.ToString(), value.ToString(), isCleared);

        public static bool GetBool(SaveKey key, bool defaultValue = false)
        {
            saver.Load();
            return saver.HasKey(key.ToString(), out var hashtable) ? saver.GetBool(key.ToString(), hashtable) : defaultValue;
        }

        public static void DeleteSave() => saver.DeleteSave();

        // public static void SetInt64(ClearedSaveKey key, long value) => saver.SetValue(key.ToString(), value.ToString());
        //
        // public static long GetInt64(ClearedSaveKey key, int defaultValue = 0) =>
        //     saver.HasKey(key.ToString()) ? saver.GetInt64(key.ToString()) : defaultValue;

        // public static void SetDouble(ClearedSaveKey key, double value) =>
        //     saver.SetValue(key.ToString(), value.ToString(CultureInfo.InvariantCulture));
        //
        // public static double GetDouble(ClearedSaveKey key, double defaultValue = 0.0f) =>
        //     saver.HasKey(key.ToString()) ? saver.GetDouble(key.ToString()) : defaultValue;

        // public static void Save() => saver.Save();
        // public static void Load() => saver.Load();
        // public static void ClearHashTable() => saver.ClearHashTable();
    }
}