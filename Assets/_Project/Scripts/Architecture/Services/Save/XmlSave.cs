using System;
using System.Collections;
using System.IO;
using System.Xml;
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.Save
{
    public class XmlSave : ISave
    {
        private const string clearedSaveFileName = "GameSettingsSaveCleared.xml";
        private const string unclearedSaveFileName = "GameSettingsSaveUncleared.xml";

        private readonly string clearedSaveFilePath;
        private readonly string unclearedSaveFilePath;

        private readonly Hashtable clearedSaveTable = new Hashtable();
        private readonly Hashtable unclearedSaveTable = new Hashtable();

        public XmlSave()
        {
            clearedSaveFilePath = Path.Combine(Application.persistentDataPath, clearedSaveFileName);
            unclearedSaveFilePath = Path.Combine(Application.persistentDataPath, unclearedSaveFileName);
        }

        public void Save(bool isCleared)
        {
            if (isCleared)
            {
                XmlDocument clearedXmlDoc = new XmlDocument();
                XmlElement root = clearedXmlDoc.CreateElement("SavedData");
                clearedXmlDoc.AppendChild(root);
                XmlElement clearedElement = clearedXmlDoc.CreateElement("ClearedData");
                root.AppendChild(clearedElement);
                AddChildren(clearedXmlDoc, clearedElement, clearedSaveTable);

                clearedXmlDoc.Save(clearedSaveFilePath);
            }

            else
            {
                XmlDocument unclearedXmlDoc = new XmlDocument();
                XmlElement unclearedRoot = unclearedXmlDoc.CreateElement("SavedData");
                unclearedXmlDoc.AppendChild(unclearedRoot);
                XmlElement unclearedElement = unclearedXmlDoc.CreateElement("UnclearedData");
                unclearedRoot.AppendChild(unclearedElement);
                AddChildren(unclearedXmlDoc, unclearedElement, unclearedSaveTable);

                unclearedXmlDoc.Save(unclearedSaveFilePath);
            }
        }

        private void AddChildren(XmlDocument xmlDoc, XmlElement xmlElement, Hashtable hashtable)
        {
            foreach (DictionaryEntry item in hashtable)
            {
                XmlElement saveNode = xmlDoc.CreateElement(item.Key.ToString());
                saveNode.InnerText = item.Value.ToString();
                xmlElement.AppendChild(saveNode);
            }
        }

        public void Load()
        {
            if (File.Exists(clearedSaveFilePath))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(clearedSaveFilePath);
                XmlNode clearedNode = xmlDoc.DocumentElement.FirstChild;

                foreach (XmlNode node in clearedNode)
                    clearedSaveTable[node.Name] = node.InnerText;
            }

            if (File.Exists(unclearedSaveFilePath))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(unclearedSaveFilePath);
                XmlNode clearedNode = xmlDoc.DocumentElement.FirstChild;

                foreach (XmlNode node in clearedNode)
                    unclearedSaveTable[node.Name] = node.InnerText;
            }
        }

        public void SetValue(string key, string value, bool isCleared = true)
        {
            if (isCleared)
            {
                clearedSaveTable[key] = value;
                Save(true);
            }
            else
            {
                unclearedSaveTable[key] = value;
                Save(false);
            }
        }

        public int GetInt(string key, Hashtable dataHashtable)
        {
            return Convert.ToInt32(dataHashtable[key]);
        }

        public long GetInt64(string key, Hashtable dataHashtable)
        {
            return Convert.ToInt64(dataHashtable[key]);
        }

        public string GetString(string key, Hashtable dataHashtable)
        {
            return dataHashtable[key].ToString();
        }

        public bool GetBool(string key, Hashtable dataHashtable)
        {
            return Convert.ToBoolean(dataHashtable[key]);
        }

        public double GetDouble(string key, Hashtable dataHashtable)
        {
            return Convert.ToDouble(dataHashtable[key]);
        }

        public float GetFloat(string key, Hashtable dataHashtable)
        {
            return Convert.ToSingle(dataHashtable[key]);
        }

        public void DeleteSave()
        {
            if (File.Exists(clearedSaveFilePath))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(clearedSaveFilePath);
                foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
                {
                    if (node.Name == "ClearedData")
                        xmlDoc.DocumentElement.RemoveChild(node);
                }

                xmlDoc.Save(clearedSaveFilePath);
                ClearHashTable(clearedSaveTable);
            }
        }

        public void ClearHashTable(Hashtable saveTable) =>
            saveTable.Clear();

        public bool HasKey(string key, out Hashtable dataHashtable)
        {
            dataHashtable = null;
            if (clearedSaveTable.ContainsKey(key))
            {
                dataHashtable = clearedSaveTable;
                return true;
            }

            if (unclearedSaveTable.ContainsKey(key))
            {
                dataHashtable = unclearedSaveTable;
                return true;
            }

            return false;
        }

        // private void Encrypt(string path, MemoryStream xmlStream)
        // {
        //     using Aes encryptor = Aes.Create();
        //     Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey,
        //         new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
        //     encryptor.Key = pdb.GetBytes(32);
        //     encryptor.IV = pdb.GetBytes(16);
        //     using FileStream fsOutput = new FileStream(path, FileMode.Create);
        //     using CryptoStream cs = new CryptoStream(fsOutput, encryptor.CreateEncryptor(),
        //         CryptoStreamMode.Write);
        //     int data;
        //     while ((data = xmlStream.ReadByte()) != -1) cs.WriteByte((byte) data);
        // }
        //
        // private void Decrypt(string path, MemoryStream xmlStream)
        // {
        //     using Aes encryptor = Aes.Create();
        //     Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey,
        //         new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
        //     encryptor.Key = pdb.GetBytes(32);
        //     encryptor.IV = pdb.GetBytes(16);
        //     using FileStream fsInput = new FileStream(path, FileMode.Open);
        //     using CryptoStream cs = new CryptoStream(fsInput, encryptor.CreateDecryptor(), CryptoStreamMode.Read);
        //     int data;
        //     while ((data = cs.ReadByte()) != -1) 
        //         xmlStream.WriteByte((byte) data);
        //
        //     xmlStream.Flush();
        //     xmlStream.Position = 0;
        // }
    }
}