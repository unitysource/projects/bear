using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities.Constants;

namespace _Project.Scripts.Architecture.Services
{
    public class DataService
    {
        public WorldSettings WorldSettings { get; }
        public UISettings UISettings { get; }

        public DataService(AssetService assetService)
        {
            WorldSettings = assetService.GetObjectByType<WorldSettings>(AssetPath.GlobalSettingsPath);
            UISettings = assetService.GetObjectByType<UISettings>(AssetPath.GlobalSettingsPath);
        }
    }
}