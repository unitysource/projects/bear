using System.Collections.Generic;
using Facebook.Unity;
using GameAnalyticsSDK;

namespace _Project.Scripts
{
    public static class AnalyticsManager
    {
        private const string Progression = "Level_0";

        public static void GameStarted()
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Progression);
        }

        public static void GameFailed(FailType failType)
        {
            var fields = new Dictionary<string, object>
            {
                {"fail_type", failType}
            };

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, Progression, fields);
        }

        public static void GameCompleted(CompleteType completeType)
        {
            var fields = new Dictionary<string, object>
            {
                {"complete_type", completeType}
            };

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Progression, fields);
        }

        public static void GameClosed(float percent, float sessionTime)
        {
            var fields = new Dictionary<string, object>
            {
                {"open_location_percent", percent},
                {"session_time", sessionTime}
            };

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Close, Progression, fields);
        }

        public static void GameRestarted()
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Restart, Progression);
        }

        public static void AdBlockBought()
        {
            GameAnalytics.NewBusinessEventGooglePlay("EUR", 499, "Ad", "AdBlock", "FromMenu", null, null);
        }

        public static void InterstitialShowed(string placementId)
        {
            GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Interstitial, "unityads", placementId);
        }

        public static void InterstitialFailed(string placementId)
        {
            GameAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Interstitial, "unityads", placementId);
        }

        public static void InterstitialClicked(string placementId)
        {
            GameAnalytics.NewAdEvent(GAAdAction.Clicked, GAAdType.Interstitial, "unityads", placementId);
        }

        public static void GameInitialize()
        {
            GameAnalytics.Initialize();
        }
        
        public static void FBInitialize()
        {
            if (FB.IsInitialized)
                FB.ActivateApp();
            else
                FB.Init(FB.ActivateApp);
        }
    }

    public enum FailType
    {
        FailInWater,
        FailEnemy
    }

    public enum CompleteType
    {
        CompletePortal,
        CompleteOpenAllTiles
    }
}