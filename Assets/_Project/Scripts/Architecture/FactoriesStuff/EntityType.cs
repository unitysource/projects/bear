namespace _Project.Scripts.FactoriesStuff
{
    public enum EntityType
    {
        None = 0,
        Hero = 1,
        Cryper = 2,
        Bear = 3,
        Zombie = 4,
        Worm = 5,
    }
}