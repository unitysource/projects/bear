using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Services;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.FactoriesStuff
{
    public interface IEntityFactory
    {
        void Load();
        void WarmPool();
        GameObject Create(string name);
        void Release(GameObject obj, string name);
    }

    public class EntityPoolFactory : IEntityFactory
    {
        private const string HeroEntity = "Entities/Hero";
        private const string CryperEntity = "Entities/Enemies/Cryper";
        private const string ZombieEntity = "Entities/Enemies/Zombie";
        private const string BearEntity = "Entities/Pets/Bear";
        private const string WormEntity = "Entities/Pets/Worm";

        private const string ContainerName = "EntitiesPool";

        private readonly AssetService _assetService;
        private readonly IPoolService _poolService;

        private Dictionary<string, GameObject> _resources;

        private const int Amount = 10;

        public EntityPoolFactory(AssetService assetService, IPoolService poolService)
        {
            _assetService = assetService;
            _poolService = poolService;
            
            Load();
            WarmPool();
        }

        public void Load()
        {
            _resources = new Dictionary<string, GameObject>
            {
                [EntityType.Cryper.ToString()] = _assetService.GetObjectByName(CryperEntity),
                [EntityType.Bear.ToString()] = _assetService.GetObjectByName(BearEntity),
                [EntityType.Zombie.ToString()] = _assetService.GetObjectByName(ZombieEntity),
                [EntityType.Worm.ToString()] = _assetService.GetObjectByName(WormEntity),
            };
        }

        public void WarmPool()
        {
            foreach (var name in _resources.Keys)
                _poolService.FillPool(new PoolInfo(name, Amount, _resources[name], ContainerName));
        }

        public GameObject Create(string name) => 
            _poolService.GetPoolObject(name);

        public void Release(GameObject obj, string name) => 
            _poolService.ReturnToPool(obj, name);
    }
}