using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Services;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.FactoriesStuff
{
    public interface IResourcesFactory
    {
        void Load();
        void WarmPool();
        GameObject Create(string name);
        void Release(GameObject obj, string name);
    }

    public class ResourcesPoolFactory : IResourcesFactory
    {
        private const string WoodResource = "GameResurces/WoodResource";
        private const string StoneResource = "GameResurces/StoneResource";
        private const string IronResource = "GameResurces/IronResource";
        private const string CrystalResource = "GameResurces/CrystalResource";
        private const string PlankResource = "GameResurces/PlankResource";
        private const string GoldResource = "GameResurces/GoldResource";

        private const string ContainerName = "ResourcesPool";

        private readonly AssetService _assetService;
        private readonly IPoolService _poolService;

        private Dictionary<string, GameObject> _resources;

        private const int Amount = 100;

        public ResourcesPoolFactory(AssetService assetService, IPoolService poolService)
        {
            _assetService = assetService;
            _poolService = poolService;
        }

        public void Load()
        {
            _resources = new Dictionary<string, GameObject>
            {
                [GameResourceType.Wood.ToString()] = _assetService.GetObjectByName(WoodResource),
                [GameResourceType.Stone.ToString()] = _assetService.GetObjectByName(StoneResource),
                [GameResourceType.Plank.ToString()] = _assetService.GetObjectByName(PlankResource),
                [GameResourceType.Iron.ToString()] = _assetService.GetObjectByName(IronResource),
                [GameResourceType.K_131.ToString()] = _assetService.GetObjectByName(CrystalResource),
                [GameResourceType.Gold.ToString()] = _assetService.GetObjectByName(GoldResource),
            };
        }

        public void WarmPool()
        {
            foreach (var name in _resources.Keys)
                _poolService.FillPool(new PoolInfo(name, Amount, _resources[name], ContainerName));
        }

        public GameObject Create(string name) => 
            _poolService.GetPoolObject(name);

        public void Release(GameObject obj, string name) => 
            _poolService.ReturnToPool(obj, name);
    }
}