using System.Collections;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.SettingsStuff;
using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace _Project.Scripts
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField] private MinMaxFloat _zoomValue;
        [SerializeField] private float _zoomDuration;

        [SerializeField] private CinemachineVirtualCamera _mainVirtualCamera;
        // [SerializeField] private CinemachineVirtualCamera _playWindowVirtualCamera;

        [SerializeField] private CinemachineVirtualCamera _virtualConnectorCamera;
        private Transform _heroTransform;
        // private IUIService _uiService;

        [Inject]
        private void Construct(HeroMovement heroMovement, IUIService uiService)
        {
            // _uiService = uiService;
            _heroTransform = heroMovement.transform;
            SetupTarget(_heroTransform);
            SetupWorldUp();
            // ZoomOut();
            
            // _uiService.AddListener(UIEventType.GameplayStartedAction, OnGameplayStarted);
        }

        // private void OnGameplayStarted(Hashtable _)
        // {
        //     _playWindowVirtualCamera.gameObject.SetActive(false);
        //     _mainVirtualCamera.gameObject.SetActive(true);
        // }

        private void SetupWorldUp()
        {
            Camera.main.GetComponent<CinemachineBrain>().m_WorldUpOverride = _heroTransform;
        }

        private void SetupTarget(Transform target)
        {
            _mainVirtualCamera.Follow = target;
            _mainVirtualCamera.LookAt = target;
        }

        public void ZoomIn(Transform target)
        {
            _virtualConnectorCamera.Follow = target;
            _virtualConnectorCamera.LookAt = target;

            _virtualConnectorCamera.gameObject.SetActive(true);
            _mainVirtualCamera.gameObject.SetActive(false);
        }

        public void ZoomOut()
        {
            _virtualConnectorCamera.gameObject.SetActive(false);
            _mainVirtualCamera.gameObject.SetActive(true);

            // _virtualConnectorCamera.LookAt = _heroMovement.transform;
            // _virtualConnectorCamera.Follow = _heroMovement.transform;
        }

        public void StandZoom()
        {
            _mainVirtualCamera.DOKill();
            DOTween.To(() => _mainVirtualCamera.m_Lens.FieldOfView,
                x => _mainVirtualCamera.m_Lens.FieldOfView = x,
                _zoomValue.MinValue, _zoomDuration).Play();
        }

        public void StandUnZoom()
        {
            _mainVirtualCamera.DOKill();
            DOTween.To(() => _mainVirtualCamera.m_Lens.FieldOfView,
                x => _mainVirtualCamera.m_Lens.FieldOfView = x,
                _zoomValue.MaxValue, _zoomDuration).Play();
        }
    }
}