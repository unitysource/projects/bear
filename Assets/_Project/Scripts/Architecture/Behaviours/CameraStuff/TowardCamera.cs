using UnityEngine;

namespace _Project.Scripts.CameraStuff
{
    public class TowardCamera : MonoBehaviour
    {
        [SerializeField] private bool _onlyYAxis;

        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
        }

        public void FixedUpdate()
        {
            var rotation = _camera.transform.rotation;

            if (_onlyYAxis)
            {
                Vector3 upAxis = transform.up;
                transform.rotation =
                    Quaternion.LookRotation(-Vector3.Cross(upAxis, Vector3.Cross(upAxis, _camera.transform.forward)),
                        upAxis);
            }
            else
                transform.LookAt(transform.position + rotation * Vector3.forward, rotation * Vector3.up);
        }
    }
}