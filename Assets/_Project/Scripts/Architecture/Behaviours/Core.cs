﻿using System;
using System.Globalization;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using Facebook.Unity;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject; // using GameAnalyticsSDK;

namespace _Project.Scripts
{
    public class Core : MonoBehaviour
    {
        [SerializeField] private Transform _gameCanvas;
        [SerializeField] private Hexsphere _hexsphere;
        [SerializeField] private EntitiesBehaviour _entities;

        private IUIService _uiService;
        private DataService _dataService;
        private DiContainer _diContainer;
        private ISaveLoadService _saveLoadService;
        private IAdsService _adsService;
        private IIAPService _iapService;
        private Joystick _joystick;
        private AudioService _audioService;

        [Inject]
        private void Construct(AssetService assetService, DataService dataService, IUIService uiService,
            DiContainer diContainer, ISaveLoadService saveLoadService, IAdsService adsService, IIAPService iapService,
            Joystick joystick, AudioService audioService)
        {
            _audioService = audioService;
            _joystick = joystick;
            _saveLoadService = saveLoadService;
            _diContainer = diContainer;
            _dataService = dataService;
            _uiService = uiService;
            _uiService.Initialize(_gameCanvas);
            _adsService = adsService;
            _adsService.Initialize();
            _iapService = iapService;
            _iapService.Initialize();
        }

        private void Awake()
        {
            AnalyticsManager.FBInitialize();
            AnalyticsManager.GameInitialize();
        }

        private void Start()
        {
            SaveManager.SetString(SaveKey.OpenGameDateTime, DateTime.Now.ToString(CultureInfo.InvariantCulture));

            SetTargetFameRate();

            Time.timeScale = 1f;
            _joystick.CanUse = true;

            _audioService.PlayMusic();

            InitializeUI();

            _hexsphere.Initialize();
            _entities.Initialize();

            SceneManager.sceneUnloaded += OnSceneUnloaded;

            AnalyticsManager.GameStarted();
        }

        private void InitializeUI()
        {
            _uiService.OpenWindow(WindowType.Open, true, false);
            _uiService.OpenWindow(WindowType.PetPointers, true, false);
        }

        private static void SetTargetFameRate() =>
            Application.targetFrameRate = 400;

        private void OnSceneUnloaded(Scene current)
        {
            QuitedCallback();
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        private void OnApplicationQuit()
        {
            QuitedCallback();
        }

        private void QuitedCallback()
        {
            CommonTools.SaveCurrentTime();
            SaveManager.SetString(SaveKey.SearchedCard, GameResourceType.None.ToString());
            _saveLoadService.SaveAllData();

            float sessionTime = PlayerPrefs.GetFloat(SaveKey.AllTimeInGame.ToString());
            double delta = (DateTime.Now - DateTime.Parse(SaveManager.GetString(SaveKey.OpenGameDateTime)))
                .TotalSeconds;
            float allSessionTime = sessionTime + (float) delta;
            PlayerPrefs.SetFloat(SaveKey.AllTimeInGame.ToString(), allSessionTime);

            AnalyticsManager.GameClosed(_hexsphere.GetOpenTilesPercent, allSessionTime);
        }
    }
}