﻿using UnityEngine;
using Zenject;

namespace _Project.Packs.HexPlanet.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class GravityBody : MonoBehaviour
    {
        private GravityAttractor _planetGravity;
        private Rigidbody _rb;
        private bool _shouldAttract = true;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            Init();
        }

        [Inject]
        private void Construct(GravityAttractor gravityAttractor)
        {
            _planetGravity = gravityAttractor;
        }

        private void Init()
        {
            _rb.useGravity = false;
            _rb.constraints = RigidbodyConstraints.FreezeRotation;
        }

        private void FixedUpdate()
        {
            if (_shouldAttract)
            {
                _planetGravity.Attract(_rb);
            }
        }

        public void ChangeAttractActivation(bool isActive) => _shouldAttract = isActive;

        public bool IsAttractActive() => _shouldAttract;
    }
}