﻿using UnityEngine;

namespace _Project.Packs.HexPlanet.Scripts
{
    public class GravityAttractor : MonoBehaviour
    {
        [SerializeField] private float _gravity = -9.8f;

        public void Attract(Rigidbody body)
        {
            var gravityUp = (body.position - transform.position).normalized;
            var localUp = body.transform.up;

            // Apply downwards gravity to body
            body.AddForce(gravityUp * _gravity);
            // Allign bodies up axis with the centre of planet
            body.rotation = Quaternion.FromToRotation(localUp, gravityUp) * body.rotation;
        }
    }
}