﻿using _Project.Scripts.Behaviours;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.GameTools;
using UnityEngine;

namespace _Project.Scripts
{
    public class ObserverWithHero : MonoBehaviour
    {
        private IHeroInteractable _heroInteractable;
        [HideInInspector] public bool IsHeroInside;

        public SphereCollider Collider { get; private set; }

        public void Setup(IHeroInteractable heroInteractable)
        {
            _heroInteractable = heroInteractable;
            Collider = GetComponent<SphereCollider>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<HeroInventory>(out _))
            {
                _heroInteractable.HeroTriggerEnter();
                IsHeroInside = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent<HeroInventory>(out _))
            {
                _heroInteractable.HeroTriggerExit();
                IsHeroInside = false;
            }
        }

        public void Activate(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}