using System.Linq;
using Assets._Project.Scripts.Utilities.Constants;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Architecture.Behaviours
{
    public class PrefabsChanger : MonoBehaviour
    {
        [SerializeField] private GameObject _prefab;
        
        // [Button("Change Objects")]
        // private void Change()
        // {
        //     var objectsToChange = FindObjectsOfType<GameObject>().Where(o =>o.CompareTag(Tags.ToDestroy)).ToList();
        //     for (int i = 0; i < objectsToChange.Count; i++)
        //     {
        //         var o = objectsToChange[i];
        //         var instance = (GameObject) PrefabUtility.InstantiatePrefab(_prefab);
        //         Transform instanceTransform = instance.transform;
        //         instanceTransform.SetParent(o.transform.parent, true);
        //         instanceTransform.SetPositionAndRotation(o.transform.position, o.transform.rotation);
        //         instanceTransform.localScale = o.transform.localScale;
        //         
        //         // DestroyImmediate(o);
        //     }
        // }
    }
}