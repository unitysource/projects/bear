﻿using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Architecture.SettingsStuff;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff
{
    public class EntityDeath : MonoBehaviour
    {
        [SerializeField] private EntityModel _model;
        [SerializeField] private DeathSettings _deathSettings;
        [SerializeField] private Renderer _renderer;

        private Material _material;

        private IPoolService _poolService;
        private string _deathParticlesName;
        private float _upParticlesPos;

        [Inject]
        private void Construct(IPoolService poolService)
        {
            _poolService = poolService;

            Initialize();
        }

        private void Initialize()
        {
            _deathParticlesName = _deathSettings.DeathParticlesPrefab.name;
            _material = _renderer.material;
            _upParticlesPos = 1f;
        }

        public void Deactivate()
        {
            var particleTransform = SetupParticles();
            particleTransform.DOLocalMoveY(0f, _deathSettings.DeathDuration/2).SetEase(_deathSettings.MoveCurve).Play();
            _material.DOFloat(0f, "_Fill", _deathSettings.DeathDuration/2).From(1f).SetEase(Ease.Linear).Play();
        }

        private Transform SetupParticles()
        {
            var particle = _poolService.GetPoolObject(_deathParticlesName).GetComponent<ParticleSystem>();
            particle.gameObject.SetActive(true);
            particle.GetComponent<ParticlesReturnToPool>().Initialize(_deathParticlesName);
            var particleMain = particle.main;
            particleMain.duration = _deathSettings.DeathDuration;
            particleMain.startColor =
                new ParticleSystem.MinMaxGradient(_deathSettings.ParticlesColor[0], _deathSettings.ParticlesColor[1]);
            Transform particleTransform;
            (particleTransform = particle.transform).SetParent(transform);
            particleTransform.localPosition = Vector3.up * _upParticlesPos;
            particleTransform.localEulerAngles = Vector3.zero;
            particle.Play();
            return particleTransform;
        }

        public void Activate()
        {
            _material.DOFloat(1f, "_Fill", _deathSettings.DeathDuration/2).From(0f).Play();
            var particleTransform = SetupParticles();
            particleTransform.DOLocalMoveY(_upParticlesPos, _deathSettings.DeathDuration/2).SetEase(_deathSettings.MoveCurve).Play();
        }
    }
}