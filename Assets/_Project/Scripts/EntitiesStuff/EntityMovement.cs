﻿using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public abstract class EntityMovement : MonoBehaviour
    {
        [Header("Properties")] [SerializeField]
        protected float _moveSpeed;

        [SerializeField] protected float _rotationSpeed;
        [SerializeField] protected ParticleSystem _moveParticles;
        [SerializeField] protected ParticleSystem _swimParticles;

        [Header("Sub Behaviours")] [SerializeField]
        protected EntityModel _model;

        protected Rigidbody _rb;
        private EntityAnimator _entityAnimator;
        protected Transform _cashedTransform;
        protected Health _health;

        protected Vector3 Direction { get; set; }

        public EntityModel Model => _model;

        private void Start()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            _entityAnimator = GetComponent<EntityAnimator>();
            _rb = GetComponent<Rigidbody>();
            _cashedTransform = transform;
            _health = GetComponent<Health>();
        }

        protected void AnimateMovement(Vector3 moveDir)
        {
            _entityAnimator.SetVelocity(moveDir.magnitude);
        }

        public void RotateByDirection(Vector3 point)
        {
            var rot = Quaternion.Euler(0, Mathf.Atan2(-point.z, point.x) * 180 / Mathf.PI + 90, 0);
            _model.SimpleRotate(rot, _rotationSpeed);
        }

        public abstract void Move();

        protected bool IsInputAboveZero() => Direction.magnitude > 0.01f;

        protected Vector3 GetInput() => new Vector3(Direction.x, 0, Direction.z);

        public void ResetDirection() => Direction = Vector3.zero;

        public void MoveToPoint(Vector3 at, float duration)
        {
            _cashedTransform.DOMove(at, duration).SetEase(Ease.Linear).Play();
        }
    }

    public enum EntitySurfaceState
    {
        Grounded,
        Swimming
    }
}