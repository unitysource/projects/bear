using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public class EntityModel : MonoBehaviour
    {
        public void SimpleRotate(Quaternion lookRotation, float rotationSpeed)
        {
            transform.localRotation =
                Quaternion.Slerp(transform.localRotation, lookRotation, rotationSpeed * Time.deltaTime);
        }

        public void Deactivate(float duration)
        {
            transform.DOScale(0f, duration).Play().SetEase(Ease.OutSine);
        }

        public void Activate()
        {
            
        }
    }
}