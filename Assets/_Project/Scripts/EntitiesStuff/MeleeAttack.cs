using _Project.Scripts.Architecture.Services.Audio;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff
{
    [RequireComponent(typeof(EntityAnimator))]
    public class MeleeAttack : MonoBehaviour
    {
        private EntityAnimator _entityAnimator;
        
        private AudioService _audioService;

        [Inject]
        private void Construct(AudioService audioService)
        {
            _audioService = audioService;
        }

        private void Awake()
        {
            _entityAnimator = GetComponent<EntityAnimator>();
        }

        public void Attack(Health health, float damage)
        {
            _entityAnimator.Attack();
            DOVirtual.DelayedCall(0.15f, () =>
            {
                _audioService.PlaySound(AudioClipId.entity_hit);
                health.TakeDamage(damage, DamageType.Entity);
            });
        }
    }
}