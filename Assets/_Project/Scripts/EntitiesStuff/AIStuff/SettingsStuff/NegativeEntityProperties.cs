using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AI
{
    [CreateAssetMenu(fileName = "NegativeEntityProperties", menuName = "AI/NegativeEntityProperties")]
    public class NegativeEntityProperties : ScriptableObject
    {
        [Header("Spawn Stuff")]
        [SerializeField] private float _respawnDuration;
        [SerializeField] private ParticleSystem _activationParticles;
        [Header("Attack Stuff")]
        [SerializeField] private int _damage = 1;
        [SerializeField] private float _attackDelay = 3f;
        
        [Header("KnockBack")]
        [SerializeField] private float _knockBackForce;
        

        public float RespawnDuration => _respawnDuration;

        public ParticleSystem ActivationParticles => _activationParticles;

        public float AttackDelay => _attackDelay;

        public int Damage => _damage;

        public float KnockBackForce => _knockBackForce;
    }
}