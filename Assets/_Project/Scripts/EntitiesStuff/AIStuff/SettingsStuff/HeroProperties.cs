using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AI
{
    [CreateAssetMenu(fileName = "HeroProperties", menuName = "AI/HeroProperties")]
    public class HeroProperties : ScriptableObject
    {
        [SerializeField] private float _knockBackForce;
        [SerializeField] private float _waterDeathTime;

        public float KnockBackForce => _knockBackForce;

        public float WaterDeathTime => _waterDeathTime;
    }
}