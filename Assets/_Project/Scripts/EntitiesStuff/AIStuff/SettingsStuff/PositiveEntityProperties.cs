using System.Runtime.Remoting.Messaging;
using _Project.Scripts.SettingsStuff;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AI
{
    [CreateAssetMenu(fileName = "PositiveEntityProperties", menuName = "AI/PositiveEntityProperties")]
    public class PositiveEntityProperties : ScriptableObject
    {
        [Header("Spawn Stuff")]
        [SerializeField] private float _respawnDuration;
        [SerializeField] private ParticleSystem _activationParticles;
        [Header("Attack Stuff")]
        [SerializeField] private int _damage = 1;
        [SerializeField] private float _attackDelay = 3f;
        [SerializeField, Range(1, 30)] private float _seeHeroDistance = 2f;
        
        [Header("Patrol Stuff")]
        [SerializeField] private float _patrolRadius;
        [SerializeField, Range(2, 20)] private float _wanderBeginDelay;

        [Header("Fear Stuff")] [SerializeField]
        private float fearRadius;
        
        [Header("KnockBack")]
        [SerializeField] private float _knockBackForce;

        [SerializeField] private float _thinkDelay = 0.2f;
        public float ThinkDelay => _thinkDelay;


        public float RespawnDuration => _respawnDuration;

        public ParticleSystem ActivationParticles => _activationParticles;

        public float AttackDelay => _attackDelay;

        public int Damage => _damage;

        public float SeeHeroDistance => _seeHeroDistance;
        public float PatrolRadius => _patrolRadius;

        public float FearRadius => fearRadius;

        public float WanderBeginDelay => _wanderBeginDelay;

        public float KnockBackForce => _knockBackForce;
    }
}