using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using Assets._Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class PositiveIdleAIState : IdleAIState
    {
        private readonly PositiveAI _ai;
        private readonly Type _downtimeState;

        private AIMovement _movement;
        private float _startWanderTimer;
        private readonly float _thinkDelay;
        private float _thinkDelayTimer;

        public PositiveIdleAIState(PositiveAI ai, Type downtimeState, float thinkDelay) : base(ai.gameObject)
        {
            _thinkDelay = thinkDelay;
            _downtimeState = downtimeState;
            _ai = ai;
            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
        }

        public override void EnterState()
        {
            _startWanderTimer = 0f;
            _movement.ResetDirection();
            ResetThinkTimer();
        }

        private void ResetThinkTimer()
        {
            _thinkDelayTimer = 0f;
        }

        public override Type FixedTick()
        {
            if (_thinkDelayTimer >= _thinkDelay)
            {
                if (_ai.IsTamed)
                {
                    if (_startWanderTimer >= _ai.EntityProperties.WanderBeginDelay)
                        return _downtimeState;

                    _startWanderTimer += Time.fixedDeltaTime;
                }

                if (_ai.HaveTarget(out Vector3 _))
                {
                    return _ai.IsTamed ? typeof(ChaseAIState) : typeof(FearAIState);
                }

                if (!_ai.IsSeeHero() && _ai.IsTamed)
                    return typeof(ChaseHeroAIState);
                
                ResetThinkTimer();
            }

            _thinkDelayTimer += Time.deltaTime;

            return null;
        }
    }
}