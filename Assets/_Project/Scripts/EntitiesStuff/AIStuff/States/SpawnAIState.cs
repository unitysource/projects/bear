using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class SpawnAIState : BaseAIState
    {
        private readonly AIBase _ai;
        private readonly EntityModel _model;

        private AiHealth _health;
        private AiInteractable _aiInteractable;
        private readonly GameObject _pointer;
        private readonly Collider[] _colliders;

        public SpawnAIState(AIBase ai, EntityModel model, Collider[] colliders, GameObject pointer) 
            : base(ai.gameObject)
        {
            _colliders = colliders;
            _pointer = pointer;
            _model = model;
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            if (_ai.UseHealthUI) _health = _ai.GetComponent<AiHealth>();
            _aiInteractable = _ai.GetComponent<AiInteractable>();
        }

        public override void EnterState()
        {
            if (_pointer != null)
                _pointer.SetActive(true);

            foreach (var col in _colliders) 
                if(col.isTrigger) col.enabled = true;
            
            _ai.transform.position = _ai.SpawnPoint;

            if (_ai.UseHealthUI) _health.Initialize();
            if (_ai.IsUseInteraction()) _aiInteractable.CanInteract = true;
        }

        public override Type Tick() => typeof(IdleAIState);
    }
}