﻿using System;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.Services;
using Assets._Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class CirclePatrolAIState : BaseAIState, IGizmos
    {
        private readonly PositiveAI _ai;
        private AIMovement _positiveAIMovement;
        private Vector3 _point;
        private readonly float _patrolRadius;
        private float _degrees;
        private float _timer;
        private float _findNextPointDelay = 0.15f;
        private const float ReachedDistance = 0.3f;

        public CirclePatrolAIState(PositiveAI ai, float patrolRadius)
            : base(ai.gameObject)
        {
            _patrolRadius = patrolRadius;
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            _positiveAIMovement = _ai.GetComponent<AIMovement>();
            _ai.GizmosService.AddGizmo(this);
        }

        public override void EnterState()
        {
            // SetNextPoint();
        }

        public override Type FixedTick()
        {
            CommonTools.TimerActionLooped(ref _timer, _findNextPointDelay, action: SetNextPoint);
            _positiveAIMovement.CalculateDirection(_point);
            _positiveAIMovement.Rotate(_point);
            _positiveAIMovement.Move();

            Debug.DrawLine(_ai.transform.position, _point, Color.red);

            if (_ai.HaveTarget(out Vector3 _) && _ai.IsTamed)
                return typeof(ChaseAIState);

            if (!_ai.IsSeeHero())
                return typeof(ChaseHeroAIState);

            return null;
        }

        private bool IsPointReached =>
            Vector3.Distance(_point, _ai.transform.position) <= ReachedDistance;

        private void SetNextPoint()
        {
            // var offset = _randomService.GetRandomPointInsideCircle(_patrolRadius);
            const float delta = 0.2f;
            var x = (_patrolRadius - delta) * Mathf.Cos(_degrees * Mathf.PI / 180);
            var y = (_patrolRadius - delta) * Mathf.Sin(_degrees * Mathf.PI / 180);
            _point = _ai.HeroTransform.position + _ai.transform.TransformDirection(new Vector3(x, 0f, y));
            _degrees += 10;
        }

        public void DrawGizmo()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(_ai.HeroTransform.position, _patrolRadius);
            Gizmos.DrawSphere(_point, 0.2f);
        }
    }
}