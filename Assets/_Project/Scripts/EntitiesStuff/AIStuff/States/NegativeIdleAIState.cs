using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class NegativeIdleAIState : IdleAIState
    {
        private readonly AIBase _ai;
        private readonly Type _downtimeState;

        private AIMovement _movement;
        private float _startWanderTimer;
        private float _wanderBeginDelay = 2f;

        public NegativeIdleAIState(AIBase ai, Type downtimeState) : base(ai.gameObject)
        {
            _downtimeState = downtimeState;
            _ai = ai;
            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
        }

        public override void EnterState()
        {
            _startWanderTimer = 0f;
            _movement.ResetDirection();
        }

        public override Type FixedTick()
        {
            if (!IsStartPointReached)
            {
                if (_startWanderTimer >= _wanderBeginDelay)
                    return _downtimeState;

                _startWanderTimer += Time.fixedDeltaTime;
            }

            var haveTarget = _ai.HaveTarget(out float _);
            if (haveTarget)
            {
                if (_movement.IsSee(_ai.Target))
                    return typeof(ChaseAIState);
            }

            return null;
        }

        private bool IsStartPointReached =>
            Vector3.Distance(_ai.SpawnPoint, _ai.transform.position) < 0.5f;
    }
}