using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class AIStateMachine : MonoBehaviour
    {
        private Dictionary<Type, BaseAIState> _availableStates;

        private BaseAIState CurrentAIState { get; set; }
        public event Action<BaseAIState> OnStateChanged = s => { };

        public void SetStates(Dictionary<Type, BaseAIState> states) =>
            _availableStates = states;

        private void Update()
        {
            // Debug.Log($"{gameObject.name} = {CurrentAIState}");
            TickAction(CurrentAIState?.Tick());
        }

        private void FixedUpdate() => TickAction(CurrentAIState?.FixedTick());

        private void TickAction(Type func)
        {
            if (CurrentAIState == null)
            {
                CurrentAIState = _availableStates.Values.First();
                CurrentAIState.EnterState();
            }

            var nextState = func;

            if (nextState != null && nextState != CurrentAIState?.GetType())
                ChangeState(nextState);
        }

        public void ChangeState(Type nextState)
        {
            CurrentAIState = _availableStates[nextState];
            OnStateChanged(CurrentAIState);
            CurrentAIState.EnterState();
        }

        private void OnTriggerEnter(Collider other)
        {
            CurrentAIState.TriggerEnter(other);
        }

        private void OnTriggerExit(Collider other)
        {
            CurrentAIState.TriggerExit(other);
        }
    }
}