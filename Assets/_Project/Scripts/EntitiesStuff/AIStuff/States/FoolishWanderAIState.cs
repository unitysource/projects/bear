// using System;
// using _Project.Scripts.Architecture.Services;
// using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
// using _Project.Scripts.SettingsStuff;
// using Assets._Project.Scripts.Utilities;
// using UnityEngine;
//
// namespace _Project.Scripts.EntitiesStuff.AIStuff.States
// {
//     public class FoolishWanderAIState : BaseAIState
//     {
//         private readonly AIBase _ai;
//         private AIMovement _movement;
//         private Vector3 _direction;
//         private float _changeDirectionTimer;
//         private readonly RandomService _randomService;
//         
//         private readonly MinMaxFloat _changeDirectionDelay = new MinMaxFloat(1f, 3f);
//
//         public FoolishWanderAIState(AIBase ai, RandomService randomService) : base(ai.gameObject)
//         {
//             _randomService = randomService;
//             _ai = ai;
//
//             Initialize();
//         }
//
//         private void Initialize()
//         {
//             _movement = _ai.GetComponent<AIMovement>();
//         }
//
//         public override void EnterState()
//         {
//             SetRandomDirection();
//         }
//
//         public override Type FixedTick()
//         {
//             CommonTools.TimerActionLooped(ref _changeDirectionTimer, 
//                 _randomService.GetValue(_changeDirectionDelay), action: SetRandomDirection);
//             
//             _movement.RotateByDirection(_direction);
//             _movement.CalculateDirection(_ai.transform.position + _direction);
//             _movement.Move();
//
//             if (_movement.IsSee(_ai.Target))
//                 return typeof(ChaseAIState);
//
//             return null;
//         }
//
//         private void SetRandomDirection()
//         {
//             _direction = _randomService.GetRandomDirection();
//         }
//     }
// }