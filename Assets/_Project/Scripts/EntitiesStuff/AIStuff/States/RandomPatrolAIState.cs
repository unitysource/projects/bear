using System;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.Services;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class RandomPatrolAIState : BaseAIState, IGizmos
    {
        private readonly PositiveAI _ai;
        private AIMovement _positiveAIMovement;
        private Vector3 _point;
        private readonly RandomService _randomService;
        private readonly float _patrolRadius;

        private const float ReachedDistance = 0.3f;

        public RandomPatrolAIState(PositiveAI ai, RandomService randomService, float patrolRadius) 
            : base(ai.gameObject)
        {
            _patrolRadius = patrolRadius;
            _ai = ai;
            _randomService = randomService;

            Initialize();
        }

        private void Initialize()
        {
            _positiveAIMovement = _ai.GetComponent<AIMovement>();
            _ai.GizmosService.AddGizmo(this);
        }

        public override void EnterState()
        {
            SetNextPoint();
        }

        public override Type FixedTick()
        {
            if (IsPointReached) 
                SetNextPoint();

            _positiveAIMovement.CalculateDirection(_point);
            _positiveAIMovement.Rotate(_point);
            _positiveAIMovement.Move();
            Debug.DrawLine(_ai.transform.position, _point, Color.red);

            if (_ai.HaveTarget(out Vector3 _) && _ai.IsTamed)
                return typeof(ChaseAIState);
            
            if(_ai.HaveTarget(out Vector3 _) && !_ai.IsTamed)
                return typeof(FearAIState);
            
            if (!_ai.IsSeeHero())
                return typeof(ChaseHeroAIState);

            return null;
        }

        private bool IsPointReached =>
            Vector3.Distance(_point, _ai.transform.position) <= ReachedDistance;

        private void SetNextPoint()
        {
            var offset = _randomService.GetRandomPointInsideCircle(_patrolRadius);
            _point = _ai.HeroTransform.position + _ai.transform.TransformDirection(new Vector3(offset.x, 0f, offset.y));
        }

        public void DrawGizmo()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(_ai.HeroTransform.position, _patrolRadius);
        }
    }
}