using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class IdleAIState : BaseAIState
    {
        protected IdleAIState(GameObject gameObject) : base(gameObject)
        {
        }
    }
}