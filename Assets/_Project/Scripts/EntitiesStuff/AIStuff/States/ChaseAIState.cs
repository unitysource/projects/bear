using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class ChaseAIState : BaseAIState
    {
        private readonly AIBase _ai;
        private AIMovement _movement;
        private AiHealth _aiHealth;

        public ChaseAIState(AIBase ai) : base(ai.gameObject)
        {
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
            _aiHealth = _ai.GetComponent<AiHealth>();
        }

        public override void EnterState()
        {
            if (_ai.UseHealthUI)
            {
                _aiHealth.StopCheckOut();
                _aiHealth.TryActivateUI();
            }
        }

        public override Type FixedTick()
        {
            if (_ai.HaveTarget(out Vector3 _))
            {
                var targetPosition = _ai.Target.position;
                _movement.Rotate(targetPosition);
                _movement.CalculateDirection(targetPosition);
                _movement.Move();
                
                if (_movement.IsNearby(_ai.Target))
                    return typeof(AttackAIState);
                
                if (!_movement.IsSee(_ai.Target) && !_movement.IsNearby(_ai.Target) && _ai.IsSeeHero())
                {
                    if(_ai.UseHealthUI) _aiHealth.BeginCheckOut();
                    return typeof(IdleAIState);
                }
            }
            else return typeof(IdleAIState);

            return null;
        }
    }
}