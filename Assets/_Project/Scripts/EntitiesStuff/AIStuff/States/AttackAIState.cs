using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using Assets._Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class AttackAIState : BaseAIState
    {
        private readonly AIBase _ai;
        private readonly float _attackDelay;
        private readonly int _damage;

        private float _attackTimer;
        private Health _health;
        private AIMovement _movement;
        private MeleeAttack _meleeAttack;
        private float _knockBackForce;

        public AttackAIState(AIBase ai, float attackDelay, int damage, float knockBackForce) : base(ai.gameObject)
        {
            _knockBackForce = knockBackForce;
            _damage = damage;
            _attackDelay = attackDelay;
            _ai = ai;
            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
            _meleeAttack = _ai.GetComponent<MeleeAttack>();
        }

        public override void EnterState()
        {
            _movement.ResetDirection();
        }

        public override Type FixedTick()
        {
            if (_ai.HaveTarget(out Vector3 direction))
            {
                SetTargetHealth();

                CommonTools.TimerActionLooped(ref _attackTimer, _attackDelay, () =>
                {
                    _meleeAttack.Attack(_health, _damage);
                    if (_ai.Target.TryGetComponent<KnockBackEntity>(out var knockBackEntity))
                        knockBackEntity.KnockBack(direction, _knockBackForce);
                });

                if (!_movement.IsNearby(_ai.Target))
                    return typeof(IdleAIState);

                _movement.Rotate(_ai.Target.position);

                if (!_health.IsAlive)
                {
                    _ai.Target = null;
                    return typeof(IdleAIState);
                }
            }

            if (!_ai.IsSeeHero())
                return typeof(ChaseHeroAIState);

            return null;
        }

        private void SetTargetHealth()
        {
            Health health = _ai.Target.GetComponent<Health>();
            if (!Equals(_health, health)) _health = health;
        }
    }
}