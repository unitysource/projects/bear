using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class DeathAIState : BaseAIState
    {
        private readonly AIBase _ai;
        private readonly float _respawnDuration;
        
        private EntityDeath _death;
        private float _spawnTimer;
        private AiInteractable _aiInteractable;
        private AIMovement _movement;
        private readonly Collider[] _colliders;

        public DeathAIState(AIBase ai, float respawnDuration, Collider[] colliders) : base(ai.gameObject)
        {
            _colliders = colliders;
            _ai = ai;
            _respawnDuration = respawnDuration;

            Initialize();
        }

        private void Initialize()
        {
            _death = _ai.GetComponent<EntityDeath>();
            _aiInteractable = _ai.GetComponent<AiInteractable>();
            _movement = _ai.GetComponent<AIMovement>();
        }

        public override void EnterState()
        {
            foreach (var col in _colliders) 
                if(col.isTrigger) col.enabled = false;
            
            _spawnTimer = 0f;
            _death.Deactivate();
            _ai.SetActiveTrigger(false);
            if(_ai.IsUseInteraction()) _aiInteractable.CanInteract = false;
            _movement.ResetDirection();
            _ai.Target = null;
        }

        public override Type FixedTick()
        {
            _spawnTimer += Time.fixedDeltaTime;
            if (_spawnTimer >= _respawnDuration)
            {
                _death.Activate();
                _ai.SetActiveTrigger(true);
                return typeof(SpawnAIState);
            }

            return null;
        }
    }
}