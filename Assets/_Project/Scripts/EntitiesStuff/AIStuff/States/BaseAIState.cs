using System;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public abstract class BaseAIState
    {
        private readonly GameObject _gameObject;

        protected BaseAIState(GameObject gameObject)
        {
            _gameObject = gameObject;
        }

        public virtual void EnterState()
        {
        }

        public virtual Type Tick() => null;

        public virtual Type FixedTick() => null;

        public virtual Type TriggerEnter(Collider other) => null;

        public virtual Type TriggerExit(Collider other) => null;
    }
}