using System;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class ReturnToStartPointAIState : BaseAIState
    {
        private readonly AIBase _ai;
        private AIMovement _movement;
        private AiHealth _aiHealth;

        public ReturnToStartPointAIState(AIBase ai) : base(ai.gameObject)
        {
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
            if(_ai.UseHealthUI) _aiHealth = _ai.GetComponent<AiHealth>();
        }

        public override Type FixedTick()
        {
            _movement.Rotate(_ai.SpawnPoint);
            _movement.CalculateDirection(_ai.SpawnPoint);
            _movement.Move();

            if (IsStartPointReached)
            {
                _ai.SetPosition(_ai.SpawnPoint);
                return typeof(IdleAIState);
            }

            if (_ai.Target != null)
            {
                if (_movement.IsNearby(_ai.Target))
                    return typeof(AttackAIState);

                if (_movement.IsSee(_ai.Target))
                {
                    if(_ai.UseHealthUI) _aiHealth.TryActivateUI();
                    return typeof(ChaseAIState);
                }
            }

            return null;
        }

        private bool IsStartPointReached => 
            Vector3.Distance(_ai.SpawnPoint, _ai.transform.position) < 0.4f;
    }
}