using System;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class ChaseHeroAIState : BaseAIState
    {
        private readonly PositiveAI _ai;
        private AIMovement _movement;
        private AiHealth _aiHealth;
        private Vector3 _point;
        private readonly RandomService _randomService;
        private bool _isPointFindOut;

        public ChaseHeroAIState(PositiveAI ai, RandomService randomService) : base(ai.gameObject)
        {
            _randomService = randomService;
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
            _aiHealth = _ai.GetComponent<AiHealth>();
        }

        public override void EnterState()
        {
            if (_ai.UseHealthUI)
            {
                _aiHealth.StopCheckOut();
                _aiHealth.TryActivateUI();
            }

            // _isPointFindOut = false;
            SetPoint();
        }

        public override Type FixedTick()
        {
            if (_ai.IsSeeHero())
            {
                return typeof(IdleAIState);
            }

            // if (_isPointFindOut)
            // {
            //     _isPointFindOut = true;
            //     SetPoint();
            // }
            var targetPosition = _ai.HeroTransform.position;
            _movement.Rotate(targetPosition);
            _movement.CalculateDirection(targetPosition);
            _movement.Move();

            return null;
        }
        
        private void SetPoint()
        {
            var offset = _randomService.GetRandomPointInsideCircle(1f).normalized;
            _point = _ai.SpawnPoint + _ai.transform.TransformDirection(new Vector3(offset.x, 0f, offset.y));
        }
    }
}