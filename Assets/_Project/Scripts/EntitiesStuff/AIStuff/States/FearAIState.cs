using System;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.Services;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.States
{
    public class FearAIState : BaseAIState
    {
        private readonly PositiveAI _ai;
        private readonly RandomService _randomService;
        private readonly float _fearRadius;
        private AIMovement _movement;
        private Vector3 _point;
        private bool _isGoBack;
        private bool _pointIsCalculated;

        public FearAIState(PositiveAI ai, RandomService randomService, float fearRadius) : base(ai.gameObject)
        {
            _randomService = randomService;
            _fearRadius = fearRadius;
            _ai = ai;

            Initialize();
        }

        private void Initialize()
        {
            _movement = _ai.GetComponent<AIMovement>();
            // if(_ai.UseHealthUI) _aiHealth = _ai.GetComponent<AiHealth>();
        }

        public override void EnterState()
        {
            // SetPoint();
            _isGoBack = false;
            _pointIsCalculated = false;
        }

        public override Type FixedTick()
        {
            Debug.DrawLine(_ai.SpawnPoint, _point, Color.cyan);

            bool haveTarget = _ai.HaveTarget(out _, out Vector3 direction);

            if (!_isGoBack)
            {
                if (!_pointIsCalculated && haveTarget && _ai.Target.GetComponent<Health>().IsAlive)
                {
                    SetPoint(direction.normalized);
                    _pointIsCalculated = true;
                }

                if (_pointIsCalculated && !IsFearPointReached)
                {
                    _movement.Rotate(_point);
                    _movement.CalculateDirection(_point);
                    _movement.Move();
                }
                else if (IsFearPointReached)
                {
                    // if (haveTarget && distance < 0.8f)
                    // {
                    //     SetPoint(direction);
                    // }
                    // else
                    // {
                    _movement.ResetDirection();
                    _ai.SetPosition(_point);
                    _isGoBack = true;
                    // }
                }
            }
            else
            {
                if (!haveTarget || !_ai.Target.GetComponent<Health>().IsAlive)
                {
                    _movement.Rotate(_ai.SpawnPoint);
                    _movement.CalculateDirection(_ai.SpawnPoint);
                    _movement.Move();

                    if (IsStartPointReached)
                    {
                        _ai.SetPosition(_ai.SpawnPoint);
                        return typeof(IdleAIState);
                    }
                }
            }

            return null;
        }

        private bool IsStartPointReached =>
            Vector3.Distance(_ai.SpawnPoint, _ai.transform.position) < 0.5f;

        private bool IsFearPointReached =>
            Vector3.Distance(_point, _ai.transform.position) < 0.5f;

        private void SetPoint(Vector3 direction)
        {
            direction = new Vector3(direction.x, 0, direction.z);
            var offset = Vector3.Cross(direction, Vector3.up).normalized * _fearRadius;
            _point = _ai.SpawnPoint + _ai.transform.TransformDirection(new Vector3(offset.x, 0f, offset.y));
        }
    }
}