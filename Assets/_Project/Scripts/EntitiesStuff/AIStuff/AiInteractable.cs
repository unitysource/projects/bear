using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.AIStuff
{
    public class AiInteractable : MonoBehaviour, IInteractable
    {
        [SerializeField] private Transform _resourcesStartPoint;
        [SerializeField] private PropsValue _resourceValue;

        private Health _health;
        private EntityAnimator _entityAnimator;
        private GameResourcesService _resourcesService;

        private MinMaxFloat _yOffset;
        private MinMaxFloat _flyDuration;
        private HeroInventory _heroInventory;
        private Transform _hero;

        private void Awake()
        {
            _health = GetComponent<Health>();
            _entityAnimator = GetComponent<EntityAnimator>();

            _yOffset = new MinMaxFloat(1.4f, 1.6f);
            _flyDuration = new MinMaxFloat(1.2f, 1.5f);
        }

        public void Construct(GameResourcesService resourcesService, Transform hero)
        {
            _hero = hero;
            _resourcesService = resourcesService;

            _heroInventory = _hero.GetComponent<HeroInventory>();
        }

        public void Interact(HeroInteractable hero, float damage, out Health health)
        {
            health = _health;
            _entityAnimator.GetHit();

            ResourceAnimate();
            _heroInventory.AddProps(_resourceValue);
        }

        private void ResourceAnimate()
        {
            _resourcesService.AnimateFlyLoop(_resourceValue.GameResourceType, _resourcesStartPoint, _hero.transform,
                1f, _yOffset, _flyDuration, _resourceValue.Amount);
        }

        public bool CanInteract { get; set; }

        public InstrumentType GetInteractionInstrumentType() => InstrumentType.Sword;

        public GameResourceType[] GetSeeResourcesTypes() => new[] { GameResourceType.None };
        public GameResourceType GetInteractionResourceType() => GameResourceType.None;
    }
}