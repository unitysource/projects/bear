using _Project.Scripts.Architecture.Behaviours;
using _Project.Scripts.EntitiesStuff.AIStuff.States;
using _Project.Scripts.FactoriesStuff;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AICharacters
{
    public abstract class AIBase : MonoBehaviour, IEntity, ITriggable
    {
        [SerializeField] EntityType _entityType;

        [SerializeField] private SimpleObserver _observer;

        public Vector3 SpawnPoint { get; protected set; }
        public Transform Target { get; set; }
        protected AIStateMachine StateMachine => GetComponent<AIStateMachine>();
        public EntityType EntityType => _entityType;

        public bool UseHealthUI { get; protected set; }

        public virtual void Initialize()
        {
            _observer.Setup(this);
        }

        public abstract void InitializeStateMachine();

        public bool HaveTarget(out float distance)
        {
            distance = 0f;
            if (Target != null)
            {
                distance = Vector3.Distance(Target.position, transform.position);
                return true;
            }

            return false;
        }

        public bool HaveTarget(out Vector3 normalizedDirection)
        {
            normalizedDirection = Vector3.zero;
            if (Target != null)
            {
                normalizedDirection = (Target.position - transform.position).normalized;
                return true;
            }

            return false;
        }

        public bool HaveTarget(out float distance, out Vector3 normalizedDirection)
        {
            distance = 0f;
            normalizedDirection = Vector3.zero;
            if (Target != null)
            {
                var position = transform.position;
                var position1 = Target.position;
                distance = Vector3.Distance(position1, position);
                normalizedDirection = (position1 - position).normalized;
                return true;
            }

            return false;
        }

        public abstract bool IsSeeHero();
        public abstract bool IsUseInteraction();
        public abstract void TriggerEnter(Collider other);
        public abstract void TriggerExit(Collider other);
        public void SetPosition(Vector3 position) => transform.position = position;
        public void SetActiveTrigger(bool active) => _observer.SetActive(active);

        public abstract void SetData(string savePath);
    }

    public interface IEntity
    {
        EntityType EntityType { get; }
    }
}