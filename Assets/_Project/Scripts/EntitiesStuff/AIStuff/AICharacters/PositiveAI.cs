using System;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.States;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.UIStuff;
using Assets._Project.Scripts.Utilities;
using Assets._Project.Scripts.Utilities.Constants;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AICharacters
{
    [SelectionBase]
    public class PositiveAI : AIBase
    {
        [SerializeField] private PositiveEntityProperties _positiveEntityProperties;
        [SerializeField] private EntityModel _entityModel;
        [SerializeField] private GameObject _pointer;

        public Transform HeroTransform { get; private set; }
        private RandomService _randomService;
        private Health _health;
        private AudioService _audioService;
        private Rigidbody _rb;
        private string _savePath;

        public GizmosService GizmosService { get; private set; }
        // public Vector3 CurrentPoint { get; private set; }

        public PositiveEntityProperties EntityProperties => _positiveEntityProperties;

        public PetPointersWindow PetPointersWindow { get; private set; }

        public bool IsTamed { get; private set; }

        [Inject]
        private void Construct(RandomService randomService, HeroMovement heroMovement, GizmosService gizmosService,
            AudioService audioService)
        {
            _audioService = audioService;
            _randomService = randomService;
            GizmosService = gizmosService;
            HeroTransform = heroMovement.transform;
        }

        public override void SetData(string savePath)
        {
            _savePath = savePath;
            IsTamed = CommonTools.IntToBool(PlayerPrefs.GetInt(savePath));
        }

        public override void Initialize()
        {
            base.Initialize();

            _pointer.SetActive(false);
            _health = GetComponent<Health>();
            // _rb = GetComponent<Rigidbody>();
            _health.Dead += OnDead;
            var position = transform.position;
            SpawnPoint = position;
            // CurrentPoint = position;
            UseHealthUI = _health.UseUI;

            PetPointersWindow = FindObjectOfType<PetPointersWindow>();

            if (IsTamed)
                PetPointersWindow.AddPointer(transform);

            SetLayer();
        }

        private void OnDead(DamageType damageType)
        {
            _audioService.PlaySound(AudioClipId.hero_death);

            StateMachine.ChangeState(typeof(DeathAIState));

            _pointer.SetActive(false);
            SetTamed(false);
            PetPointersWindow.RemovePointer(transform);
        }

        public void SetLayer()
        {
            string layerName = IsTamed ? Layers.Default : Layers.UnTamedPet;
            int layer = LayerMask.NameToLayer(layerName);
            gameObject.layer = layer;
        }

        public override void InitializeStateMachine()
        {
            var cols = GetComponents<Collider>();
            var states = new Dictionary<Type, BaseAIState>
            {
                {typeof(SpawnAIState), new SpawnAIState(this, _entityModel, cols, _pointer)},
                {
                    typeof(IdleAIState),
                    new PositiveIdleAIState(this, typeof(CirclePatrolAIState), _positiveEntityProperties.ThinkDelay)
                },
                {
                    typeof(CirclePatrolAIState),
                    new CirclePatrolAIState(this, _positiveEntityProperties.PatrolRadius)
                },
                {typeof(ReturnToStartPointAIState), new ReturnToStartPointAIState(this)},
                {typeof(ChaseHeroAIState), new ChaseHeroAIState(this, _randomService)},
                {typeof(ChaseAIState), new ChaseAIState(this)},
                {typeof(DeathAIState), new DeathAIState(this, _positiveEntityProperties.RespawnDuration, cols)},
                {typeof(FearAIState), new FearAIState(this, _randomService, _positiveEntityProperties.FearRadius)},
                {
                    typeof(AttackAIState),
                    new AttackAIState(this, _positiveEntityProperties.AttackDelay, _positiveEntityProperties.Damage,
                        _positiveEntityProperties.KnockBackForce)
                },
            };

            StateMachine.SetStates(states);
        }

        public override bool IsSeeHero()
        {
            var distance = Vector3.Distance(HeroTransform.position, transform.position);
            return distance <= _positiveEntityProperties.SeeHeroDistance;
        }

        public override bool IsUseInteraction() => false;

        public override void TriggerEnter(Collider other)
        {
            if (other.TryGetComponent<NegativeAI>(out var negativeAI))
            {
                Debug.Log($"entity: {negativeAI}, me: {EntityType}");
                Target = negativeAI.transform;
            }
        }

        public override void TriggerExit(Collider other)
        {
            if (other.TryGetComponent<NegativeAI>(out var negativeAI))
            {
                // Debug.Log($"entity: {entity.EntityType}, me: {EntityType}");
                Target = null;
            }
        }

        public void SetTamed(bool value)
        {
            PlayerPrefs.SetInt(_savePath, CommonTools.BoolToInt(value));
            IsTamed = value;
        }

        #region Gizmos

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            var position = transform.position;
            Gizmos.DrawWireSphere(position, _positiveEntityProperties.SeeHeroDistance);
        }

        #endregion
    }
    
    // public class AiContainer
    // {
    //     private readonly List<PositiveAI> _positiveAis = new();
    //
    //     public void Add(PositiveAI item)
    //     {
    //         _positiveAis.Add(item);
    //     }
    //     
    //     public void Remove(PositiveAI item)
    //     {
    //         _positiveAis.Remove(item);
    //     }
    //
    //     public bool GetData()
    //     {
    //         return ;
    //     }
    // }
}