using System;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.States;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.Services;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.AIStuff.AICharacters
{
    public class NegativeAI : AIBase
    {
        [SerializeField] private NegativeEntityProperties _negativeEntityProperties;
        [SerializeField] private EntityModel _entityModel;
        private RandomService _randomService;
        private Transform _heroTransform;
        private GameResourcesService _resourcesService;
        private AudioService _audioService;
        private List<IEntity> _collideEntities = new List<IEntity>();

        [Inject]
        private void Construct(RandomService randomService, HeroMovement heroMovement,
            GameResourcesService resourcesService, AudioService audioService)
        {
            _audioService = audioService;
            _resourcesService = resourcesService;
            _randomService = randomService;
            _heroTransform = heroMovement.transform;
        }

        public override void Initialize()
        {
            base.Initialize();
            var health = GetComponent<Health>();
            health.Dead += OnDead;
            GetComponent<AiInteractable>().Construct(_resourcesService, _heroTransform);

            SpawnPoint = transform.position;

            UseHealthUI = health.UseUI;
        }

        private void OnDestroy()
        {
            GetComponent<Health>().Dead -= OnDead;
        }

        private void OnDead(DamageType damageType)
        {
            StateMachine.ChangeState(typeof(DeathAIState));
            _collideEntities.Clear();

            _audioService.PlaySound(AudioClipId.hero_death);
        }

        public override void InitializeStateMachine()
        {
            // var heroHealth = _heroTransform.GetComponent<Health>();
            var cols = GetComponents<Collider>();
            var states = new Dictionary<Type, BaseAIState>
            {
                { typeof(SpawnAIState), new SpawnAIState(this, _entityModel, cols, null) },
                {
                    typeof(IdleAIState), new NegativeIdleAIState(this, downtimeState: typeof(ReturnToStartPointAIState))
                },
                { typeof(ReturnToStartPointAIState), new ReturnToStartPointAIState(this) },
                { typeof(ChaseAIState), new ChaseAIState(this) },
                {
                    typeof(AttackAIState),
                    new AttackAIState(this, _negativeEntityProperties.AttackDelay,
                        _negativeEntityProperties.Damage, _negativeEntityProperties.KnockBackForce)
                },
                { typeof(DeathAIState), new DeathAIState(this, _negativeEntityProperties.RespawnDuration, cols) },
            };

            StateMachine.SetStates(states);
        }

        public override bool IsSeeHero() => true;
        public override bool IsUseInteraction() => true;

        public override void TriggerEnter(Collider other)
        {
            if (other.TryGetComponent<IEntity>(out var entity) && !other.isTrigger && entity.EntityType != EntityType)
            {
                if (other.TryGetComponent(out PositiveAI ai))
                    // && !ai.EntityProperties.IsTamed)
                    return;
                if (other.TryGetComponent(out NegativeAI _))
                    return;
                
                Target = other.transform;
                _collideEntities.Add(other.GetComponent<IEntity>());
            }
        }

        public override void TriggerExit(Collider other)
        {
            if (other.TryGetComponent<IEntity>(out var entity) && !other.isTrigger && entity.EntityType != EntityType)
            {
                // if (other.TryGetComponent(out PositiveAI ai) && !ai.EntityProperties.IsTamed)
                //     return;
                if (other.TryGetComponent(out PositiveAI _))
                    return;
                if (other.TryGetComponent(out NegativeAI _))
                    return;

                Debug.Log($"entity exited: {entity.EntityType}, me: {EntityType}");
                Target = null;
                _collideEntities.Remove(other.GetComponent<IEntity>());
            }
        }

        public override void SetData(string savePath)
        {
            
        }
    }
}