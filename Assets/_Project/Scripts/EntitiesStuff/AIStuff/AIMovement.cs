﻿using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff
{
    public class AIMovement : EntityMovement
    {
        [SerializeField, Range(1f, 20f)] private float _seeDistance;
        [SerializeField, Range(0.1f, 5f)] private float _interactDistance;
        [SerializeField] protected Transform _looker;

        private void Update()
        {
            AnimateMovement(Direction);
        }

        public void CalculateDirection(Vector3 point) =>
            Direction = (point - transform.position).normalized;

        public void CircularMovement(Vector3 centerPos)
        {
            // get current magnitude
            var magnitude = _rb.velocity.magnitude;

            // get vector center <- obj
            var gravityVector = centerPos - _rb.position;

            // check whether left or right of target
            var left = Vector2.SignedAngle(_rb.velocity, gravityVector) > 0;

            // get new vector which is 90° on gravityDirection 
            // and world Z (since 2D game)
            // normalize so it has magnitude = 1
            var newDirection = Vector3.Cross(gravityVector, Vector3.forward).normalized;

            // invert the newDirection in case user is touching right of movement direction
            if (!left) newDirection *= -1;

            // set new direction but keep speed(previously stored magnitude)
            _rb.velocity = newDirection * magnitude;
        }

        public override void Move()
        {
            Vector3 move = Direction * (Time.fixedDeltaTime * _moveSpeed);
            _rb.MovePosition(_rb.position + move);
        }

        public void Rotate(Vector3 point)
        {
            _looker.LookAt(point, Model.transform.up);
            var localRotation = _looker.localRotation;
            localRotation = Quaternion.Euler(0, localRotation.eulerAngles.y, 0);
            Model.SimpleRotate(localRotation, _rotationSpeed * 10f);
        }

        public bool IsSee(Transform objTransform)
        {
            var distance = Vector3.Distance(transform.position, objTransform.position);
            return distance <= _seeDistance && distance > _interactDistance;
        }

        public bool IsNearby(Transform objTransform)
        {
            var distance = Vector3.Distance(transform.position, objTransform.position);
            return distance <= _interactDistance;
        }

        #region Gizmos

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            var position = transform.position;
            Gizmos.DrawWireSphere(position, _interactDistance);
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(position, _seeDistance);
            Gizmos.color = Color.white;
        }

        #endregion
    }
}