using System.Collections;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.AIStuff
{
    public class AiHealth : Health
    {
        [SerializeField] private float _checkDelay = 1f;

        public void BeginCheckOut()
        {
            StartCoroutine(nameof(CheckOutRoutine));
        }

        public void StopCheckOut()
        {
            StopCoroutine(nameof(CheckOutRoutine));
        }

        private IEnumerator CheckOutRoutine()
        {
            yield return new WaitForSeconds(_checkDelay);
            _healthUI.CanvasActivation(false);
        }

        public void TryActivateUI()
        {
            if (!IsFull) 
                _healthUI.CanvasActivation(true);
        }
    }
}