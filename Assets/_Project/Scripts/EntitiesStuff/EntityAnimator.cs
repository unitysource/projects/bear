﻿using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public class EntityAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        private static readonly int AttackTrigger = Animator.StringToHash("Attack");
        private static readonly int GetHitTrigger = Animator.StringToHash("GetHit");
        private static readonly int GroundedBool = Animator.StringToHash("IsGrounded");
        private static readonly int VelocityFloat = Animator.StringToHash("Velocity");

        public void Attack()
        {
            _animator.SetTrigger(AttackTrigger);
        }

        public void GetHit() => _animator.SetTrigger(GetHitTrigger);

        public void SetVelocity(float velocity) => _animator.SetFloat(VelocityFloat, velocity);

        public void ChangeState(EntitySurfaceState entitySurfaceState)
        {
            switch (entitySurfaceState)
            {
                case EntitySurfaceState.Grounded:
                    _animator.SetBool(GroundedBool, true);
                    break;
                case EntitySurfaceState.Swimming:
                    _animator.SetBool(GroundedBool, false);
                    break;
            }
        }
    }
}