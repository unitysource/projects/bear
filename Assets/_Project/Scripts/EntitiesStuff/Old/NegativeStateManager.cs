// using _Project.Scripts.Entities.Strategy;
// using _Project.Scripts.Hero;
// using DG.Tweening;
// using UnityEngine;
// using Zenject;
//
// namespace _Project.Scripts.Entities
// {
//     public class NegativeStateManager : EntityStateManager
//     {
//         private AIMeleeAttack _meleeAttack;
//         private Health _heroHealth;
//         private AiHealth _aiHealth;
//         private HeroMovement _heroMovement;
//         private AIMovement _movement;
//         private EntitySpawn _entitySpawn;
//
//         [Inject]
//         private void Construct(HeroMovement heroMovement)
//         {
//             _heroMovement = heroMovement;
//         }
//
//         protected override void Initialize()
//         {
//             _meleeAttack = GetComponent<AIMeleeAttack>();
//             _aiHealth = GetComponent<AiHealth>();
//             _heroHealth = _heroMovement.GetComponent<Health>();
//             _movement = GetComponent<AIMovement>();
//
//             _aiHealth.Dead += OnDead;
//             _entitySpawn.SpawnAction += OnSpawn;
//         }
//
//         private void OnDead()
//         {
//             _entityState = EntityState.Dead;
//         }
//
//         private void OnSpawn()
//         {
//             DOVirtual.DelayedCall(_activationDuration, () =>
//             {
//                 _entityState = EntityState.Idle;
//                 _meleeAttack.BeginAttack(_heroHealth);
//             });
//         }
//
//         protected override void IdleState()
//         {
//             if (_movement.IsSee(_heroMovement.transform))
//             {
//                 _aiHealth.StopCheckOut();
//                 _aiHealth.TryActivateUI();
//                 _meleeAttack.ChangeAttack(false);
//                 _entityState = EntityState.TargetMove;
//             }
//
//             if (_movement.IsNearby(_heroMovement.transform))
//             {
//                 _movement.Rotate(_heroMovement.transform.position);
//             }
//         }
//
//         protected override void TargetMoveState()
//         {
//             _movement.Rotate(_heroMovement.transform.position);
//
//             if (_thinkTimer >= _thinkDelay)
//             {
//                 if (_movement.IsNearby(_heroMovement.transform)) 
//                     _entityState = EntityState.Attack;
//
//                 _movement.CalculateDirection();
//
//                 _thinkTimer = 0;
//             }
//
//             if (!_movement.IsSee(_heroMovement.transform) && !_movement.IsNearby(_heroMovement.transform))
//             {
//                 _movement.ResetDirection();
//                 _aiHealth.BeginCheckOut();
//                 _entityState = EntityState.Idle;
//             }
//         }
//
//         protected override void AttackState()
//         {
//             _movement.ResetDirection();
//             _meleeAttack.ChangeAttack(true);
//             _entityState = EntityState.Idle;
//         }
//
//         private void OnDrawGizmos()
//         {
//             Gizmos.color = Color.cyan;
//             var position = transform.position;
//             Gizmos.DrawWireSphere(position, _movement.InteractDistance);
//             Gizmos.color = Color.magenta;
//             Gizmos.DrawWireSphere(position, _movement.SeeDistance);
//             Gizmos.color = Color.white;
//         }
//     }
// }