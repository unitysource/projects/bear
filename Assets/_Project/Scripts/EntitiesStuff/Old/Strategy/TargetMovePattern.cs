using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.Old.Strategy
{
    public class TargetMovePattern : IMovable
    {
        private EntityMovement _entityMovement;

        public TargetMovePattern(EntityMovement entityMovement)
        {
            _entityMovement = entityMovement;
        }

        public void Move(Vector3 point)
        {
            // _entityMovement.
        }

        public void Initialize(params object[] list)
        {
            
        }
    }
}