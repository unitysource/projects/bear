// using System;
// using System.Collections;
// using _Project.Scripts.Behaviours;
// using UnityEngine;
//
// namespace _Project.Scripts.Entities.Strategy
// {
//     public abstract class AIEntity : MonoBehaviour
//     {
//         protected IInteractable _interactable;
//         protected IMovable _movable;
//
//         [SerializeField] protected float _thinkTimer;
//         private Transform _target;
//
//         public void Construct(Transform hero)
//         {
//             _target = hero;
//         }
//
//         public abstract void InitBehaviours();
//
//         protected void ChangeMoving(IMovable movable) => _movable = movable;
//
//
//         public void Interact() => _interactable.Interact();
//
//         public void Move() => _movable.Move(_target.transform.position);
//
//         private IEnumerator UpdateRoutine()
//         {
//             while (true)
//             {
//                 Move();
//                 Interact();
//
//                 yield return new WaitForSeconds(_thinkTimer);
//             }
//         }
//     }
// }