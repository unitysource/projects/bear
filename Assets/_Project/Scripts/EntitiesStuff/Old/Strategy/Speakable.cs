namespace _Project.Scripts.EntitiesStuff.Old.Strategy
{
    public interface IInteractable : IInitialize
    {
        void Interact();
    }

    public class NoInteractBehaviour : IInteractable
    {
        public void Interact()
        {
            // Nothing to do
        }

        public void Initialize(params object[] list)
        {
            
        }
    }
    
    public class MeleeAttackBehaviour : IInteractable
    {

        public void Interact()
        {
            // Attack
        }

        public void Initialize(params object[] list)
        {
            
        }
    }
}