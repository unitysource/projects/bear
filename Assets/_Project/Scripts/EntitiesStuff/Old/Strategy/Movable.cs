using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.Old.Strategy
{
    public interface IMovable : IInitialize
    {
        void Move(Vector3 point);
    }

    public interface IInitialize
    {
        void Initialize(params object[] list);
    }

    /*public class PatrolMovePattern : IMovable
    {
        private Transform _transform;
        private Vector3[] _endPoints;

        public PatrolMovePattern(Transform transform, Vector3[] endPoints)
        {
            _transform = transform;
            _endPoints = endPoints;
        }

        public void Move(Vector3 point)
        {
            // Move
        }
    }*/
    
    public class IdlePattern : IMovable
    {
        public IdlePattern()
        {
        }

        public void Move(Vector3 point)
        {
            // Do not move
        }

        public void Initialize(params object[] list)
        {
            
        }
    }
}