// using System;
// using _Project.Scripts.Hero;
// using UnityEngine;
//
// namespace _Project.Scripts.Entities
// {
//     public abstract class EntityStateManager : MonoBehaviour
//     {
//         [SerializeField] protected float _activationDuration;
//         [SerializeField] protected float _thinkDelay = 0.2f;
//         
//         protected EntityState _entityState = EntityState.Spawn;
//         protected float _thinkTimer;
//         
//         protected abstract void Initialize();
//         
//         private void LateUpdate()
//         {
//             switch (_entityState)
//             {
//                 case EntityState.Spawn:
//                     // SpawnState();
//                     break;
//                 case EntityState.Idle:
//                     IdleState();
//                     break;
//                 case EntityState.TargetMove:
//                     TargetMoveState();
//                     break;
//                 case EntityState.Attack:
//                     AttackState();
//                     break;
//                 case EntityState.RandomMove:
//                     RandomMoveState();
//                     break;
//                 case EntityState.Dead:
//                     DeadState();
//                     break;
//             }
//             
//             _thinkTimer += Time.fixedDeltaTime;
//         }
//
//         protected virtual void RandomMoveState()
//         {
//         }
//
//         protected virtual void IdleState()
//         {
//         }
//
//         protected virtual void AttackState()
//         {
//         }
//
//         protected virtual void TargetMoveState()
//         {
//         }
//
//         protected virtual void DeadState()
//         {
//         }
//     }
//     
//     public enum EntityState
//     {
//         Spawn,
//         Idle,
//         RandomMove,
//         TargetMove,
//         Attack,
//         Dead
//     }
// }