// using System;
// using System.Collections;
// using UnityEngine;
//
// namespace _Project.Scripts.Entities
// {
//     public class AIMeleeAttack : MeleeAttack
//     {
//         // [SerializeField] private int _damage = 1;
//         // [SerializeField] private float _attackDelay = 3f;
//
//         public IEnumerator AttackRoutine(Health health)
//         {
//             while (gameObject.activeSelf)
//             {
//                 if (_canAttack)
//                     Attack(health, _damage);
//                 yield return new WaitForSeconds(_attackDelay);
//             }
//         }
//
//         public void BeginAttack(Health health)
//         {
//             ChangeAttack(false);
//             StartCoroutine(nameof(AttackRoutine), health);
//         }
//
//         public void StopAttack() => StopCoroutine(nameof(AttackRoutine));
//
//         public void ChangeAttack(bool active)
//         {
//             _canAttack = active;
//         }
//     }
// }