using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Architecture.SettingsStuff;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff
{
    public class EntitiesBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject _deathParticlesPrefab;

        private const string ContainerName = "DeathParticlesPool";
        private string _deathParticlesName;
        private IPoolService _poolService;

        [Inject]
        private void Construct(IPoolService poolService)
        {
            _poolService = poolService;
        }

        public void Initialize()
        {
            WarmDeathParticlesPool();
        }

        private void WarmDeathParticlesPool()
        {
            _deathParticlesName = _deathParticlesPrefab.name;
            _poolService.FillPool(PoolInfo.Create(_deathParticlesName, 10,
                _deathParticlesPrefab, ContainerName));
        }
    }
}