using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.GameTools;
using _Project.Scripts.UIStuff;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public class PetPurchase : RecycleToolBase
    {
        // [SerializeField] private PropsValue _requiredResources;
        // public PropsValue RequiredResources => _requiredResources;
        [SerializeField] private PositiveEntityProperties _positiveEntityProperties;
        private PositiveAI _positiveAI;

        protected override void Initialize()
        {
            base.Initialize();
            _positiveAI = GetComponent<PositiveAI>();
        }

        public override void Launch(int spendAmount, int resultAmount, int minValue)
        {
            bool enoughResources = _heroInventory.GetResourcesAmountByType(_spendResourceType) >= minValue;
            base.Launch(spendAmount, resultAmount, minValue);
            if (!enoughResources) return;

            _heroInventory.TakeProps(_spendResourceType, spendAmount);
            _positiveAI.SetTamed(true);
            _positiveAI.SetLayer();
            _recycleToolUI.Deactivate();
            _positiveAI.PetPointersWindow.AddPointer(transform);
        }

        public override void HeroTriggerEnter()
        {
            if (!_positiveAI.IsTamed)
                base.HeroTriggerEnter();
        }

        public override void HeroTriggerExit()
        {
            if (!_positiveAI.IsTamed)
                base.HeroTriggerExit();
        }
    }
}