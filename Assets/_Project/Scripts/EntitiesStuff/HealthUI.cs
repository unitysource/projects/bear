using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.EntitiesStuff
{
    public class HealthUI : MonoBehaviour
    {
        [SerializeField] private Image _healthBarImage;

        // private CanvasGroup _canvasGroup;
        private float _maxHealth;
        private const float _duration = 0.15f;

        public void Setup(float maxHealth)
        {
            _maxHealth = maxHealth;
            // _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void UpdateUI(float currentHealth)
        {
            _healthBarImage.fillAmount = currentHealth / _maxHealth;
        }

        public void CanvasActivation(bool active, bool isSmooth = true)
        {
            if (active)
            {
                if (transform.localScale != Vector3.one && isSmooth)
                    transform.DOScale(1f, _duration).From(0f).SetEase(Ease.OutBack).Play();
                else
                    transform.localScale = Vector3.one;
            }
            else
            {
                if (transform.localScale != Vector3.zero && isSmooth)
                    transform.DOScale(0f, _duration).From(1f).SetEase(Ease.InBack).Play();
                else 
                    transform.localScale = Vector3.zero;
            }
        }
    }
}