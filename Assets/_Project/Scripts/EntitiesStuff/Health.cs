﻿using System;
using _Project.Scripts.Entities;
using NaughtyAttributes;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public class Health : MonoBehaviour
    {
        public Action<DamageType> Dead = damageType => { };

        [SerializeField] private bool _haveUI;
        [ShowIf("_haveUI"), SerializeField] protected HealthUI _healthUI;
        [SerializeField] protected HealthSettings _healthSettings;

        protected float _currentHealth;
        private bool _isAlive;

        private void Start()
        {
            Initialize();
        }

        public void Initialize()
        {
            _isAlive = true;
            _currentHealth = _healthSettings.MAXHealth;

            if (_haveUI)
            {
                _healthUI.Setup(_healthSettings.MAXHealth);
                _healthUI.UpdateUI(_currentHealth);
                _healthUI.CanvasActivation(false);
            }
        }

        public virtual void TakeDamage(float dmg, DamageType damageType)
        {
            if (!_isAlive) return;

            if (_currentHealth - dmg <= 0)
            {
                if (_haveUI) _healthUI.CanvasActivation(false);
                _currentHealth = 0;
                Dead(damageType);
                _isAlive = false;
            }
            else
            {
                if (_haveUI) _healthUI.CanvasActivation(true);

                _currentHealth -= dmg;
            }

            if (_haveUI) _healthUI.UpdateUI(_currentHealth);
        }

        public void AddHealth(int health)
        {
            if (_currentHealth > _healthSettings.MAXHealth)
                _currentHealth = _healthSettings.MAXHealth;
            else
                _currentHealth += health;
        }

        public void UpgradeHealth(int value) =>
            _healthSettings.MAXHealth += value;

        protected bool IsDead => _currentHealth <= 0;

        protected bool IsFull => _currentHealth >= _healthSettings.MAXHealth;

        public bool IsAlive => _isAlive;

        public bool UseUI => _haveUI;
    }

    public enum DamageType
    {
        Water,
        Entity
    }
}