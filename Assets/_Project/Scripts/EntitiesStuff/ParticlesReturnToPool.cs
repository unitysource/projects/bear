using _Project.Scripts.Architecture.Services.Pool;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticlesReturnToPool : MonoBehaviour
    {
        [Inject] private IPoolService _poolService;
        
        private string _particleName;
        
        public void Initialize(string particleName)
        {
            _particleName = particleName;
            
            var main = GetComponent<ParticleSystem>().main;
            main.stopAction = ParticleSystemStopAction.Callback;
        }

        private void OnParticleSystemStopped()
        {
            _poolService.ReturnToPool(gameObject, _particleName);
        }
    }
}