﻿using System.Collections.Generic;
using _Project.Scripts.Tiles;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    [CreateAssetMenu(fileName = "HeroInventorySettings", menuName = "Data/HeroInventorySettings")]
    public class HeroInventorySettings : ScriptableObject
    {
        [SerializeField] private List<PropsValue> _propsesValues;

        public List<PropsValue> PropsesValues => _propsesValues;

        [Button("Full Inventory")]
        public void FullUpInventory()
        {
            _propsesValues.ForEach(p => p.Amount = 10000);
        }
        
        [Button("Clear Inventory")]
        public void ClearInventory()
        {
            _propsesValues.ForEach(p => p.Amount = 0);
        }
    }
}