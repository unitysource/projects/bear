using System;
using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Managers;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    [RequireComponent(typeof(HeroInventory))]
    [RequireComponent(typeof(HeroInstruments))]
    [RequireComponent(typeof(HeroAnimator))]
    public class HeroInteractable : MonoBehaviour
    {
        [Header("Properties")] [SerializeField]
        private LayerMask _interactableLayers;

        [SerializeField] private float _interactRadius;
        [SerializeField] private float _seeRadius;
        [SerializeField, Range(0, 180)] private float _maxInteractAngle = 100;
        [SerializeField] private float _reloadAttackDelay = 1f;
        [SerializeField] private float _reloadSeeDelay = 1f;

        [Header("Sub Behaviours")] [SerializeField]
        private EntityModel _entityModel;

        [SerializeField] private AddedResources _addedResources;
        [SerializeField] private HeroProperties _properties;


        private readonly Collider[] _interactColliders = new Collider[10];
        private readonly Collider[] _seeColliders = new Collider[10];
        readonly Stack<Collider> _interactableObjects = new Stack<Collider>();
        private HeroAnimator _animator;
        private HeroInstruments _instruments;
        private HeroInventory _heroInventory;

        public Action<GameResourceType[]> SeeResourceAction = resourceType => { };
        public Action<InstrumentType> InteractionInstrumentAction = instrumentType => { };
        public bool CanInteract;
        private int _interactObjectsAmount;
        private int _viewsObjectsAmount;
        private MeleeAttack _attack;
        private AudioService _audioService;
        private VibrationService _vibrationService;

        [Inject]
        private void Construct(AudioService audioService, VibrationService vibrationService)
        {
            _vibrationService = vibrationService;
            _audioService = audioService;
        }

        private void Start()
        {
            _instruments = GetComponent<HeroInstruments>();
            _animator = GetComponent<HeroAnimator>();
            _heroInventory = GetComponent<HeroInventory>();
            _attack = GetComponent<MeleeAttack>();
            CanInteract = true;

            _instruments.SetActiveTrail(false);

            BeginOverlapInteract();
            DOVirtual.DelayedCall(1f, BeginSeeInteract);
        }

        private void BeginSeeInteract() => StartCoroutine(nameof(SeeRoutine));

        private void BeginOverlapInteract() => StartCoroutine(nameof(InteractRoutine));

        private IEnumerator InteractRoutine()
        {
            while (gameObject.activeSelf)
            {
                yield return new WaitForSeconds(_reloadAttackDelay);

                if (!CanInteract) continue;

                _interactObjectsAmount = Physics.OverlapSphereNonAlloc(transform.position, _interactRadius,
                    _interactColliders, _interactableLayers);
                InteractNears();

                // Debug.Log($"_interactableObjects.Count {_interactableObjects.Count}");
                // DOVirtual.DelayedCall(0.2f, () =>
                // {
                    if (_interactableObjects.Count <= 0)
                    {
                        _instruments.TryDeactivateActive();
                        _instruments.SetActiveTrail(false);
                    }
                    else
                    {
                        _instruments.SetActiveTrail(true);
                    }
                // });

                _interactableObjects.Clear();
            }
        }

        private IEnumerator SeeRoutine()
        {
            while (gameObject.activeSelf)
            {
                _viewsObjectsAmount = Physics.OverlapSphereNonAlloc(transform.position, _seeRadius, _seeColliders,
                    _interactableLayers);
                SeeNears();

                yield return new WaitForSeconds(_reloadSeeDelay);
            }
        }

        private void InteractNears()
        {
            for (int i = 0; i < _interactObjectsAmount; i++)
            {
                Collider col = _interactColliders[i];
                bool isInteractable = col.TryGetComponent<IInteractable>(out var interactable);
                if (!SuitableAngle(col) || !isInteractable || !interactable.CanInteract) continue;

                Interact(interactable);
                _interactableObjects.Push(col);
            }
        }

        private void SeeNears()
        {
            for (int i = 0; i < _viewsObjectsAmount; i++)
            {
                Collider col = _seeColliders[i];
                bool isInteractable = col.TryGetComponent<IInteractable>(out var interactable);
                if (!isInteractable) continue;

                var seeResourcesTypes = interactable.GetSeeResourcesTypes();
                if (seeResourcesTypes[0] != GameResourceType.None)
                {
                    SeeResourceAction(seeResourcesTypes);
                }
            }
        }

        private void Interact(IInteractable interactable)
        {
            InstrumentType instrumentType = interactable.GetInteractionInstrumentType();
            InteractionInstrumentAction(instrumentType);

            float damage = _instruments.InstrumentDamages[instrumentType];
            // int instrumentLevel = _instruments.GetInstrumentSettings(instrumentType.ToString(), false).Level;
            InstrumentInteraction(instrumentType);
            interactable.Interact(this, damage, out Health health);
            
            _vibrationService.Vibrate(30);

            if (instrumentType == InstrumentType.Sword)
            {
                _attack.Attack(health, damage);
                _audioService.PlaySound(AudioClipId.entity_hit);
                health.GetComponent<KnockBackEntity>().KnockBack(health.transform.position - transform.position,
                    _properties.KnockBackForce);
            }

            var resourceType = interactable.GetInteractionResourceType();
            if (resourceType != GameResourceType.None)
            {
                _heroInventory.AddProps(resourceType, (int) damage);
                ResourceUI newResUI = _addedResources.GetInstance();
                newResUI.SetAmount((int) damage);
                newResUI.Animate(Vector3.zero, 1f);
            }
        }

        private float GetAngle(Collider col)
        {
            Vector3 direction = (col.transform.position - transform.position).normalized;
            float angle = Vector3.Angle(direction, _entityModel.transform.forward);
            return angle;
        }

        private bool SuitableAngle(Collider col) => GetAngle(col) <= _maxInteractAngle;

        private void InstrumentInteraction(InstrumentType instrumentType)
        {
            InstrumentBase instrument = _instruments.GetInstrumentByType(instrumentType);
            instrument.Activate(_instruments.ActivateDuration);
            Color trailColor = _instruments.GetInstrumentSettings(instrumentType.ToString(), false).TrailColor;
            _instruments.SetTrailColor(trailColor);
            _animator.Attack();
        }

        #region Gizmos

#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            var position = transform.position;
            Gizmos.DrawWireSphere(position, _interactRadius);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(position, _seeRadius);
            Gizmos.color = Color.white;
        }

#endif

        #endregion
    }
}