using System.Collections;
using _Project.Scripts.EntitiesStuff.AIStuff;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class RestoredHealth : AiHealth
    {
        [SerializeField] private float _restoreDelay = 1f;
        [SerializeField] private float _restoreSpeed = 1f;
        [SerializeField] private float _hideUIDelay = 1f;

        public override void TakeDamage(float dmg, DamageType damageType)
        {
            base.TakeDamage(dmg, damageType);

            if (!IsDead)
            {
                StopCoroutine(nameof(RestoreHealthRoutine));
                StartCoroutine(nameof(RestoreHealthRoutine));
            }
        }

        private IEnumerator RestoreHealthRoutine()
        {
            yield return new WaitForSeconds(_restoreDelay);
            while (_currentHealth < _healthSettings.MAXHealth)
            {
                _currentHealth += Time.deltaTime * _restoreSpeed;
                _healthUI.UpdateUI(_currentHealth);
                yield return new WaitForSeconds(0.02f);
            }
            yield return new WaitForSeconds(_hideUIDelay);
            _healthUI.CanvasActivation(false);
        }
    }
}