using _Project.Scripts.Architecture.Services.Pool;
using TMPro;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class AddedResources : MonoBehaviour
    {
        [SerializeField] private ResourceUI _resourceUIPrefab;
        [SerializeField] private Transform _parentTransform;

        private string _resName;
        private IPoolService _poolService;

        private const string ContainerName = "ResourceUIPool";

        [Inject]
        private void Construct(IPoolService poolService)
        {
            _poolService = poolService;
            FillPool();
        }

        private void FillPool()
        {
            _resName = _resourceUIPrefab.name;
            _poolService.FillPool(PoolInfo.Create(_resName, 10,
                _resourceUIPrefab.gameObject, ContainerName));
        }

        public ResourceUI GetInstance()
        {
            ResourceUI instance = _poolService.GetPoolObject(_resName).GetComponent<ResourceUI>();
            instance.transform.SetParent(_parentTransform, false);
            instance.gameObject.SetActive(true);
            instance.transform.localEulerAngles = Vector3.zero;
            return instance;
        }
    }
}