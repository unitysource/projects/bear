using System;
using System.Collections;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.FactoriesStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities.Extensions;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class ResourcesArrow : MonoBehaviour
    {
        [Header("Properties")] [SerializeField]
        float _thinkDelay = 1f;

        [SerializeField] private float _rotationSpeed = 1f;
        [SerializeField] private float _minSearchDistance = 2f;

        [Header("Sub Behaviours")] [SerializeField]
        private Transform _looker;

        [SerializeField] private Transform _resourceParent;
        [SerializeField] private Transform _arrow;

        public Action FinishSearchUIAction = () => { };


        private GameResourcesService _resourcesService;
        private IResourcesFactory _resourcesPoolFactory;
        private GameResourceType _resourceType;
        private Vector3 _nearestPropsPos;

        // public event Action<GameResourceType> SearchResourceAction = r => { };

        [Inject]
        private void Construct(IResourcesFactory resourcesPoolFactory, GameResourcesService resourcesService)
        {
            _resourcesService = resourcesService;
            _resourcesPoolFactory = resourcesPoolFactory;

            ActivateArrow(false);
        }

        public void StopSearch()
        {
            StopCoroutine(nameof(SearchRoutine));
            ActivateArrow(false);
        }

        public void BeginSearch(GameResourceType resourceType)
        {
            StopCoroutine(nameof(SearchRoutine));
            ReleaseOldModel();
            AddResourceModel(resourceType);
            ActivateArrow(true);
            StartCoroutine(nameof(SearchRoutine), resourceType);
        }

        private void ActivateArrow(bool active)
        {
            _arrow.gameObject.SetActive(active);
        }

        private void ReleaseOldModel()
        {
            _resourceParent.gameObject.ActionAllChildren(c =>
            {
                if (c.gameObject.activeSelf) 
                    _resourcesPoolFactory.Release(c.gameObject, _resourceType.ToString());
            });
        }

        private void AddResourceModel(GameResourceType resourceType)
        {
            GameObject resource = _resourcesPoolFactory.Create(resourceType.ToString());
            resource.SetActive(true);
            resource.transform.SetParent(_resourceParent, false);
        }

        private IEnumerator SearchRoutine(GameResourceType resourceType)
        {
            while (gameObject.activeSelf)
            {
                _resourceType = resourceType;
                _nearestPropsPos =
                    _resourcesService.GetDirectionToNearestProps(_resourceType, transform.position,
                        out var nearestDistance);

                if (nearestDistance <= _minSearchDistance)
                {
                    // FinishSearchUIAction();
                    SaveManager.SetString(SaveKey.SearchedCard, GameResourceType.None.ToString());
                    break;
                }

                _looker.LookAt(_nearestPropsPos, transform.up);
                var localRotation = _looker.localRotation;
                localRotation = Quaternion.Euler(0, localRotation.eulerAngles.y, 0);
                _looker.localRotation = localRotation;
                _arrow.localRotation =
                    Quaternion.Lerp(_arrow.localRotation, localRotation, _rotationSpeed * Time.deltaTime);
                yield return new WaitForSeconds(_thinkDelay);
            }

            ActivateArrow(false);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(_nearestPropsPos, 0.5f);
            Gizmos.color = Color.white;
        }
    }
}