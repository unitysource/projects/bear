﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class HeroInventory : MonoBehaviour
    {
        [SerializeField] private HeroInventorySettings _heroInventorySettings;
        [SerializeField] private Transform _resourcesPoint;

        public Transform ResourcesPoint => _resourcesPoint;

        private List<PropsValue> _propsesValues;

        public List<PropsValue> PropsesValues => _propsesValues;

        private void Awake()
        {
            _propsesValues = _heroInventorySettings.PropsesValues;
        }

        private void Start()
        {
            if (IsFirstGame)
            {
                ClearInventory();
                PlayerPrefs.SetInt(SaveKey.IsFirstGame.ToString(), 0);
            }
        }

        private void ClearInventory() => 
            _heroInventorySettings.PropsesValues.ForEach(resource => resource.Amount = 0);

        private static bool IsFirstGame => PlayerPrefs.GetInt(SaveKey.IsFirstGame.ToString(), 1) == 1;

        public void AddProps(GameResourceType gameResourceType, int addAmount)
        {
            if (!TryGetPropsInventoryIndex(gameResourceType, out var propsTypeIndex))
                _propsesValues.Add(new PropsValue(gameResourceType, 0));

            _propsesValues[propsTypeIndex].Amount += addAmount;
        }

        public void AddProps(PropsValue resourceValue)
        {
            if (!TryGetPropsInventoryIndex(resourceValue.GameResourceType, out var propsTypeIndex))
                _propsesValues.Add(new PropsValue(resourceValue.GameResourceType, 0));

            _propsesValues[propsTypeIndex].Amount += resourceValue.Amount;
        }

        public void TakeProps(GameResourceType gameResourceType, int takeAmount)
        {
            if (TryGetPropsInventoryIndex(gameResourceType, out var propsTypeIndex))
            {
                int amount = _propsesValues[propsTypeIndex].Amount;
                if (amount >= takeAmount)
                    _propsesValues[propsTypeIndex].Amount -= takeAmount;
            }
        }


        private bool TryGetPropsInventoryIndex(GameResourceType gameResourceType, out int propsTypeIndex)
        {
            propsTypeIndex = -1;
            for (int i = 0; i < _propsesValues.Count; i++)
                if (_propsesValues[i].GameResourceType == gameResourceType)
                {
                    propsTypeIndex = i;
                    return true;
                }

            return false;
        }

        public bool IsEnoughPropsAmount(GameResourceType gameResourceType, int requiredAmount)
        {
            return GetResourcesAmountByType(gameResourceType) >= requiredAmount;
        }

        public bool HaveRequiredProps(GameResourceType gameResourceType)
        {
            return GetResourcesAmountByType(gameResourceType) > 0;
        }

        public int GetResourcesAmountByType(GameResourceType gameResourceType)
        {
            if (!TryGetPropsInventoryIndex(gameResourceType, out var propsTypeIndex))
                throw new IndexOutOfRangeException("Cannot GetPropsInventoryIndex");

            int inventoryAmount = _propsesValues[propsTypeIndex].Amount;
            return inventoryAmount;
        }
    }
}