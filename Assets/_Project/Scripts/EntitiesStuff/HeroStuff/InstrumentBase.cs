using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class InstrumentBase : MonoBehaviour
    {
        private Vector3 _scale;
        public bool IsActive { get; private set; }

        private void Awake()
        {
            _scale = transform.localScale;
        }

        public void Activate(float activateDuration)
        {
            IsActive = true;
            transform.DOKill();
            // transform.DOScale(_scale, activateDuration).From(0f).SetEase(Ease.OutBack).Play();
            transform.localScale = _scale;
            // .onComplete += () => 
        }

        public void Deactivate(bool isSmooth, float deactivateDuration = 0f)
        {
            IsActive = false;

            if (isSmooth)
            {
                transform.DOKill();
                transform.DOScale(0f, deactivateDuration).From(transform.localScale).SetEase(Ease.InBack).Play();
            }
            else
                transform.localScale = Vector3.zero;
        }
    }
}