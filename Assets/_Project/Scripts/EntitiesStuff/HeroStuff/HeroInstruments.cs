using System;
using System.Collections.Generic;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Mechanics;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities.Constants;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class HeroInstruments : MonoBehaviour
    {
        [Header("Sub Behaviours")] [SerializeField]
        private float _activateDuration = 0.2f;

        [SerializeField] private float _deactivateDuration = 0.2f;
        [SerializeField] private float _deactivateDelay = 1f;

        [Header("Sub Behaviours")] [SerializeField]
        private InstrumentValue[] _instrumentValues;

        [SerializeField] private TrailRenderer _trailRenderer;

        private AssetService _assetService;

        public Dictionary<InstrumentType, float> InstrumentDamages { get; } = new Dictionary<InstrumentType, float>(3);

        public float ActivateDuration => _activateDuration;

        public float DeactivateDelay => _deactivateDelay;

        public Action<InstrumentType> InstrumentIncreaseLevelAction = type => { };

        [Inject]
        private void Construct(AssetService assetService)
        {
            _assetService = assetService;
        }

        private void Start()
        {
            SpawnInstruments();
            DeactivateInstruments(false);
        }

        public void SetActiveTrail(bool active)
        {
            _trailRenderer.gameObject.SetActive(active);
        }

        public void SetTrailColor(Color color)
        {
            _trailRenderer.startColor = color;
        }

        private void SpawnInstruments()
        {
            foreach (InstrumentValue instrument in _instrumentValues)
            {
                InstrumentType instrumentType = instrument.InstrumentType;
                var instrumentSettings = GetInstrumentSettings(instrumentType.ToString(), false);
                InstrumentDamages.Add(instrumentType, instrumentSettings.Damage);
                SpawnInstrument(instrumentSettings, instrumentType);
            }
        }

        public void ChangeInstrument(InstrumentSettings instrumentSettings, InstrumentType instrumentType)
        {
            DeSpawnInstrument(instrumentType);
            SpawnInstrument(instrumentSettings, instrumentType);
        }

        private void DeSpawnInstrument(InstrumentType instrumentType)
        {
            InstrumentBase instrumentParent = GetInstrumentParentByType(instrumentType);
            Destroy(instrumentParent.transform.GetChild(0).gameObject);
        }

        private void SpawnInstrument(InstrumentSettings instrumentSettings, InstrumentType instrumentType)
        {
            InstrumentBase instrumentParent = GetInstrumentParentByType(instrumentType);
            Instantiate(instrumentSettings.Prefab, instrumentParent.transform, false);
        }

        private InstrumentBase GetInstrumentParentByType(InstrumentType instrumentType)
        {
            foreach (InstrumentValue instrumentValue in _instrumentValues)
            {
                if (instrumentValue.InstrumentType == instrumentType)
                    return instrumentValue.Instrument;
            }

            throw new Exception("Something went wrong ]:");
        }

        public void DeactivateInstruments(bool isSmooth)
        {
            foreach (InstrumentValue instrument in _instrumentValues)
            {
                instrument.Instrument.gameObject.SetActive(true);
                instrument.Instrument.Deactivate(isSmooth, _deactivateDuration);
            }
        }

        public InstrumentBase GetInstrumentByType(InstrumentType instrumentType)
        {
            foreach (InstrumentValue instrument in _instrumentValues)
                if (instrument.InstrumentType == instrumentType)
                    return instrument.Instrument;

            throw new Exception($"Instrument with type [{instrumentType}] doesn't exist!");
        }

        private bool HaveActiveInstruments(out InstrumentValue inst)
        {
            inst = null;
            foreach (var instrument in _instrumentValues)
            {
                if (instrument.Instrument.IsActive)
                {
                    inst = instrument;
                    return true;
                }
            }

            return false;
        }

        public void TryDeactivateActive()
        {
            if (HaveActiveInstruments(out var instrument))
            {
                instrument.Instrument.Deactivate(true, _deactivateDuration);
                DeactivateNotSuitable(instrument.InstrumentType);
            }
        }

        private void DeactivateNotSuitable(InstrumentType instrumentType)
        {
            foreach (var instrument in _instrumentValues)
                if (instrument.InstrumentType != instrumentType)
                    instrument.Instrument.Deactivate(false);
        }

        public InstrumentSettings GetInstrumentSettings(string instrumentType, bool isNextIndex)
        {
            var index = GetLevel(instrumentType);
            return _assetService.GetScriptableObjectByIndex<InstrumentSettings>(
                $"{AssetPath.Instruments}/{instrumentType}", $"{instrumentType}",
                isNextIndex ? index + 1 : index);
        }

        public int GetLevel(string instrumentType) =>
            PlayerPrefs.GetInt(GetInstrumentLevelPrefsName(instrumentType));

        public void IncreaseInstrumentLevel(string instrumentType)
        {
            string prefsName = GetInstrumentLevelPrefsName(instrumentType);
            int newLevel = PlayerPrefs.GetInt(prefsName) + 1;
            PlayerPrefs.SetInt(prefsName, newLevel);
            InstrumentType key = EnumTools.GetElementByName<InstrumentType>(typeof(InstrumentType), instrumentType);
            InstrumentDamages[key] = GetInstrumentSettings(instrumentType, false).Damage;
        }

        private static string GetInstrumentLevelPrefsName(string instrumentType) =>
            $"Instrument_{instrumentType}_level";

        [Serializable]
        public class InstrumentValue
        {
            [SerializeField] private InstrumentType _instrumentType;
            [SerializeField] private InstrumentBase _instrument;

            public InstrumentBase Instrument => _instrument;

            public InstrumentType InstrumentType => _instrumentType;
        }
    }
}