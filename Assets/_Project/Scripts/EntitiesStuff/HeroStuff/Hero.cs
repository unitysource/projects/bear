using System;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff.AIStuff.AICharacters;
using _Project.Scripts.FactoriesStuff;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    public class Hero : MonoBehaviour, IEntity
    {
        [SerializeField] private float _respawnDelay = 0.1f;

        [SerializeField] private EntityModel _model;

        private EntityDeath _entityDeath;
        private Health _health;
        private Vector3 _startPoint;
        private EntityMovement _movement;
        private HeroInteractable _heroInteractable;
        private HeroInstruments _heroInstruments;
        private AudioService _audioService;
        private Joystick _joystick;

        [Inject]
        private void Construct(AudioService audioService, Joystick joystick)
        {
            _joystick = joystick;
            _audioService = audioService;
        }

        private void Start()
        {
            _movement = GetComponent<EntityMovement>();
            _entityDeath = GetComponent<EntityDeath>();
            _heroInteractable = GetComponent<HeroInteractable>();
            _heroInstruments = GetComponent<HeroInstruments>();
            _health = GetComponent<Health>();

            _startPoint = transform.position;
            _health.Dead += OnDead;
            // _joystick.CanUse = true;
        }

        private void OnDestroy()
        {
            _health.Dead -= OnDead;
        }

        private void OnDead(DamageType damageType)
        {
            _audioService.PlaySound(AudioClipId.hero_death);
            
            _entityDeath.Deactivate();
            _heroInteractable.CanInteract = false;
            _heroInstruments.DeactivateInstruments(false);
            _joystick.CanUse = false;

            switch (damageType)
            {
                case DamageType.Water:
                    AnalyticsManager.GameFailed(FailType.FailInWater);
                    break;
                case DamageType.Entity:
                    AnalyticsManager.GameFailed(FailType.FailEnemy);
                    break;
            }
            
            DOVirtual.DelayedCall(_respawnDelay, Spawn);
        }

        private void Spawn()
        {
            transform.position = _startPoint;
            _heroInteractable.CanInteract = true;
            _entityDeath.Activate();
            _movement.ResetDirection();
            _health.Initialize();
            
            _joystick.CanUse = true;
        }

        public EntityType EntityType => EntityType.Hero;
    }
}