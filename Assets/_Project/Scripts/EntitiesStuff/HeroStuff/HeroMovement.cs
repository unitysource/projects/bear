using System;
using System.Linq;
using _Project.Packs.HexPlanet.Scripts;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.EntitiesStuff.HeroStuff
{
    [RequireComponent(typeof(HeroAnimator), typeof(GravityBody))]
    [SelectionBase]
    public class HeroMovement : EntityMovement
    {
        [SerializeField] private HeroProperties _heroProperties;

        [SerializeField] private ParticleSystem _waterParticlesPrefab;
        [SerializeField] private Transform _waterParticlesPoint;

        private Joystick _joystick;
        private Vector3 _moveAmount;
        private Vector3 _smoothMoveVelocity;
        private bool _isMoving;
        private CameraManager _cameraManager;
        private LoopedAudio _swimLoopedAudio;
        private EntitySurface _entitySurface;
        private AudioService _audioService;

        private float _walkAudioTimer;
        private float _walkAudioMax = 0.4f;

        private float _waterParticleTimer;
        private readonly MinMaxFloat _waterParticleMax = new MinMaxFloat(0.5f, 0.7f);
        private RandomService _randomService;
        private IPoolService _poolService;
        private const string ContainerName = "WaterParticlesPool";

        [Inject]
        private void Construct(Joystick joystick, AudioService audioService, RandomService randomService, IPoolService poolService)
        {
            _poolService = poolService;
            _randomService = randomService;
            _audioService = audioService;
            _joystick = joystick;
            _cameraManager = FindObjectOfType<CameraManager>();

            var loopedAudios = GetComponents<LoopedAudio>();
            _swimLoopedAudio = loopedAudios.First(l => l.ID == 1);

            _entitySurface = GetComponent<EntitySurface>();

            FillWaterParticlesPool();
        }

        private void FillWaterParticlesPool()
        {
            _poolService.FillPool(PoolInfo.Create(_waterParticlesPrefab.name, 50, _waterParticlesPrefab.gameObject, ContainerName));
        }

        private void Update()
        {
            if (!_health.IsAlive) return;

            if (_joystick.IsInputAboveZero())
            {
                RotateByDirection(_joystick.GetInput());
            }

            CalculateMovement(_joystick.GetInput());
            AnimateMovement(_joystick.GetInput());
        }

        private void FixedUpdate()
        {
            if (_joystick.IsInputAboveZero())
            {
                if (!_isMoving)
                {
                    _isMoving = true;
                    MoveParticlesSetActive(true);
                    _cameraManager.StandUnZoom();
                }

                switch (_entitySurface.SurfaceState)
                {
                    case EntitySurfaceState.Grounded:
                        if (_walkAudioTimer >= _walkAudioMax * (1.5f / (_joystick.Direction.magnitude + 0.5f)))
                        {
                            _audioService.PlaySound(AudioClipId.entity_run_0);
                            _walkAudioTimer = 0f;
                        }

                        _walkAudioTimer += Time.deltaTime;
                        break;
                    case EntitySurfaceState.Swimming:
                        _swimLoopedAudio.FixedPlayAudio();

                        if (_waterParticleTimer >= _randomService.GetValue(_waterParticleMax))
                        {
                            GameObject instance = _poolService.GetPoolObject(_waterParticlesPrefab.name);
                            instance.GetComponent<ParticlesReturnToPool>().Initialize(_waterParticlesPrefab.name);
                            instance.transform.position = _waterParticlesPoint.position;
                            instance.transform.rotation = transform.rotation;
                            instance.SetActive(true);
                            _waterParticleTimer = 0f;
                        }

                        _waterParticleTimer += Time.deltaTime;
                        break;
                }

                Move();
            }
            else if (!_joystick.IsInputAboveZero() && _isMoving)
            {
                _isMoving = false;
                MoveParticlesSetActive(false);
                _cameraManager.StandZoom();
            }
        }

        private void MoveParticlesSetActive(bool active)
        {
            _moveParticles.gameObject.SetActive(false);
            _swimParticles.gameObject.SetActive(false);

            switch (_entitySurface.SurfaceState)
            {
                case EntitySurfaceState.Grounded:
                    _moveParticles.gameObject.SetActive(active);
                    break;
                case EntitySurfaceState.Swimming:
                    _swimParticles.gameObject.SetActive(active);
                    break;
            }
        }

        public override void Move()
        {
            Vector3 localMove = _cashedTransform.TransformDirection(_moveAmount) * Time.fixedDeltaTime;
            _rb.MovePosition(_rb.position + localMove);
        }

        private void CalculateMovement(Vector3 moveDir)
        {
            const float smoothTime = 0.15f;
            Vector3 targetMoveAmount = moveDir * _moveSpeed;
            _moveAmount = Vector3.SmoothDamp(_moveAmount, targetMoveAmount, ref _smoothMoveVelocity, smoothTime);
        }
    }
}