using _Project.Packs.HexPlanet.Scripts;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    [RequireComponent(typeof(Rigidbody))]
    public class StairClimb : MonoBehaviour
    {
        [Header("Properties")] [SerializeField]
        float stepSmooth = 2f;

        [SerializeField] private LayerMask _climbLayerMask;

        [Header("Sub Behaviours")] [SerializeField]
        private Transform _upperRayTransform;

        [SerializeField] private Transform _lowerRayTransform;
        [SerializeField] private Transform _heroMeshTransform;

        private GravityBody _gravityBody;
        private const float RayDistance = 0.6f;

        private void Start()
        {
            _gravityBody = GetComponent<GravityBody>();
        }

        private void Update()
        {
            StepClimb();
        }

        private void StepClimb()
        {
            if (CheckRaycast(_upperRayTransform, RayDistance*2f))
            {
                return;
            }

            if (!CheckRaycast(_upperRayTransform, RayDistance*2f) && CheckRaycast(_lowerRayTransform, RayDistance))
            {
                if (_gravityBody.IsAttractActive())
                    _gravityBody.ChangeAttractActivation(false);

                transform.Translate(Vector3.up * (Time.fixedDeltaTime * stepSmooth), _heroMeshTransform);
            }

            if (!CheckRaycast(_lowerRayTransform, RayDistance) && !_gravityBody.IsAttractActive())
            {
                const float duration = 0.09f;
                transform.DOMove(
                    transform.position + _lowerRayTransform.TransformDirection(Vector3.forward) * RayDistance,
                    duration).Play();
                _gravityBody.ChangeAttractActivation(true);
            }
        }

        private bool CheckRaycast(Transform rayTransform, float distance)
        {
#if UNITY_EDITOR
            var position = rayTransform.position;
            Debug.DrawLine(position,
                position + rayTransform.TransformDirection(Vector3.forward) * distance, Color.black);
#endif
            return Physics.Raycast(rayTransform.position,
                       rayTransform.TransformDirection(Vector3.forward), out var hit, distance, _climbLayerMask) &&
                   !hit.collider.isTrigger;
        }
    }
}