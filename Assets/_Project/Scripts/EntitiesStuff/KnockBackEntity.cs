using System;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    [RequireComponent(typeof(Rigidbody))]
    public class KnockBackEntity : MonoBehaviour
    {
        private Rigidbody _rb;

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _rb = GetComponent<Rigidbody>();
        }

        public void KnockBack(Vector3 direction, float force)
        {
            direction = direction.normalized;
            _rb.AddForce(direction * force, ForceMode.Impulse);
        }
    }
}