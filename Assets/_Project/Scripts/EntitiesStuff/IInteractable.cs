using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public interface IInteractable
    {
        void Interact(HeroInteractable hero, float damage, out Health health);

        bool CanInteract { get; set; }
        
        InstrumentType GetInteractionInstrumentType();

        GameResourceType[] GetSeeResourcesTypes();

        GameResourceType GetInteractionResourceType();
    }

    public enum InstrumentType
    {
        Axe = 0,
        Pick = 1,
        Sword = 2,
    }
}