using _Project.Scripts.EntitiesStuff.AIStuff.AI;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    public class HeroSurface : EntitySurface
    {
        [SerializeField] private HeroProperties _heroProperties;
        [SerializeField] private ParticleSystem _waterDeathParticles;

        [SerializeField] private Collider _groundCollider;
        [SerializeField] private Collider _waterCollider;

        private Health _health;
        private float _takeDamageTimer;
        private bool _isHeroInWater;

        private void Awake()
        {
            _health = GetComponent<Health>();
            _groundCollider.enabled = true;
            _waterCollider.enabled = false;
        }

        protected override void SetGrounded()
        {
            _isHeroInWater = false;
            _waterDeathParticles.Stop();

            SetColliders(true);
            base.SetGrounded();
        }

        protected override void SetSwimming()
        {
            _isHeroInWater = true;
            _waterDeathParticles.Play();
            SetColliders(false);
            base.SetSwimming();
        }

        private void SetColliders(bool activate)
        {
            _groundCollider.enabled = activate;
            _waterCollider.enabled = !activate;
        }

        private void FixedUpdate()
        {
            if (_takeDamageTimer >= _heroProperties.WaterDeathTime && _isHeroInWater)
            {
                _health.TakeDamage(0.5f, DamageType.Water);
                _takeDamageTimer = 0f;
            }

            _takeDamageTimer += Time.deltaTime;
        }
    }
}