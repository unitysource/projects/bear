using System;
using Assets._Project.Scripts.Utilities.Constants;
using UnityEngine;

namespace _Project.Scripts.EntitiesStuff
{
    [RequireComponent(typeof(EntityAnimator), typeof(Health))]
    public class EntitySurface : MonoBehaviour
    {
        protected EntitySurfaceState _entitySurfaceState;
        protected EntityAnimator _entityAnimator;

        public EntitySurfaceState SurfaceState => _entitySurfaceState;

        private void Start()
        {
            _entityAnimator = GetComponent<EntityAnimator>();
            _entitySurfaceState = EntitySurfaceState.Grounded;
        }

        private void OnCollisionEnter(Collision col)
        {
            CollisionEnter(col);
        }

        private void CollisionEnter(Collision col)
        {
            int groundMask = LayerMask.NameToLayer(Layers.Ground);
            int waterMask = LayerMask.NameToLayer(Layers.Water);

            if (col.gameObject.layer == groundMask && _entitySurfaceState != EntitySurfaceState.Grounded)
            {
                SetGrounded();
            }

            if (col.gameObject.layer == waterMask && _entitySurfaceState != EntitySurfaceState.Swimming)
            {
                SetSwimming();
                // Debug.Log($"col {col.gameObject.name}");

                if (col.gameObject.layer == groundMask && SurfaceState != EntitySurfaceState.Grounded)
                {
                    // Debug.Log($"ground");
                    _entitySurfaceState = EntitySurfaceState.Grounded;
                    _entityAnimator.ChangeState(SurfaceState);
                }

                if (col.gameObject.layer == waterMask && SurfaceState != EntitySurfaceState.Swimming)
                {
                    // Debug.Log($"water");
                    _entitySurfaceState = EntitySurfaceState.Swimming;
                    _entityAnimator.ChangeState(SurfaceState);
                }
            }
        }

        protected virtual void SetGrounded()
        {
            _entitySurfaceState = EntitySurfaceState.Grounded;
            _entityAnimator.ChangeState(_entitySurfaceState);
        }

        protected virtual void SetSwimming()
        {
            _entitySurfaceState = EntitySurfaceState.Swimming;
            _entityAnimator.ChangeState(_entitySurfaceState);
        }
    }
}