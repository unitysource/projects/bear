using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class FinalWindow : WindowBase
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _clearDataButton;
        private ISaveLoadService _saveLoadService;

        public override void Initialize()
        {
            base.Initialize();

            CloseWindow(false);

            _restartButton.onClick.AddListener(OnRestart);
            _clearDataButton.onClick.AddListener(OnClearData);
        }

        [Inject]
        private void Construct(ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
        }

        private void OnDestroy()
        {
            _restartButton.onClick.RemoveListener(OnRestart);
            _clearDataButton.onClick.RemoveListener(OnClearData);
        }

        private void OnRestart()
        {
            CommonTools.RestartScene();
        }

        private void OnClearData()
        {
            PlayerPrefs.DeleteAll();
            SaveManager.DeleteSave();
            _saveLoadService.DeleteAll();
            
            CommonTools.RestartScene();
        }

        public override WindowType GetWindowType() => WindowType.Final;
    }
}