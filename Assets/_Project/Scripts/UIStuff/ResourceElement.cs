﻿using System.Collections;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class ResourceElement : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private TMP_Text _tmpText;
        private float _currentLifetime;
        private float _lifeDuration;
        private ResourcesHUD _resourcesHUD;

        public GameResourceType ResourceType { get; set; }

        public void Construct(ResourcesHUD resourcesHUD)
        {
            _resourcesHUD = resourcesHUD;

            Initialize();
        }

        private void Initialize()
        {
            _lifeDuration = _resourcesHUD.LifeDuration;

            UpdateSprite();
        }

        public void Activate(bool active)
        {
            if (gameObject.activeSelf == active) return;

            if (active)
            {
                _currentLifetime = 0;
                _resourcesHUD.AddAnimate(true);
                UpdateAmount();
                gameObject.SetActive(true);
                DOVirtual.DelayedCall(_resourcesHUD.StretchDuration, () =>
                {
                    StartCoroutine(nameof(IncreaseTimerRoutine));
                });
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private IEnumerator IncreaseTimerRoutine()
        {
            while (gameObject.activeSelf)
            {
                if (!IsAlive)
                {
                    if (!_resourcesHUD.LastActiveElement)
                    {
                        _resourcesHUD.RemoveAnimate(true);
                        Activate(false);
                    }
                }

                _currentLifetime += Time.deltaTime;
                yield return null;
            }
        }

        private void UpdateSprite()
        {
            _image.sprite = _resourcesHUD.ResourcesService.GetSprite(ResourceType);
        }

        public void UpdateAmount()
        {
            CommonTools.UpdateText(_tmpText, _resourcesHUD.Inventory.GetResourcesAmountByType(ResourceType));
        }

        public float RemainingTime => _lifeDuration - _currentLifetime;

        private bool IsAlive => RemainingTime >= 0;

        public bool IsActive => gameObject.activeSelf;

        public void ResetLifetime() => _currentLifetime = 0f;
    }
}