using System;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using Assets._Project.Scripts.Utilities;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class AudioConfigUI : MonoBehaviour
    {
        [SerializeField] private Slider _musicSlider;
        [SerializeField] private Slider _soundsSlider;

        [SerializeField] private Button _musicButton;
        [SerializeField] private Button _soundsButton;

        [SerializeField] private Image _musicImage;
        [SerializeField] private Image _soundsImage;

        [SerializeField] private Sprite _musicOnSprite;
        [SerializeField] private Sprite _musicOffSprite;

        [SerializeField] private Sprite _soundsOnSprite;
        [SerializeField] private Sprite _soundsOffSprite;

        private AudioService _audioService;
        private ISaveLoadService _saveLoadService;

        [Inject]
        private void Construct(AudioService audioService, ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
            _audioService = audioService;
        }

        public void Initialize()
        {
            UpdateMuteSprite(AudioType.Sounds);
            UpdateMuteSprite(AudioType.Music);

            UpdateSliderValue(AudioType.Sounds);
            UpdateSliderValue(AudioType.Music);

            _musicButton.onClick.AddListener(ChangeMusicMute);
            _soundsButton.onClick.AddListener(ChangeSoundsMute);

            _musicSlider.onValueChanged.AddListener(MusicVolumeChange);
            _soundsSlider.onValueChanged.AddListener(SoundsVolumeChange);
        }

        private void UpdateSliderValue(AudioType audioType)
        {
            switch (audioType)
            {
                case AudioType.Sounds:
                    _soundsSlider.value = _saveLoadService.GameConfig.SoundsVolumeMultiplier;
                    break;
                case AudioType.Music:
                    _musicSlider.value = _saveLoadService.GameConfig.MusicVolumeMultiplier;
                    break;
            }
        }

        private void SoundsVolumeChange(float value)
        {
            _saveLoadService.GameConfig.SoundsVolumeMultiplier = value;
            _saveLoadService.SaveAllData();

            _audioService.UpdateSoundsVolume();
        }

        private void MusicVolumeChange(float value)
        {
            _saveLoadService.GameConfig.MusicVolumeMultiplier = value;
            _saveLoadService.SaveAllData();

            _audioService.UpdateMusicVolume();
        }

        private void ChangeMusicMute()
        {
            CommonTools.ChangeIntValue(ref _saveLoadService.GameConfig.MusicOn);
            _saveLoadService.SaveAllData();
            _audioService.UpdateMusicMute();

            UpdateMuteSprite(AudioType.Music);
        }

        private void ChangeSoundsMute()
        {
            CommonTools.ChangeIntValue(ref _saveLoadService.GameConfig.SoundsOn);
            _saveLoadService.SaveAllData();
            _audioService.UpdateSoundsMute();

            UpdateMuteSprite(AudioType.Sounds);
        }

        private void UpdateMuteSprite(AudioType audioType)
        {
            switch (audioType)
            {
                case AudioType.Sounds:
                    _soundsImage.sprite = CommonTools.IntToBool(_saveLoadService.GameConfig.SoundsOn)
                        ? _soundsOnSprite
                        : _soundsOffSprite;
                    break;
                case AudioType.Music:
                    _musicImage.sprite = CommonTools.IntToBool(_saveLoadService.GameConfig.MusicOn)
                        ? _musicOnSprite
                        : _musicOffSprite;
                    break;
            }
        }
    }

    public enum AudioType
    {
        Sounds,
        Music
    }
}