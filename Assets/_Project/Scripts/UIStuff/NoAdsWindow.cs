using System;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Architecture.Services.UIStuff;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class NoAdsWindow : WindowBase
    {
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private Button _closeButton;
        [SerializeField] private IAPButton _disableAdsButton;
        private ISaveLoadService _saveLoadService;
        private IAdsService _adsService;

        private void Awake()
        {
            DOVirtual.DelayedCall(0.1f, () =>
            {
                _disableAdsButton.onPurchaseComplete.AddListener(OnBuyComplete);
                _disableAdsButton.onPurchaseFailed.AddListener(OnBuyFailed);

                var product = CodelessIAPStoreListener.Instance.GetProduct(_disableAdsButton.productId);
                _priceText.text = product.metadata.localizedPriceString;
            });
        }

        public override void Initialize()
        {
            base.Initialize();

            CloseWindow(false);

            _closeButton.onClick.AddListener(OnResume);
        }

        [Inject]
        private void Construct(IAdsService adsService)
        {
            _adsService = adsService;
        }


        private void OnBuyFailed(Product arg0, PurchaseFailureReason arg1)
        {
        }

        private void OnBuyComplete(Product product)
        {
            if (!_adsService.CanShowInterstitial()) return;

            _adsService.DisableAd();
            AnalyticsManager.AdBlockBought();
        }

        private void OnDestroy()
        {
            _closeButton.onClick.RemoveListener(OnResume);
            _disableAdsButton.onPurchaseComplete.RemoveListener(OnBuyComplete);
            _disableAdsButton.onPurchaseFailed.RemoveListener(OnBuyFailed);
        }

        private void OnResume()
        {
            _uiService.UnloadWindow(GetWindowType());
            _uiService.OpenWindow(_uiService.IsGameplayStarted ? WindowType.HUD : WindowType.Open, false, true);
        }

        public override WindowType GetWindowType() => WindowType.NoAds;
    }
}