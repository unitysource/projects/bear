using _Project.Scripts.GameTools;
using UnityEngine;

namespace _Project.Scripts.UIStuff
{
    public class PetPurchaseUI : BaseToolUI
    {
        [SerializeField] private RecycleToolButton _petPurchaseButton;
        
        public override void Initialize(RecycleToolBase recycleTool)
        {
            _recycleTool = recycleTool;

            _petPurchaseButton.Initialize(recycleTool);
            _missingResourcesUI.Initialize();
        }
        
        public override void Activate()
        {
            _petPurchaseButton.Activate(_recycleTool.UISettings.OpenDuration);
        }
        
        public override void Deactivate()
        {
            _petPurchaseButton.Deactivate(_recycleTool.UISettings.CloseDuration);
        }

        public override void UpdateUI(int spendResourceAmount)
        {
            bool isHeroHaveRequiredResources = spendResourceAmount >= _recycleTool.ResultMultiplier;
            _petPurchaseButton.SetColor(isHeroHaveRequiredResources);
            _petPurchaseButton.UpdateValues(_recycleTool.ResultMultiplier, 0);
        }
    }
}