using _Project.Scripts.Architecture.Services.UIStuff;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class OpenGameWindow : WindowBase
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _openOptionsButton;
        [SerializeField] private Button _noAdsButton;

        public override void Initialize()
        {
            base.Initialize();

            _playButton.onClick.AddListener(Play);
            _openOptionsButton.onClick.AddListener(OpenOptions);
            _noAdsButton.onClick.AddListener(OpenNoAds);
        }

        private void OnDestroy()
        {
            _openOptionsButton.onClick.RemoveListener(OpenOptions);
            _noAdsButton.onClick.RemoveListener(OpenNoAds);
        }

        private void Play()
        {
            _uiService.OpenWindow(WindowType.HUD, true, true);
            _uiService.TriggerEvent(UIEventType.GameplayStartedAction);
            _uiService.IsGameplayStarted = true;
            _uiService.UnloadWindow(GetWindowType());
        }

        private void OpenOptions()
        {
            DOVirtual.DelayedCall(_uiSettings.OpenDuration, () =>
            {
                Time.timeScale = 0f;
            });
            
            _uiService.OpenWindow(WindowType.Options, true, true);
            CloseWindow(false);
        }

        private void OpenNoAds()
        {
            _uiService.OpenWindow(WindowType.NoAds, true, true);
            CloseWindow(false);
        }

        public override WindowType GetWindowType() => WindowType.Open;
    }
}