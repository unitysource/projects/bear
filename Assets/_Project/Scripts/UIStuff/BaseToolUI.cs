﻿using _Project.Scripts.GameTools;
using UnityEngine;

namespace _Project.Scripts
{
    public abstract class BaseToolUI : MonoBehaviour
    {
        [SerializeField] protected MissingResourcesUI _missingResourcesUI;

        protected RecycleToolBase _recycleTool;
        
        public MissingResourcesUI MissingResourcesUI => _missingResourcesUI;

        public abstract void Initialize(RecycleToolBase recycleTool);

        public abstract void Activate();

        public abstract void Deactivate();

        public abstract void UpdateUI(int spendResourceAmount);
    }
}