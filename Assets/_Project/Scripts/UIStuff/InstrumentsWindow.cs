using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.Behaviours;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class InstrumentsWindow : WindowBase
    {
        [SerializeField] private float _stretchDuration = 0.2f;
        [SerializeField] private InstrumentElement _instrumentElementPrefab;
        [SerializeField] private Transform _groupParent;

        private RectTransform _rectTransform;
        private Button _button;
        private Vector2 _minHeight, _maxHeight;
        private int _instrumentsAmount;
        private DiContainer _diContainer;

        private List<InstrumentElement> _instrumentElements;
        private HeroInteractable _heroInteractable;
        private List<InstrumentType> _instrumentTypes;
        private HeroInstruments _heroInstruments;
        private InstrumentType _currentType;

        [Inject]
        private void Construct(DiContainer diContainer, HeroInventory heroInventory)
        {
            _diContainer = diContainer;
            _heroInstruments = heroInventory.GetComponent<HeroInstruments>();
            _heroInteractable = heroInventory.GetComponent<HeroInteractable>();
        }

        public override void Initialize()
        {
            _rectTransform = GetComponent<RectTransform>();
            _button = GetComponent<Button>();
            var sizeDelta = _rectTransform.sizeDelta;
            _instrumentTypes = Enum.GetValues(typeof(InstrumentType)).Cast<InstrumentType>().ToList();
            _instrumentsAmount = _instrumentTypes.Count;

            _minHeight = new Vector2(sizeDelta.x, sizeDelta.y);
            _maxHeight = new Vector2(sizeDelta.x, sizeDelta.y * _instrumentsAmount);

            _button.onClick.AddListener(OnButtonClicked);
            ChangeState(WindowState.Closed);

            CreateInstrumentElements();

            _instrumentElements.Last().ChangeActive(true);
            _heroInteractable.InteractionInstrumentAction += OnHeroInteract;
            _heroInstruments.InstrumentIncreaseLevelAction += OnInstrumentIncreaseLevel;
            _currentType = _instrumentElements.Last().InstrumentType;
        }

        private void OnHeroInteract(InstrumentType instrumentType)
        {
            if (instrumentType != _currentType)
            {
                _instrumentTypes.Move(_instrumentTypes.Find(type => type == instrumentType), _instrumentTypes.Count);
                
                for (int i = 0; i < _instrumentsAmount; i++)
                {
                    var instrumentElement = _instrumentElements[i];
                    instrumentElement.InstrumentType = _instrumentTypes[i];
                    UpdateElementValues(instrumentElement);
                }

                _currentType = instrumentType;
            }
        }

        private void CreateInstrumentElements()
        {
            _instrumentElements = new List<InstrumentElement>();
            for (int i = 0; i < _instrumentsAmount; i++)
            {
                _instrumentElements.Add(_diContainer.InstantiatePrefab(_instrumentElementPrefab, _groupParent)
                    .GetComponent<InstrumentElement>());

                var element = _instrumentElements[i];
                element.InstrumentType = _instrumentTypes[i];
                element.Construct(_heroInstruments);
                UpdateElementValues(element);

                element.ChangeActive(false);
            }
        }

        private static void UpdateElementValues(InstrumentElement element)
        {
            element.UpdateSprite();
            element.UpdateLevel();
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
            _heroInteractable.InteractionInstrumentAction -= OnHeroInteract;
            _heroInstruments.InstrumentIncreaseLevelAction -= OnInstrumentIncreaseLevel;
        }

        private void OnInstrumentIncreaseLevel(InstrumentType instrumentType)
        {
            var instrumentElement = _instrumentElements.Find(element => element.InstrumentType == instrumentType);
            UpdateElementValues(instrumentElement);
        }

        private void OnButtonClicked()
        {
            if (IsWindowOpened)
            {
                CloseWindow(true);

                foreach (var instrumentElement in _instrumentElements)
                    instrumentElement.ChangeActive(false);
                _instrumentElements.Last().ChangeActive(true);
            }
            else
            {
                OpenWindow(true);
                DOVirtual.DelayedCall(_stretchDuration, () =>
                {
                    foreach (var instrumentElement in _instrumentElements)
                        instrumentElement.ChangeActive(true);
                });
            }
        }

        protected override void OpenAnimate(bool isSmooth)
        {
            _rectTransform.DOSizeDelta(_maxHeight, _stretchDuration).From(_minHeight).Play();
        }

        protected override void CloseAnimate(bool isSmooth)
        {
            _rectTransform.DOSizeDelta(_minHeight, _stretchDuration).From(_maxHeight).Play();
        }

        public override WindowType GetWindowType() => WindowType.InstrumentsHUD;
    }
}