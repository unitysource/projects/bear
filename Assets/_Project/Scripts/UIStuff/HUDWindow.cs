using _Project.Scripts.Architecture.Services.UIStuff;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class HUDWindow : WindowBase
    {
        [SerializeField] private Button _openBackpackButton;
        [SerializeField] private Button _viewBackpackButton;
        [SerializeField] private Button _openOptionsButton;
        [SerializeField] private Button _noAdsButton;

        [SerializeField] private WindowBase _instrumentsWindow;
        [SerializeField] private ResourcesHUD _resourcesHUDBlock;

        public override void Initialize()
        {
            base.Initialize();

            _openBackpackButton.onClick.AddListener(OpenBackpack);
            _viewBackpackButton.onClick.AddListener(OpenBackpack);
            _openOptionsButton.onClick.AddListener(OpenOptions);
            _noAdsButton.onClick.AddListener(OpenNoAds);
            
            _instrumentsWindow.Initialize();
            _resourcesHUDBlock.Initialize();
        }

        private void OnDestroy()
        {
            _openBackpackButton.onClick.RemoveListener(OpenBackpack);
            _viewBackpackButton.onClick.RemoveListener(OpenBackpack);
            _openOptionsButton.onClick.RemoveListener(OpenOptions);
            _noAdsButton.onClick.RemoveListener(OpenNoAds);
        }

        private void OpenBackpack()
        {
            _uiService.OpenWindow(WindowType.Backpack, true, true);
            CloseWindow(true);
        }

        private void OpenOptions()
        {
            DOVirtual.DelayedCall(_uiSettings.OpenDuration, () =>
            {
                Time.timeScale = 0f;
            });
            
            _uiService.OpenWindow(WindowType.Pause, true, true);
            CloseWindow(false);
        }

        private void OpenNoAds()
        {
            _uiService.OpenWindow(WindowType.NoAds, true, true);
            CloseWindow(false);
        }

        public override WindowType GetWindowType() => WindowType.HUD;
    }
}