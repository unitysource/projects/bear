﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class ResourcesHUD : MonoBehaviour
    {
        [SerializeField] private float _stretchDuration = 0.2f;
        [SerializeField] private ResourceElement _resourceElementPrefab;
        [SerializeField] private Transform _groupParent;
        [SerializeField] private int _maxElementsAmount = 6;
        [SerializeField, Range(1, 100)] private float _lifeDuration;
        [SerializeField] private float _reloadResourcesDelay;

        private RectTransform _rectTransform;
        private Vector2 _minHeight;
        private Vector2 _currentHeight;
        private Vector2 _resourceElementHeight;
        private int _resourcesAmount;
        private DiContainer _diContainer;

        private List<ResourceElement> _resourceElements;
        private HeroInteractable _heroInteractable;
        private List<GameResourceType> _resourceTypes;
        private ResourceElement _oldestResourceElement;
        private float _minRemainingLifetime;

        public float LifeDuration => _lifeDuration;

        public GameResourcesService ResourcesService { get; private set; }

        public HeroInventory Inventory { get; private set; }

        public float StretchDuration => _stretchDuration;

        [Inject]
        private void Construct(DiContainer diContainer, HeroInventory heroInventory,
            GameResourcesService resourcesService, DataService dataService)
        {
            Inventory = heroInventory;
            ResourcesService = resourcesService;
            _diContainer = diContainer;
            _heroInteractable = Inventory.GetComponent<HeroInteractable>();

            _heroInteractable.SeeResourceAction += OnHeroInteract;
        }

        private void OnDestroy()
        {
            _heroInteractable.SeeResourceAction -= OnHeroInteract;
        }

        public void Initialize()
        {
            _rectTransform = GetComponent<RectTransform>();
            var sizeDelta = _rectTransform.sizeDelta;
            _resourceTypes = ResourcesService.GetPriorityHUDResources();
            _resourcesAmount = _resourceTypes.Count;

            _currentHeight = _minHeight = new Vector2(sizeDelta.x, sizeDelta.y);
            _resourceElementHeight = new Vector2(0, _resourceElementPrefab.GetComponent<RectTransform>().sizeDelta.y);

            CreateResourceElements();
            _resourceElements[0].Activate(true);
        }

        private void OnEnable() => BeginUpdateValues();
        private void OnDisable() => StopUpdateValues();

        private void BeginUpdateValues() => StartCoroutine(nameof(UpdateValuesRoutine));
        private void StopUpdateValues() => StopCoroutine(nameof(UpdateValuesRoutine));

        private IEnumerator UpdateValuesRoutine()
        {
            while (gameObject.activeSelf)
            {
                UpdateValues();
                yield return new WaitForSeconds(_reloadResourcesDelay);
            }
        }

        private void CreateResourceElements()
        {
            _resourceElements = new List<ResourceElement>(_resourcesAmount);
            for (int i = 0; i < _resourcesAmount; i++)
            {
                _resourceElements.Add(_diContainer.InstantiatePrefab(_resourceElementPrefab, _groupParent)
                    .GetComponent<ResourceElement>());

                var element = _resourceElements[i];
                element.ResourceType = _resourceTypes[i];
                element.Construct(this);
                element.Activate(false);
            }
        }

        private void OnHeroInteract(GameResourceType[] resourceTypes)
        {
            foreach (var resourceType in resourceTypes.Distinct())
            {
                var element = _resourceElements.Find(element => element.ResourceType == resourceType);
                if (element == null) continue;

                if (!element.IsActive)
                {
                    element.Activate(true);

                    if (_resourceElements.Count(el => el.IsActive) >= _maxElementsAmount)
                    {
                        _minRemainingLifetime = _lifeDuration;
                        _oldestResourceElement = null;
                        foreach (var resourceElement in _resourceElements)
                        {
                            var remainingTime = resourceElement.RemainingTime;
                            if (resourceElement.IsActive && remainingTime < _minRemainingLifetime)
                            {
                                _minRemainingLifetime = remainingTime;
                                _oldestResourceElement = resourceElement;
                            }
                        }

                        if (_oldestResourceElement != null)
                        {
                            _oldestResourceElement.Activate(false);
                            RemoveAnimate(true);
                        }
                    }
                }
                else
                {
                    element.ResetLifetime();
                }
            }
        }

        private void UpdateValues()
        {
            if (_resourceElements == null) return;
            
            foreach (var element in _resourceElements) 
                element.UpdateAmount();
        }

        public void AddAnimate(bool isSmooth)
        {
            Vector2 newHeight = _currentHeight + _resourceElementHeight;
            if (isSmooth)
                _rectTransform.DOSizeDelta(newHeight, _stretchDuration)
                    .From(_currentHeight).Play();
            else
                _rectTransform.sizeDelta = newHeight;

            _currentHeight = newHeight;
        }

        public void RemoveAnimate(bool isSmooth)
        {
            Vector2 newHeight = _currentHeight - _resourceElementHeight;
            if (isSmooth)
                _rectTransform.DOSizeDelta(newHeight, _stretchDuration)
                    .From(_currentHeight).Play();
            else
                _rectTransform.sizeDelta = newHeight;

            _currentHeight = newHeight;
        }

        public bool LastActiveElement => _resourceElements.Count(el => el.IsActive) <= 1;
    }
}