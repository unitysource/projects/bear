﻿using System.Security.AccessControl;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using Assets._Project.Scripts.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class InstrumentElement : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private TMP_Text _tmpText;
        
        private HeroInstruments _heroInstruments;

        public InstrumentType InstrumentType { get; set; }

        public void Construct(HeroInstruments heroInstruments)
        {
            _heroInstruments = heroInstruments;
        }

        public void ChangeActive(bool active)
        {
            if (gameObject.activeSelf != active)
            {
                gameObject.SetActive(active);
            }
        }
        
        public void UpdateSprite()
        {
            var settings = _heroInstruments.GetInstrumentSettings(InstrumentType.ToString(), false);
            _image.sprite = settings.Sprite;
        }
        
        public void UpdateLevel()
        {
            CommonTools.UpdateText(_tmpText,
                $"lvl{_heroInstruments.GetLevel(InstrumentType.ToString()) + 1}");
        }
    }
}