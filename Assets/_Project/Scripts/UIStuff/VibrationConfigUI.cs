using _Project.Scripts.Architecture.Services.SaveLoadService;
using Assets._Project.Scripts.Managers;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff
{
    public class VibrationConfigUI : MonoBehaviour
    {
        [SerializeField] private Button _vibrationButton;
        [SerializeField] private Image _vibrationImage;

        [SerializeField] private Sprite _vibrationOnSprite;
        [SerializeField] private Sprite _vibrationOffSprite;

        [Inject] private ISaveLoadService _saveLoadService;
        [Inject] private VibrationService _vibrationService;

        public void Initialize()
        {
            UpdateMuteSprite();

            _vibrationButton.onClick.AddListener(ChangeVibrationMute);
        }

        private void ChangeVibrationMute()
        {
            CommonTools.ChangeIntValue(ref _saveLoadService.GameConfig.VibrationOn);
            _saveLoadService.SaveAllData();
            _vibrationService.SetActive(CommonTools.IntToBool(_saveLoadService.GameConfig.VibrationOn));
            
            UpdateMuteSprite();
        }

        private void UpdateMuteSprite()
        {
            _vibrationImage.sprite = CommonTools.IntToBool(_saveLoadService.GameConfig.VibrationOn)
                ? _vibrationOnSprite
                : _vibrationOffSprite;
        }
    }
}