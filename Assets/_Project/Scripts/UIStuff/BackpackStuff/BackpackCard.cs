using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff.BackpackStuff
{
    public class BackpackCard : MonoBehaviour
    {
        [Header("Properties")] [SerializeField]
        private Sprite _searchSprite;

        [Header("Sub Behaviours")] [SerializeField]
        private Image _cardImage;

        [SerializeField] private Image _resourceImage;
        [SerializeField] private TMP_Text _resourceAmountTMP;

        private Sprite _defaultSprite;
        private Button _button;
        private BackpackWindow _backpackWindow;

        private GameResourceType _resourceType;

        public GameResourceType ResourceType => _resourceType;

        public void Initialize(BackpackWindow backpackWindow)
        {
            _backpackWindow = backpackWindow;

            _defaultSprite = _cardImage.sprite;
            _button = GetComponent<Button>();
            
            _button.onClick.AddListener(OnButtonClicked);
        }


        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            _backpackWindow.ChangeSearchCard(this);
        }

        public void UpdateValues(PropsValue resourceValue)
        {
            _resourceType = resourceValue.GameResourceType;
            var sprite = _backpackWindow.ResourcesService.GetSprite(_resourceType);
            _resourceImage.sprite = sprite;

            CommonTools.UpdateText(_resourceAmountTMP, resourceValue.Amount);
        }

        public void BeginSearch()
        {
            _backpackWindow.ResourcesArrow.BeginSearch(_resourceType);
            SetSearchSprite();
        }

        public void SetSearchSprite()
        {
            _cardImage.sprite = _searchSprite;
        }
        
        public void SetDefaultSprite()
        {
            _cardImage.sprite = _defaultSprite;
        }

        public void FinishSearch()
        {
            SetDefaultSprite();
        }
    }
}