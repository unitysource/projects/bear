using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Architecture.Services.Save;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.UIStuff.BackpackStuff
{
    public class BackpackWindow : WindowBase
    {
        [Header("Properties")] [SerializeField]
        private BackpackCard _backpackCardPrefab;

        [Header("Sub Behaviours")] [SerializeField]
        private Transform _resourcesGroup;

        [SerializeField] private Button _closeButton;
        [SerializeField] private Button _closeBGButton;

        private IPoolService _poolService;
        private HeroInventory HeroInventory { get; set; }
        public ResourcesArrow ResourcesArrow { get; private set; }
        public GameResourcesService ResourcesService { get; private set; }

        public BackpackCard SearchedCard { get; set; }


        [Inject]
        private void Construct(DataService dataService, IPoolService poolService, HeroInventory heroInventory,
            GameResourcesService resourcesService)
        {
            ResourcesService = resourcesService;
            HeroInventory = heroInventory;
            _poolService = poolService;

            ResourcesArrow = HeroInventory.GetComponent<ResourcesArrow>();
        }

        public override void Initialize()
        {
            base.Initialize();

            if (GameObject.Find("BackpackCardPool").transform.childCount <= 0)
                FillPool();

            UpdateValues();

            _closeButton.onClick.AddListener(OnClose);
            _closeBGButton.onClick.AddListener(OnClose);
        }

        private void OnDestroy()
        {
            _closeButton.onClick.RemoveListener(OnClose);
            _closeBGButton.onClick.RemoveListener(OnClose);
        }

        private void OnClose()
        {
            _uiService.UnloadWindow(GetWindowType());
            _uiService.OpenWindow(WindowType.HUD, false, true);
        }

        private void FillPool()
        {
            _poolService.FillPool(new PoolInfo(_backpackCardPrefab.name, 20,
                _backpackCardPrefab.gameObject, "BackpackCardPool"));
        }

        private void UpdateValues()
        {
            foreach (var resourceValue in HeroInventory.PropsesValues)
            {
                if (resourceValue.Amount <= 0) continue;

                BackpackCard card = InitCard();
                card.Initialize(this);
                card.UpdateValues(resourceValue);

                string cardType = SaveManager.GetString(SaveKey.SearchedCard);
                if (resourceValue.GameResourceType.ToString() == cardType)
                {
                    SearchedCard = card;
                    SearchedCard.SetSearchSprite();
                }
            }
        }

        private BackpackCard InitCard()
        {
            var backpackCard = _poolService.GetPoolObject(_backpackCardPrefab.name);
            backpackCard.SetActive(true);
            backpackCard.transform.SetParent(_resourcesGroup, false);
            return backpackCard.GetComponent<BackpackCard>();
        }

        public void ChangeSearchCard(BackpackCard backpackCard)
        {
            if (SearchedCard == null)
            {
                if (HaveProps(backpackCard))
                {
                    SearchedCard = backpackCard;
                    ResourcesArrow.StopSearch();
                    SearchedCard.BeginSearch();
                }
            }
            else if (SearchedCard == backpackCard)
            {
                SearchedCard.FinishSearch();
                ResourcesArrow.StopSearch();
                SearchedCard = null;
            }
            else
            {
                SearchedCard.FinishSearch();
                if (HaveProps(backpackCard))
                {
                    SearchedCard = backpackCard;
                    ResourcesArrow.StopSearch();
                    SearchedCard.BeginSearch();
                }
                else
                {
                    SearchedCard = null;
                    ResourcesArrow.StopSearch();
                }
            }
        }
        
        protected override void OnClosedState()
        {
            if (SearchedCard != null)
                SaveManager.SetString(SaveKey.SearchedCard, SearchedCard.ResourceType.ToString());
        }

        public override WindowType GetWindowType() => WindowType.Backpack;

        private bool HaveProps(BackpackCard backpackCard) => ResourcesService.HaveProps(backpackCard.ResourceType);
    }
}