using _Project.Scripts.Architecture.Services.Pool;
using UnityEngine;
using Zenject;

namespace _Project.Scripts
{
    public class PoolReturn : MonoBehaviour
    {
        private IPoolService _poolService;
        private string _nameType;

        public void Initialize(IPoolService poolService, string nameType)
        {
            _nameType = nameType;
            _poolService = poolService;
        }

        public void ReturnToPool()
        {
            _poolService.ReturnToPool(gameObject, _nameType);
        }
    }
}