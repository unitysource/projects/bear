using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.GameTools
{
    public abstract class ToolBase : MonoBehaviour, IHeroInteractable, IInteractable, ICameraConnector
    {
        [SerializeField] private Transform _resourcesPoint;

        protected GameResourcesService _resourcesService;
        protected HeroInventory _heroInventory;
        private CameraManager _cameraManager;
        protected IAdsService _adsService;
        protected IUIService _uiService;
        protected AudioService _audioService;

        protected WorldSettings WorldSettings { get; private set; }
        public UISettings UISettings { get; private set; }

        public HeroInventory HeroInventory => _heroInventory;

        public GameResourcesService ResourcesService => _resourcesService;

        [Inject]
        private void Construct(DataService dataService, HeroInventory heroInventory,
            GameResourcesService resourcesService, IAdsService adsService, IUIService uiService, AudioService audioService)
        {
            _audioService = audioService;
            _uiService = uiService;
            _adsService = adsService;
            _resourcesService = resourcesService;
            _heroInventory = heroInventory;

            WorldSettings = dataService.WorldSettings;
            UISettings = dataService.UISettings;
            
            _resourcesPoint.localEulerAngles = Vector3.up * transform.localEulerAngles.y;
            
            FindCameraController();
        }

        public virtual void HeroTriggerEnter()
        {
            // Connect(_resourcesPoint);
        }

        public virtual void HeroTriggerExit()
        {
            // UnConnect();
        }

        protected void AnimateResources(int spendAmount, GameResourceType resourceType)
        {
            if (spendAmount > WorldSettings.MAXFlyResourcesAmount)
                spendAmount = WorldSettings.MAXFlyResourcesAmount;

            _resourcesService.AnimateFlyLoop(resourceType, _heroInventory.ResourcesPoint,
                _resourcesPoint, 0.9f, new MinMaxFloat(1.4f, 1.6f), new MinMaxFloat(1.2f, 1.3f), spendAmount);
        }

        public void Interact(HeroInteractable hero, float damage, out Health health)
        {
            health = null;
        }

        public bool CanInteract { get; set; } = false;

        public InstrumentType GetInteractionInstrumentType() => InstrumentType.Axe;

        public abstract GameResourceType[] GetSeeResourcesTypes();
        public abstract GameResourceType GetInteractionResourceType();

        public virtual void Launch()
        {
        }

        public void FindCameraController() => _cameraManager = FindObjectOfType<CameraManager>();

        public void Connect(Transform target) => _cameraManager.ZoomIn(target);

        public void UnConnect() => _cameraManager.ZoomOut();
    }
}