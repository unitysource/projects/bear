﻿namespace _Project.Scripts.GameTools
{
    public interface IHeroInteractable
    {
        void HeroTriggerEnter();
        void HeroTriggerExit();
    }
}