using _Project.Scripts.Mechanics;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    [CreateAssetMenu(fileName = "PortalSettings", menuName = "Settings/PortalSettings")]
    public class PortalSettings : ScriptableObject
    {
        [SerializeField] private int _level;
        [SerializeField] private Sprite _sprite;
        [SerializeField] private Color _color;
        [SerializeField] private PropsValue[] _resourceAmountProperties;

        public Sprite Sprite => _sprite;
        public PropsValue[] ResourceAmountProperties => _resourceAmountProperties;

        public int Level => _level;

        public Color Color => _color;
    }
}