﻿using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class RecycleToolUI : BaseToolUI
    {
        [SerializeField] private RecycleToolButton _someResourcesToolButton;
        [SerializeField] private RecycleToolButton _allResourcesToolButton;
        [SerializeField] private ToolProgressUI _toolProgress;
        [SerializeField] private ResourceUI _resourceUI;

        public ResourceUI ResourceUI => _resourceUI;
        public ToolProgressUI ToolProgress => _toolProgress;
        
        public override void Initialize(RecycleToolBase recycleTool)
        {
            _recycleTool = recycleTool;

            _someResourcesToolButton.Initialize(recycleTool);
            _allResourcesToolButton.Initialize(recycleTool);
            ToolProgress.Initialize();
            MissingResourcesUI.Initialize();
        }

        public override void Activate()
        {
            _someResourcesToolButton.Activate(_recycleTool.UISettings.OpenDuration);

            if (_recycleTool.InventoryAmountAboveTrait())
                _allResourcesToolButton.Activate(_recycleTool.UISettings.OpenDuration);
        }

        public override void Deactivate()
        {
            _someResourcesToolButton.Deactivate(_recycleTool.UISettings.CloseDuration);
            _allResourcesToolButton.Deactivate(_recycleTool.UISettings.CloseDuration);
        }

        public override void UpdateUI(int spendResourceAmount)
        {
            int resultMultiplier = _recycleTool.ResultMultiplier;
            int remainder = spendResourceAmount % resultMultiplier;

            _someResourcesToolButton.SetColor(spendResourceAmount >= resultMultiplier);

            if (spendResourceAmount < resultMultiplier * 100)
            {
                _someResourcesToolButton.UpdateValues(resultMultiplier, 1);
            }
            else
            {
                int reducedAmount = spendResourceAmount / 10;
                int spendAmount = reducedAmount - reducedAmount % resultMultiplier;
                _someResourcesToolButton.UpdateValues(spendAmount,
                    spendAmount / resultMultiplier);
            }

            if (_recycleTool.InventoryAmountAboveTrait())
            {
                _allResourcesToolButton.UpdateValues(spendResourceAmount - remainder,
                    spendResourceAmount / resultMultiplier);
            }
            else
            {
                _allResourcesToolButton.ChangeActive(false);
            }
        }
    }
}