﻿using _Project.Scripts.SettingsStuff;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class PickUpToolUI : MonoBehaviour
    {
        [Header("Sub Behaviours")] [SerializeField]
        private PickUpToolButton _pickUpToolButton;

        [SerializeField] private ToolProgressUI _toolProgress;
        [SerializeField] private ResourceUI _instrumentSplashUI;
        [SerializeField] private MissingResourcesUI _missingResourcesUI;

        public ToolProgressUI ToolProgress => _toolProgress;

        public ResourceUI InstrumentSplashUI => _instrumentSplashUI;

        // private ToolBase _pickUpTool;

        public void Initialize(ToolBase pickUpTool)
        {
            // _pickUpTool = pickUpTool;
            _pickUpToolButton.Initialize(pickUpTool);
            _toolProgress.Initialize();
            _missingResourcesUI.Initialize();
        }

        public void Activate(float openDuration)
        {
            _toolProgress.SetActive(false);
            _pickUpToolButton.Activate(openDuration);
        }

        public void Deactivate(float closeDuration)
        {
            _toolProgress.SetActive(true);
            _pickUpToolButton.Deactivate(closeDuration);
        }

        public void UpdateValues(InstrumentSettings instrumentSettings)
        {
            _pickUpToolButton.UpdateValues(instrumentSettings);
            _toolProgress.SetSprite(instrumentSettings.Sprite);
        }

        public void UpdateValues(PortalSettings portalSettings)
        {
            _pickUpToolButton.UpdateValues(portalSettings);
            _pickUpToolButton.SetSpriteColor(portalSettings);
            
            _toolProgress.SetSprite(portalSettings.Sprite);
            _toolProgress.SetSpriteColor(portalSettings);
        }

        public void PreLaunch(Sprite sprite, float closeDuration)
        {
            _instrumentSplashUI.SetSprite(sprite);

            _toolProgress.SetActive(true);
            _toolProgress.ChangeProgressActive(true);
            _toolProgress.SetSprite(sprite);

            _pickUpToolButton.Deactivate(closeDuration);
        }

        public void Launch(float workSpeed)
        {
            _toolProgress.SoftFill(workSpeed);
        }

        public void FinishLaunch(bool isHeroInside, float openDuration, bool isMaxLevel)
        {
            _toolProgress.ChangeProgressActive(false);
            _instrumentSplashUI.Animate(null, 35f);

            if (isHeroInside && !isMaxLevel)
                _pickUpToolButton.Activate(openDuration);
        }

        public void CancelLaunch()
        {
            _missingResourcesUI.Animate();
        }
    }
}