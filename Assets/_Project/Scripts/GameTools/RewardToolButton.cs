using _Project.Scripts.GameTools;
using _Project.Scripts.Services.InputStuff;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Zenject;

public class RewardToolButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _resultTMP;

    private InputBehaviour _inputBehaviour;
    private char _resultSpriteIndex;
    private RecycleToolBase _recycleTool;

    private int _spendAmount, _resultAmount;
    private readonly bool _canClick = true;

    [Inject]
    private void Construct(InputBehaviour inputBehaviour)
    {
        _inputBehaviour = inputBehaviour;
    }

    public void Initialize(RecycleToolBase recycleTool)
    {
        _recycleTool = recycleTool;
        _inputBehaviour.ObjectMouseClick += OnToolButtonClicked;

        if (_resultTMP != null)
        {
            string resultTMPText = _resultTMP.text;
            _resultSpriteIndex = resultTMPText[resultTMPText.Length - 2];
        }

        transform.localScale = Vector3.zero;
        ChangeActive(false);
    }

    private void OnDestroy()
    {
        _inputBehaviour.ObjectMouseClick -= OnToolButtonClicked;
    }

    private void OnToolButtonClicked(GameObject clickedObj)
    {
        if (clickedObj.GetHashCode() != gameObject.GetHashCode() ||
            CommonTools.IsPointerOverUIObjectIgnoreJoystick() || !_canClick) return;

        _recycleTool.Launch(_spendAmount, _resultAmount, _recycleTool.ResultMultiplier);
    }

    public void UpdateValues(int resultAmount)
    {
        _resultAmount = resultAmount;

        if (_resultTMP != null)
            CommonTools.UpdateText(_resultTMP, $"{resultAmount} <sprite={_resultSpriteIndex}>");
    }

    public void Activate(float activateDuration)
    {
        ChangeActive(true);
        transform.DOScale(1f, activateDuration).From(0f).SetEase(Ease.OutBack).Play();
    }

    public void Deactivate(float deactivateDuration)
    {
        transform.DOScale(0f, deactivateDuration).From(1f).SetEase(Ease.InBack).Play().onComplete +=
            () => ChangeActive(false);
    }

    private void ChangeActive(bool active) => gameObject.SetActive(active);
}