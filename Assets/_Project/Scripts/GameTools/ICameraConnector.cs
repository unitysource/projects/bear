using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public interface ICameraConnector
    {
        public void FindCameraController();
        public void Connect(Transform target);
        public void UnConnect();
    }
}