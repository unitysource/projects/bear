using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class RewardTool : RecycleToolBase
    {
        protected override void Initialize()
        {
            base.Initialize();
        }

        public override void Launch(int spendAmount, int resultAmount, int minValue)
        {
            
        }

        public override void HeroTriggerEnter()
        {
            _recycleToolUI.Activate();
        }

        public override void HeroTriggerExit()
        {
            base.HeroTriggerExit();
        }
    }
}