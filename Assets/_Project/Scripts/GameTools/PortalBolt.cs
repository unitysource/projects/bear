using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class PortalBolt : MonoBehaviour
    {
        [SerializeField] private Transform _model;

        private float _spinOffset;
        private float _spinDuration;
        private float _startYPos;
        private float _defaultScale;
        private ParticleSystem _particles;
        private const float _activateDuration = 0.5f;

        public void Initialize(float offset, float duration, ParticleSystem particles)
        {
            _particles = particles;
            _spinOffset = offset;
            _spinDuration = duration;
            _startYPos = _model.localPosition.y;
        }

        public void BeginLevitate()
        {
            _model.DOKill();

            const float moveDuration = 2f;
            const float rotDuration = 2f;
            const float offset = 0.12f;
            _model.DOLocalMoveY(_startYPos + offset, moveDuration).SetLoops(-1, LoopType.Yoyo).Play()
                .SetEase(Ease.InOutQuart);
            _model.DOLocalRotate(_model.localEulerAngles + Vector3.up * 360f, rotDuration,
                RotateMode.FastBeyond360).Play().SetLoops(-1, LoopType.Yoyo);
        }

        public void Spin()
        {
            _model.DOKill();
            _model.DOLocalMoveY(_startYPos - _spinOffset, _spinDuration).From(_startYPos).Play();
            _model.DOLocalRotate(_model.localEulerAngles + Vector3.up * 360f, _spinDuration,
                RotateMode.FastBeyond360).Play();
        }

        public void Activate(bool isSmooth)
        {
            // if (isSmooth)
            // {
            //     _model.DOKill();
            //     _model.DOScale(1f, _activateDuration).From(0f).Play();
            // }
            // else
            // {
            //     _model.localScale = Vector3.one;
            // }
            var particle = Instantiate(_particles, _model.position, Quaternion.identity);
            particle.transform.SetParent(transform);
            particle.transform.localScale = Vector3.one * 0.4f;
            gameObject.SetActive(true);
            _model.localScale = Vector3.one;
        }

        public void DeActivate(bool isSmooth)
        {
            if (isSmooth)
            {
                _model.DOKill();
                _model.DOScale(0f, _activateDuration).Play().SetEase(Ease.InQuad).onComplete += () =>
                {
                    gameObject.SetActive(false);
                };
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }
}