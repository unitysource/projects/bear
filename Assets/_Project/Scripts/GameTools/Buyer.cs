using System.Collections;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class Buyer : RecycleToolBase
    {
        [SerializeField] private float _inviteDelay;
        private static readonly int InviteTrigger = Animator.StringToHash("Invite");

        protected override void Initialize()
        {
            base.Initialize();
            StartCoroutine(nameof(InviteRoutine));
        }

        private IEnumerator InviteRoutine()
        {
            while (gameObject.activeSelf)
            {
                _animator.SetTrigger(InviteTrigger);
                yield return new WaitForSeconds(_inviteDelay);
            }
        }

        public override void Launch(int spendAmount, int resultAmount, int minValue)
        {
            base.Launch(spendAmount, resultAmount, minValue);
            _heroInventory.TakeProps(_spendResourceType, spendAmount);
            _heroInventory.AddProps(_resultResourceType, resultAmount);
            _recycleToolUI.UpdateUI(_heroInventory.GetResourcesAmountByType(_spendResourceType));
        }
    }
}