﻿using System;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Services.InputStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace _Project.Scripts.GameTools
{
    public class PickUpToolButton : MonoBehaviour
    {
        [SerializeField] private Image _instrumentImage;
        [SerializeField] private Transform _requiredResourcesGroupParent;
        [SerializeField] private TMP_Text[] _pickUpTMPs;

        [SerializeField] private TextMeshProUGUI _tmpPrefab;

        private ToolBase _pickUpTool;
        private InputBehaviour _inputBehaviour;

        private string[] _pickUpsProperties;

        private Color _enoughResourceColor, _deficiencyResourceColor;
        private IPoolService _poolService;

        [Inject]
        private void Construct(InputBehaviour inputBehaviour, IPoolService poolService)
        {
            _inputBehaviour = inputBehaviour;
            _poolService = poolService;
        }

        public void Initialize(ToolBase toolBase)
        {
            _pickUpTool = toolBase;
            _inputBehaviour.ObjectMouseClick += OnToolButtonClicked;

            FillPool();
            InitTMPs();

            _enoughResourceColor = toolBase.UISettings.EnoughResourceColor;
            _deficiencyResourceColor = toolBase.UISettings.DeficiencyResourceColor;

            transform.localScale = Vector3.zero;
            ChangeActive(false);
        }

        private void InitTMPs()
        {
            _pickUpsProperties = new string[_pickUpTMPs.Length];
            for (var i = 0; i < _pickUpTMPs.Length; i++)
            {
                string text = _pickUpTMPs[i].text;
                _pickUpsProperties[i] = text.Remove(text.Length - 1);
            }
        }

        private void FillPool()
        {
            _poolService.FillPool(new PoolInfo(_tmpPrefab.name, 15,
                _tmpPrefab.gameObject, "ToolTMPPool"));
        }

        private void OnDestroy()
        {
            _inputBehaviour.ObjectMouseClick -= OnToolButtonClicked;
        }

        private void OnToolButtonClicked(GameObject clickedObj)
        {
            if (clickedObj.GetHashCode() != gameObject.GetHashCode() ||
                CommonTools.IsPointerOverUIObjectIgnoreJoystick()) return;

            _pickUpTool.Launch();
        }

        public void Activate(float activateDuration)
        {
            ChangeActive(true);
            transform.DOScale(1f, activateDuration).From(0f).SetEase(Ease.OutBack).Play();
        }

        public void Deactivate(float deactivateDuration)
        {
            transform.DOScale(0f, deactivateDuration).From(1f).SetEase(Ease.InBack).Play().onComplete +=
                () => ChangeActive(false);
        }

        public void UpdateValues(InstrumentSettings instrumentSettings)
        {
            _requiredResourcesGroupParent.gameObject.ActionAllChildren(child => _poolService.ReturnToPool(child.gameObject, _tmpPrefab.name));
            _instrumentImage.sprite = instrumentSettings.Sprite;

            for (var i = 0; i < instrumentSettings.IncreaseAmounts.Length; i++)
            {
                CommonTools.UpdateText(_pickUpTMPs[i],
                    $"{_pickUpsProperties[i]}{instrumentSettings.IncreaseAmounts[i].ToString()}");
            }

            foreach (PropsValue resourceAmountProperty in instrumentSettings.ResourceAmountProperties)
            {
                var requiredResourceType = resourceAmountProperty.GameResourceType;
                int requiredAmount = resourceAmountProperty.Amount;
                TextMeshProUGUI tmpElement = _poolService.GetPoolObject(_tmpPrefab.name)
                    .GetComponent<TextMeshProUGUI>();
                tmpElement.gameObject.SetActive(true);
                tmpElement.transform.SetParent(_requiredResourcesGroupParent, false);
                int tmpSpriteIndex = _pickUpTool.ResourcesService.GetTMPSpriteIndexByResourceType(requiredResourceType);
                CommonTools.UpdateText(tmpElement, $"{requiredAmount.ToString()} <sprite={tmpSpriteIndex.ToString()}>");
                SetTMPColor(tmpElement,
                    _pickUpTool.HeroInventory.IsEnoughPropsAmount(requiredResourceType, requiredAmount));
            }
        }
        
        public void UpdateValues(PortalSettings portalSettings)
        {
            _requiredResourcesGroupParent.gameObject.ActionAllChildren(child => _poolService.ReturnToPool(child.gameObject, _tmpPrefab.name));
            _instrumentImage.sprite = portalSettings.Sprite;

            foreach (PropsValue resourceAmountProperty in portalSettings.ResourceAmountProperties)
            {
                var requiredResourceType = resourceAmountProperty.GameResourceType;
                int requiredAmount = resourceAmountProperty.Amount;
                TextMeshProUGUI tmpElement = _poolService.GetPoolObject(_tmpPrefab.name)
                    .GetComponent<TextMeshProUGUI>();
                tmpElement.gameObject.SetActive(true);
                tmpElement.transform.SetParent(_requiredResourcesGroupParent, false);
                int tmpSpriteIndex = _pickUpTool.ResourcesService.GetTMPSpriteIndexByResourceType(requiredResourceType);
                CommonTools.UpdateText(tmpElement, $"{requiredAmount.ToString()} <sprite={tmpSpriteIndex.ToString()}>");
                SetTMPColor(tmpElement,
                    _pickUpTool.HeroInventory.IsEnoughPropsAmount(requiredResourceType, requiredAmount));
            }
        }

        public void ChangeActive(bool active) => gameObject.SetActive(active);

        private void SetTMPColor(TextMeshProUGUI resourceTMP, bool enoughResource) =>
            resourceTMP.color = enoughResource ? _enoughResourceColor : _deficiencyResourceColor;
        
        public void SetSpriteColor(PortalSettings settings)
        {
            _instrumentImage.color = settings.Color;
        }
    }
}