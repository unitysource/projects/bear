using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Tiles;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class RecycleToolBase : ToolBase
    {
        [SerializeField] protected GameResourceType _spendResourceType;
        [SerializeField] protected GameResourceType _resultResourceType;
        [SerializeField] protected BaseToolUI _recycleToolUI;
        [SerializeField] protected ObserverWithHero _observer;
        [SerializeField] protected Animator _animator;
        [SerializeField] protected int _resultMultiplier;

        protected int _resourcesCounter;
        public int ResultMultiplier => _resultMultiplier;

        private void Awake() => Initialize();

        protected virtual void Initialize()
        {
            _recycleToolUI.Initialize(this);
            _observer.Setup(this);
        }

        public override void HeroTriggerEnter()
        {
            base.HeroTriggerEnter();

            _recycleToolUI.UpdateUI(_heroInventory.GetResourcesAmountByType(_spendResourceType));
            _recycleToolUI.Activate();
        }

        public override void HeroTriggerExit()
        {
            base.HeroTriggerExit();

            _recycleToolUI.Deactivate();
        }

        public bool InventoryAmountAboveTrait() =>
            _heroInventory.GetResourcesAmountByType(_spendResourceType) >= _resultMultiplier * 2;

        public virtual void Launch(int spendAmount, int resultAmount, int minValue)
        {
            bool enoughResources = _heroInventory.GetResourcesAmountByType(_spendResourceType) >= minValue;
            if (!enoughResources)
            {
                _recycleToolUI.MissingResourcesUI.Animate();
                return;
            }
            
            _audioService.PlaySound(AudioClipId.user_buy);
            
            AnimateResources(spendAmount, _spendResourceType);
        }

        public override GameResourceType[] GetSeeResourcesTypes() => new[] { _spendResourceType, _resultResourceType };

        public override GameResourceType GetInteractionResourceType() => GameResourceType.None;
    }
}