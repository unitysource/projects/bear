using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class RewardToolUI : BaseToolUI
    {
        [SerializeField] private RewardToolButton _resourcesToolButton;
        [SerializeField] private ToolProgressUI _toolProgress;
        [SerializeField] private ResourceUI _resourceUI;

        public ResourceUI ResourceUI => _resourceUI;
        public ToolProgressUI ToolProgress => _toolProgress;
        
        public override void Initialize(RecycleToolBase recycleTool)
        {
            _recycleTool = recycleTool;

            _resourcesToolButton.Initialize(recycleTool);
            ToolProgress.Initialize();
            MissingResourcesUI.Initialize();
        }

        public override void Activate()
        {
            _resourcesToolButton.Activate(_recycleTool.UISettings.OpenDuration);
        }

        public override void Deactivate()
        {
            _resourcesToolButton.Deactivate(_recycleTool.UISettings.CloseDuration);
        }

        public override void UpdateUI(int spendResourceAmount)
        {
        }
    }
}