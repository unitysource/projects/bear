﻿using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.GameTools
{
    public class ToolProgressUI : MonoBehaviour
    {
        [SerializeField] private GameObject _progressObject;
        [SerializeField] private Image _progressImage;
        [SerializeField] private TMP_Text _resourcesAmountTMP;
        
        [SerializeField] private Image _image;

        private Vector3 _tmpScale;
        private CanvasGroup _canvasGroup;
        private const float FadeDuration = 0.5f;

        public void Initialize()
        {
            _tmpScale = _resourcesAmountTMP.transform.localScale;
            _canvasGroup = GetComponent<CanvasGroup>();

            ChangeProgressActive(false);
            SetFill(0f);
        }

        public void ChangeProgressActive(bool active)
        {
            if (_progressObject.activeSelf != active)
            {
                _progressObject.SetActive(active);
                _resourcesAmountTMP.gameObject.SetActive(active);
            }
        }

        public void DeActivate()
        {
            gameObject.SetActive(false);
        }


        public void SoftFill(float workDelay)
        {
            _progressImage.DOFillAmount(1f, workDelay).From(0f).SetEase(Ease.Linear).Play();
        }

        public void SetFill(float value)
        {
            _progressImage.fillAmount = value;
        }

        public void SetAmount(int amount)
        {
            const float duration = 0.1f;
            Transform tr = _resourcesAmountTMP.transform;
            tr.DOScale(_tmpScale * 1.1f, duration).From(_tmpScale).Play().onComplete += () =>
                tr.DOScale(_tmpScale, duration / 2).Play();
            
            CommonTools.UpdateText(_resourcesAmountTMP, $"{amount}");
        }

        public void SetActive(bool active)
        {
            if (active)
                _canvasGroup.DOFade(1f, FadeDuration).From(0f).Play();
            else
                _canvasGroup.DOFade(0f, FadeDuration).From(1f).Play();
        }
        
        public void SetSprite(Sprite sprite) => 
            _image.sprite = sprite;

        private void SetActiveImage(bool active) => 
            _image.gameObject.SetActive(active);
        
        public void SetSpriteColor(PortalSettings settings)
        {
            _image.color = settings.Color;
        }
    }
}