﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class MissingResourcesUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _missingResourcesTMP;

        private float _missingResourcesStartYOffset;

        private const float AnimateDuration = 1f;

        public void Initialize()
        {
            _missingResourcesStartYOffset = _missingResourcesTMP.transform.localPosition.y;
            _missingResourcesTMP.DOFade(0f, 0f).Play();
        }

        public void Animate()
        {
            _missingResourcesTMP.DOKill();
            _missingResourcesTMP.transform.DOKill();
            _missingResourcesTMP.DOFade(1f, AnimateDuration / 2).From(0f).Play();
            _missingResourcesTMP.transform.DOLocalMoveY(_missingResourcesStartYOffset + 15f, AnimateDuration / 2)
                .From(_missingResourcesStartYOffset).Play().onComplete += () =>
            {
                _missingResourcesTMP.transform
                    .DOLocalMoveY(_missingResourcesStartYOffset + 30f, AnimateDuration / 2).Play();
                _missingResourcesTMP.DOFade(0f, AnimateDuration / 2).Play();
            };
        }
    }
}