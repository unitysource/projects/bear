using System;
using System.Collections;
using System.Linq;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Architecture.Services.UIStuff;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities.Constants;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.GameTools
{
    public class Portal : ToolBase
    {
        [SerializeField] private string _portalPath;

        [SerializeField] private PickUpToolUI _pickUpToolUI;
        [SerializeField] private ObserverWithHero _observer;
        [SerializeField] private Animator _animator;
        [SerializeField] private float _workSpeed = 1f;
        [SerializeField] private int _maxUpgradeLevel;

        [Header("Bolts")] [SerializeField] private PortalBolt[] _portalBolts;
        [SerializeField] private float _spinDuration;
        [SerializeField] private float _boltOffset;
        [SerializeField] private float _pushObserverRadius;
        [SerializeField] private ParticleSystem _boltParticles;
        [SerializeField] private Transform _heroPoint;
        [SerializeField] private GameObject _stairs;

        private PortalSettings _portalSettings;
        private AssetService _assetService;
        private Joystick _joystick;
        private bool _isPushed;

        private static readonly int PushTrigger = Animator.StringToHash("Push");
        private static readonly int ToBrokenState = Animator.StringToHash("ToBrokenState");
        private static readonly int Idle = Animator.StringToHash("Idle");
        private Collider _collider;
        private AudioService _audioService;

        [Inject]
        private void Construct(AssetService assetService, Joystick joystick, AudioService audioService)
        {
            _audioService = audioService;
            _joystick = joystick;
            _assetService = assetService;
        }

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _collider = GetComponent<Collider>();

            _isPushed = false;
            _pickUpToolUI.Initialize(this);
            _observer.Setup(this);

            foreach (var bolt in _portalBolts)
            {
                bolt.Initialize(_boltOffset, _spinDuration, _boltParticles);
                bolt.DeActivate(false);
            }

            if (!IsMaxLevel)
            {
                _portalSettings = GetCurrentLevelSettings();
                _pickUpToolUI.UpdateValues(_portalSettings);

                // SetActiveStairs(false);

                _animator.SetInteger(ToBrokenState, _portalSettings.Level);

                for (int i = 0; i < _portalSettings.Level; i++)
                {
                    PortalBolt bolt = _portalBolts[i];
                    bolt.Activate(false);
                    bolt.BeginLevitate();
                }
            }
            else
            {
                _observer.Collider.radius = _pushObserverRadius;
                _pickUpToolUI.ToolProgress.DeActivate();

                // SetActiveStairs(true);

                _animator.SetTrigger(Idle);

                foreach (var bolt in _portalBolts)
                {
                    bolt.Activate(false);
                    bolt.BeginLevitate();
                }
            }
        }

        private PortalSettings GetCurrentLevelSettings()
        {
            var level = GetLevel(_portalPath);
            foreach (var portalSettings in _assetService.GetScriptableObjects<PortalSettings>(
                $"{AssetPath.Portals}/{_portalPath}"))
                if (portalSettings.Level == level)
                    return portalSettings;

            throw new Exception("Index upper then need");
        }

        private string GetPortalLevelPrefsName(string portalType) =>
            $"Portal_{portalType}_level";

        public override void HeroTriggerEnter()
        {
            base.HeroTriggerEnter();

            if (IsMaxLevel)
            {
                if (!_isPushed)
                {
                    PushHero();
                    _isPushed = true;
                }

                return;
            }

            _pickUpToolUI.Activate(UISettings.OpenDuration);
            _pickUpToolUI.UpdateValues(_portalSettings);
        }

        private void PushHero()
        {
            Debug.Log($"push");
            _joystick.CanUse = false;
            StartCoroutine(FinalWhole());
        }

        public override void HeroTriggerExit()
        {
            if (IsMaxLevel) return;

            base.HeroTriggerExit();

            _pickUpToolUI.Deactivate(UISettings.CloseDuration);
        }

        public override GameResourceType[] GetSeeResourcesTypes()
        {
            if (IsMaxLevel)
                return new[] { GameResourceType.None };

            int length = _portalSettings.ResourceAmountProperties.Length;
            var resources = new GameResourceType[length];
            for (var i = 0; i < length; i++)
            {
                var resourceAmountProperty = _portalSettings.ResourceAmountProperties[i];
                resources[i] = resourceAmountProperty.GameResourceType;
            }

            return resources;
        }

        public override void Launch()
        {
            if (IsMaxLevel) return;

            if (HeroHaveRequiredResources())
                StartCoroutine(nameof(LaunchRoutine));
            else
                _pickUpToolUI.CancelLaunch();
        }

        private IEnumerator LaunchRoutine()
        {
            IncreaseLevel();
            FillResources();
            _pickUpToolUI.PreLaunch(_portalSettings.Sprite, UISettings.CloseDuration);

            yield return new WaitForSeconds(1.2f);
            //Resources are filled in
            _pickUpToolUI.Launch(_workSpeed);

            yield return new WaitForSeconds(_workSpeed);
            FinishLaunch();
        }

        private void FillResources()
        {
            foreach (var resourceAmountProperty in _portalSettings.ResourceAmountProperties)
            {
                int spendAmount = resourceAmountProperty.Amount;
                var resourceType = resourceAmountProperty.GameResourceType;
                _heroInventory.TakeProps(resourceType, spendAmount);

                AnimateResources(spendAmount, resourceType);
            }
        }

        private void FinishLaunch()
        {
            _pickUpToolUI.FinishLaunch(_observer.IsHeroInside, UISettings.OpenDuration, IsMaxLevel);
            _pickUpToolUI.InstrumentSplashUI.SetSpriteColor(_portalSettings.Color);
            _pickUpToolUI.UpdateValues(_portalSettings);
            
            DOVirtual.DelayedCall(WorldSettings.AnimAdDelay, () => _adsService.ShowInterstitial());
        }

        private void IncreaseLevel()
        {
            if (IsMaxLevel) return;

            string prefsName = GetPortalLevelPrefsName(_portalPath);
            int currLevel = GetLevel(_portalPath);
            int newLevel = currLevel + 1;
            PlayerPrefs.SetInt(prefsName, newLevel);

            NextStateAnimator();
            var bolt = _portalBolts[currLevel];
            bolt.Activate(false);
            bolt.BeginLevitate();

            if (newLevel == _maxUpgradeLevel)
            {
                _observer.Collider.radius = _pushObserverRadius;
                // SetActiveStairs(true);
                _pickUpToolUI.ToolProgress.DeActivate();
                
                _audioService.PlaySound(AudioClipId.portal_final);

                AnalyticsManager.GameCompleted(CompleteType.CompletePortal);
            }
            else
            {
                _audioService.PlaySound(AudioClipId.portal_key);
            }

            if (!IsMaxLevel)
                _portalSettings = GetCurrentLevelSettings();
        }

        private IEnumerator FinalWhole()
        {
            _animator.SetTrigger(PushTrigger);

            yield return new WaitForSeconds(0.5f);

            foreach (var portalBolt in _portalBolts)
                portalBolt.Spin();

            const float duration = 1.2f;
            EntityMovement entityMovement = _heroInventory.GetComponent<EntityMovement>();
            entityMovement.MoveToPoint(_heroPoint.position, duration);
            yield return new WaitForSeconds(duration * 0.2f);
            entityMovement.Model.Deactivate(duration * 1.5f);
            yield return new WaitForSeconds(duration / 2);
            _heroInventory.GetComponent<EntityDeath>().Deactivate();

            foreach (var portalBolt in _portalBolts)
                portalBolt.DeActivate(true);

            yield return new WaitForSeconds(3.8f);
            _uiService.OpenWindow(WindowType.Final, true, true);
        }

        // private void SetActiveStairs(bool active)
        // {
        //     _stairs.gameObject.SetActive(active);
        //     _collider.enabled = !active;
        // }

        private void NextStateAnimator()
        {
            _animator.SetInteger(ToBrokenState, GetLevel(_portalPath));
        }

        private int GetLevel(string portalTypeName) =>
            PlayerPrefs.GetInt(GetPortalLevelPrefsName(portalTypeName));

        private bool HeroHaveRequiredResources() =>
            _portalSettings.ResourceAmountProperties.All(resourceAmountProperty =>
                _heroInventory.IsEnoughPropsAmount(resourceAmountProperty.GameResourceType,
                    resourceAmountProperty.Amount));

        public override GameResourceType GetInteractionResourceType() => GameResourceType.None;

        private bool IsMaxLevel => GetLevel(_portalPath) >= _maxUpgradeLevel;
    }
}