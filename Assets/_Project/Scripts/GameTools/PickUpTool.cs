﻿using System.Collections;
using System.Linq;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.GameTools
{
    public class PickUpTool : ToolBase
    {
        [Header("Resources Name of instrument!")] [SerializeField]
        private InstrumentType _instrumentType;

        [SerializeField] private PickUpToolUI _pickUpToolUI;
        [SerializeField] private ObserverWithHero _observer;
        [SerializeField] private Animator _animator;
        [SerializeField] private float _workSpeed = 1f;
        [SerializeField] private int _maxUpgradeLevel;

        private InstrumentSettings _instrumentSettings;
        private HeroInstruments _heroInstruments;
        private string _instrumentTypeString;

        private static readonly int WorkTrigger = Animator.StringToHash("Work");

        private AudioService _audioService;

        [Inject]
        private void Construct(AudioService audioService)
        {
            _audioService = audioService;
        }

        private void Start()
        {
            _heroInstruments = _heroInventory.GetComponent<HeroInstruments>();
            _instrumentTypeString = _instrumentType.ToString();

            _instrumentSettings = _heroInstruments.GetInstrumentSettings(_instrumentTypeString, true);

            _pickUpToolUI.Initialize(this);
            _observer.Setup(this);

            _pickUpToolUI.UpdateValues(_instrumentSettings);
        }

        public override void HeroTriggerEnter()
        {
            base.HeroTriggerEnter();

            if (IsMaxLevel)
            {
                Debug.Log($"Max Level");
                return;
            }

            _pickUpToolUI.Activate(UISettings.OpenDuration);
            _pickUpToolUI.UpdateValues(_instrumentSettings);
        }

        public override void HeroTriggerExit()
        {
            if (IsMaxLevel) return;

            base.HeroTriggerExit();

            _pickUpToolUI.Deactivate(UISettings.CloseDuration);
        }

        public override GameResourceType[] GetSeeResourcesTypes()
        {
            int length = _instrumentSettings.ResourceAmountProperties.Length;
            var resources = new GameResourceType[length];
            for (var i = 0; i < length; i++)
            {
                var resourceAmountProperty = _instrumentSettings.ResourceAmountProperties[i];
                resources[i] = resourceAmountProperty.GameResourceType;
            }

            return resources;
        }

        public override GameResourceType GetInteractionResourceType() => GameResourceType.None;

        public override void Launch()
        {
            if (HeroHaveRequiredResources())
            {
                if (_instrumentType == InstrumentType.Sword)
                {
                    _heroInstruments.GetComponent<Health>().UpgradeHealth(1);
                }

                StartCoroutine(nameof(LaunchRoutine));
            }
            else
            {
                _pickUpToolUI.CancelLaunch();
            }
        }

        private bool HeroHaveRequiredResources() =>
            _instrumentSettings.ResourceAmountProperties.All(resourceAmountProperty =>
                _heroInventory.IsEnoughPropsAmount(resourceAmountProperty.GameResourceType,
                    resourceAmountProperty.Amount));

        private IEnumerator LaunchRoutine()
        {
            _heroInstruments.ChangeInstrument(_instrumentSettings, _instrumentType);
            _heroInstruments.IncreaseInstrumentLevel(_instrumentTypeString);

            FillResources();
            _pickUpToolUI.PreLaunch(_instrumentSettings.Sprite, UISettings.CloseDuration);
            
            _audioService.PlaySound(AudioClipId.user_buy);

            yield return new WaitForSeconds(1.2f);
            
            _audioService.PlaySound(AudioClipId.tool_work);
            //Resources are filled in
            _pickUpToolUI.Launch(_workSpeed);
            _animator.SetTrigger(WorkTrigger);
            
            yield return new WaitForSeconds(_workSpeed);
            FinishLaunch();
        }

        private void FillResources()
        {
            foreach (var resourceAmountProperty in _instrumentSettings.ResourceAmountProperties)
            {
                int spendAmount = resourceAmountProperty.Amount;
                var resourceType = resourceAmountProperty.GameResourceType;
                _heroInventory.TakeProps(resourceType, spendAmount);

                AnimateResources(spendAmount, resourceType);
            }
        }

        private void FinishLaunch()
        {
            _audioService.StopSound(AudioClipId.tool_work);
            
            _instrumentSettings = _heroInstruments.GetInstrumentSettings(_instrumentTypeString, true);
            _heroInstruments.InstrumentIncreaseLevelAction(_instrumentType);

            _pickUpToolUI.FinishLaunch(_observer.IsHeroInside, UISettings.OpenDuration, IsMaxLevel);
            _pickUpToolUI.UpdateValues(_instrumentSettings);
            
            DOVirtual.DelayedCall(WorldSettings.AnimAdDelay, () => _adsService.ShowInterstitial());
        }

        private bool IsMaxLevel => _heroInstruments.GetLevel(_instrumentTypeString) >= _maxUpgradeLevel;
    }
}