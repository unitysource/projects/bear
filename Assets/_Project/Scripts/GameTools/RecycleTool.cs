using System.Collections;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.GameTools
{
    public class RecycleTool : RecycleToolBase
    {
        [SerializeField] private RecycleToolUI _recycleToolUIOverriden;
        [SerializeField] private float _workSpeed = 1f;
        [SerializeField] private AudioSource _audioSource;

        private static readonly int IsWorking = Animator.StringToHash("IsWorking");

        protected override void Initialize()
        {
            base.Initialize();
            CheckQuitedWork();

            _resourcesService.AddSpawnedPropsPosition(this, transform.position);
            _audioService.RegisterAudioSource(_audioSource);
        }

        private void OnApplicationQuit()
        {
            PlayerPrefs.SetInt(GetUnfinishedPropertyPrefsName, _resourcesCounter);
        }

        private void CheckQuitedWork()
        {
            int unfinishedAmount = PlayerPrefs.GetInt(GetUnfinishedPropertyPrefsName);
            if (unfinishedAmount > 0)
            {
                double quitedGameTime = CommonTools.GetQuitedGameSeconds();
                int madeAfterQuitedAmount = (int)(quitedGameTime / _workSpeed);
                madeAfterQuitedAmount = Mathf.Clamp(madeAfterQuitedAmount, 0, unfinishedAmount);
                int requiredToMakeAmount = unfinishedAmount - madeAfterQuitedAmount;

                if (requiredToMakeAmount > 0)
                {
                    _resourcesCounter = requiredToMakeAmount;
                    _recycleToolUIOverriden.ToolProgress.ChangeProgressActive(true);
                    StartCoroutine(nameof(FillRoutine));
                }

                _heroInventory.AddProps(_resultResourceType, madeAfterQuitedAmount);
                _recycleToolUIOverriden.ResourceUI.SetAmount(madeAfterQuitedAmount);
                _recycleToolUIOverriden.ResourceUI.Animate(null, 35f);
            }
        }

        public override void Launch(int spendAmount, int resultAmount, int minValue)
        {
            base.Launch(spendAmount, resultAmount, minValue);
            if (WorldSettings.IsSmoothly)
            {
                _heroInventory.TakeProps(_spendResourceType, spendAmount);
                _recycleToolUIOverriden.UpdateUI(_heroInventory.GetResourcesAmountByType(_spendResourceType));
                _resourcesCounter += resultAmount;

                _recycleToolUIOverriden.ToolProgress.ChangeProgressActive(true);

                StopCoroutine(nameof(FillRoutine));
                StartCoroutine(nameof(FillRoutine));
            }
            else
            {
                _heroInventory.TakeProps(_spendResourceType, spendAmount);
                _heroInventory.AddProps(_resultResourceType, resultAmount);
                _recycleToolUIOverriden.UpdateUI(_heroInventory.GetResourcesAmountByType(_spendResourceType));
            }
        }

        private IEnumerator FillRoutine()
        {
            while (_resourcesCounter > 0)
            {
                _animator.SetBool(IsWorking, true);

                _recycleToolUIOverriden.ToolProgress.SetAmount(_resourcesCounter);
                _recycleToolUIOverriden.ToolProgress.SoftFill(_workSpeed);
                
                _audioService.PlaySound(AudioClipId.tool_work, _audioSource);

                yield return new WaitForSeconds(_workSpeed);

                _audioService.StopSound(_audioSource);

                _animator.SetBool(IsWorking, false);

                _heroInventory.AddProps(_resultResourceType, 1);
                _recycleToolUIOverriden.ResourceUI.SetAmount(1);
                _recycleToolUIOverriden.ResourceUI.Animate(null, 35f);
                _resourcesCounter--;
                yield return null;
            }

            _recycleToolUIOverriden.ToolProgress.ChangeProgressActive(false);
        }

        private string GetUnfinishedPropertyPrefsName => $"Tool_{gameObject.GetHashCode()}_unfinishedAmount";
        
        public override GameResourceType GetInteractionResourceType() => _resultResourceType;
    }
}