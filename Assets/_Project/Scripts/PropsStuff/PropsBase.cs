using System;
using System.Collections;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.Audio;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.EntitiesStuff.HeroStuff;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using UnityEngine;
using Zenject;

namespace _Project.Scripts
{
    public abstract class PropsBase : MonoBehaviour, IInteractable
    {
        [Header("Setup Properties")] [SerializeField]
        protected InstrumentType _instrumentType;

        [SerializeField] protected GameResourceType _resourceType;
        [SerializeField] protected float _restoreDelay = 10f;

        [Header("Sub Behaviours")] [SerializeField]
        private Transform _resourcesStartPoint;

        [SerializeField] protected PropsModel _propsModel;
        [SerializeField, Min(1)] private int _level;

        protected const float _restoreScaleDelay = 0.2f;
        protected int _currentDecreaseStep;
        protected Vector3 _scale;
        private GameResourcesService _resourcesService;
        private WorldSettings _worldSettings;

        private int[][] _stepsAmounts;
        private int _stepPointer;
        private AudioService _audioService;

        [Inject]
        private void Construct(DataService dataService, GameResourcesService resourcesService, AudioService audioService)
        {
            _audioService = audioService;
            _resourcesService = resourcesService;
            var transform1 = transform;
            _scale = transform1.localScale;
            _worldSettings = dataService.WorldSettings;

            _resourcesService.AddSpawnedPropsPosition(this, transform1.position);
            CanInteract = true;

            InitSteps();
        }

        private void InitSteps()
        {
            _stepsAmounts = new int[8][];
            _stepsAmounts[0] = new[] {4};
            _stepsAmounts[1] = new[] {2, 2};
            _stepsAmounts[2] = new[] {2, 1, 1};
            _stepsAmounts[3] = new[] {1, 1, 1, 1};
            _stepsAmounts[4] = new[] {1, 1, 0, 1, 1};
            _stepsAmounts[5] = new[] {1, 0, 1, 0, 1, 1};
            _stepsAmounts[6] = new[] {1, 0, 1, 0, 1, 0, 1};
            _stepsAmounts[7] = new[] {0, 1, 0, 1, 0, 1, 0, 1};
        }

        public virtual void Interact(HeroInteractable hero, float damage, out Health health)
        {
            health = null;
            if (!CanInteract) return;

            switch (_resourceType)
            {
                case GameResourceType.Wood:
                    _audioService.PlaySound(AudioClipId.tree_hit);
                    break;
                case GameResourceType.Stone:
                    _audioService.PlaySound(AudioClipId.rock_hit);
                    break;
                case GameResourceType.Gold:
                    _audioService.PlaySound(AudioClipId.gold_hit);
                    break;
                case GameResourceType.K_131:
                    _audioService.PlaySound(AudioClipId.crystal_hit);
                    break;
            }
            
            int actionsAmount = 4 + (_level - (int) damage);
            int stepsAmount = _stepsAmounts[actionsAmount - 1][_stepPointer++];

            AnimateResources(stepsAmount, _resourceType, hero);
            DecreasePart(stepsAmount);
        }

        private void AnimateResources(int spendAmount, GameResourceType resourceType, HeroInteractable hero)
        {
            if (spendAmount > _worldSettings.MAXFlyResourcesAmount)
                spendAmount = _worldSettings.MAXFlyResourcesAmount;

            _resourcesService.AnimateFlyLoop(resourceType, _resourcesStartPoint,
                hero.transform, 1f, new MinMaxFloat(1.4f, 1.6f),
                new MinMaxFloat(1.2f, 1.5f), spendAmount);
        }

        protected IEnumerator RestoreRoutine()
        {
            yield return new WaitForSeconds(_restoreDelay);

            Restore();
            CanInteract = true;
            _currentDecreaseStep = 0;
            
            _stepPointer = 0;

            _propsModel.CollidersActivation(true);
        }

        protected abstract void Restore();
        protected abstract void DecreasePart(int stepsAmount);
        public bool CanInteract { get; set; }
        public InstrumentType GetInteractionInstrumentType() => _instrumentType;
        public GameResourceType[] GetSeeResourcesTypes() => new[] {_resourceType};
        public GameResourceType GetInteractionResourceType() => _resourceType;
        protected abstract bool IsDead();
    }
}