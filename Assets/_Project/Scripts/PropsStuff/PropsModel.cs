using System;
using UnityEngine;

namespace _Project.Scripts
{
    public class PropsModel : MonoBehaviour
    {
        private Collider[] _colliders;
        
        private void Awake()
        {
            _colliders = GetComponents<Collider>();
        }

        public void CollidersActivation(bool activate)
        {
            foreach (Collider col in _colliders)
            {
                col.enabled = activate;
            }
        }
    }
}