using System;
using _Project.Scripts.ResourcesStuff;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts
{
    public class BlendShapesProps : PropsBase
    {
        [SerializeField] private SkinnedMeshRenderer _modelRenderer;
        [SerializeField] private MinMaxFloat _minMaxValue;
        [SerializeField] private ShapeStates[] _shapeProperties;
        private float _duration = 0.25f;

        private void SetValue(ShapeState shapeState) =>
            SetValue(shapeState.ShapeIndex, shapeState.Value);

        private void SetValue(int index, float value)
        {
            DOTween.To(() => _modelRenderer.GetBlendShapeWeight(index),
                x => _modelRenderer.SetBlendShapeWeight(index, x),
                value * _minMaxValue.MaxValue, _duration).Play();
        }

        protected override void DecreasePart(int stepsAmount)
        {
            StopCoroutine(nameof(RestoreRoutine));

            for (int i = _currentDecreaseStep; i < _currentDecreaseStep + stepsAmount; i++)
            {
                int partsAmount = _shapeProperties.Length - 1;

                foreach (var shape in _shapeProperties[i + 1].Shapes)
                    SetValue(shape);

                if (i >= partsAmount - 1)
                {
                    CanInteract = false;
                    _propsModel.CollidersActivation(false);
                    break;
                }
            }

            _currentDecreaseStep += stepsAmount;
            StartCoroutine(nameof(RestoreRoutine));
        }

        protected override void Restore()
        {
            foreach (var shape in _shapeProperties[0].Shapes)
                SetValue(shape);
        }

        protected override bool IsDead() => _currentDecreaseStep >= _shapeProperties.Length;

        #region Sub Classes

        [Serializable]
        internal class ShapeStates
        {
            [SerializeField] private ShapeState[] _shapeStates;

            public ShapeState[] Shapes => _shapeStates;
        }

        [Serializable]
        internal class ShapeState
        {
            [SerializeField] private int _shapeIndex;
            [SerializeField, Range(0f, 1f)] private float _value;

            public float Value => _value;

            public int ShapeIndex => _shapeIndex;
        }

        #endregion
    }
}