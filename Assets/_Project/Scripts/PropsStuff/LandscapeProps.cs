﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts
{
    [SelectionBase]
    public class LandscapeProps : PropsBase
    {
        [SerializeField] private LandscapePart[] _landscapeParts = new LandscapePart[4];

        protected override void DecreasePart(int stepsAmount)
        {
            StopCoroutine(nameof(RestoreRoutine));

            for (int i = _currentDecreaseStep; i < _currentDecreaseStep + stepsAmount; i++)
            {
                int landscapePartsAmount = _landscapeParts.Length;
                _landscapeParts[i].StepParts.ForEach(part => part.SetActive(false));

                if (i >= landscapePartsAmount - 1)
                {
                    CanInteract = false;
                    _propsModel.CollidersActivation(false);
                    break;
                }
            }

            _currentDecreaseStep += stepsAmount;

            StartCoroutine(nameof(RestoreRoutine));
        }

        protected override bool IsDead() => _currentDecreaseStep >= _landscapeParts.Length;

        protected override void Restore()
        {
            foreach (LandscapePart landscapePart in _landscapeParts)
                landscapePart.StepParts.ForEach(part => part.SetActive(true));

            transform.DOScale(_scale, _restoreScaleDelay).From(0f).SetEase(Ease.OutBack).Play();
        }
    }

    [Serializable]
    internal class LandscapePart
    {
        [SerializeField] private List<GameObject> _stepParts;

        public List<GameObject> StepParts => _stepParts;
    }
}