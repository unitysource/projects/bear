using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.EntitiesStuff;
using _Project.Scripts.FactoriesStuff;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using ModestTree;
using UnityEngine;

namespace _Project.Scripts.ResourcesStuff
{
    public class GameResourcesService
    {
        private const float maxAngle = 360f;
        private readonly RandomService _randomService;
        private readonly IResourcesFactory _poolFactory;
        private readonly WorldSettings _worldSettings;

        private Dictionary<string, Stack<Vector3>> _spawnedPropses;

        public GameResourcesService(IResourcesFactory poolFactory, RandomService randomService,
            DataService dataService)
        {
            _randomService = randomService;
            _poolFactory = poolFactory;
            _worldSettings = dataService.WorldSettings;

            _poolFactory.Load();
            _poolFactory.WarmPool();

            InitSpawnedPropses();
        }

        public void AnimateFlyLoop(GameResourceType gameResourceType, Transform from, Transform at, float _maxOffset,
            MinMaxFloat _yOffset, MinMaxFloat _flyDuration, int loopsAmount)
        {
            for (int i = 0; i < loopsAmount; i++)
                AnimateFly(gameResourceType, from, at, _maxOffset, _yOffset, _flyDuration);
        }

        public void AnimateFly(GameResourceType resourceType, Transform from, Transform at, float _maxOffset,
            MinMaxFloat _yOffset, MinMaxFloat _flyDuration)
        {
            // var startPosition = from.position;
            var gameResource = _poolFactory.Create(resourceType.ToString()).GetComponent<Resource>();
            gameResource.gameObject.SetActive(true);
            var trans = gameResource.transform;
            var resourceModel = gameResource.Model;

            trans.position = from.position;
            trans.up = from.up;

            var offset = _randomService.GetOffsetFor3Axis(
                new MinMaxFloat(-_maxOffset, _maxOffset),
                _yOffset,
                new MinMaxFloat(-_maxOffset, _maxOffset));

            float flyDelay = _randomService.GetValue(_flyDuration.MinValue, _flyDuration.MaxValue);
            float flyUpDelay = flyDelay / 2;

            var startRotation = _randomService.GetOffsetFor3Axis(new MinMaxFloat(maxAngle));
            var endRotation = _randomService.GetOffsetFor3Axis(new MinMaxFloat(maxAngle));

            Tween flyUpTween = resourceModel.DOLocalMove(offset, flyUpDelay).SetEase(Ease.OutQuart);
            Tween rotationTween = resourceModel.DOLocalRotate(endRotation, flyDelay).From(startRotation)
                .SetEase(Ease.InOutSine);
            Tween flyToHeroTween = resourceModel.DOLocalMove(Vector3.zero, flyDelay / 3).SetEase(Ease.InQuart);
            Tween flyToHeroTweenMain = trans.DOLocalMove(Vector3.zero, flyDelay / 3).SetEase(Ease.InQuart);

            rotationTween.Play();
            flyUpTween.Play().onComplete += () =>
            {
                trans.SetParent(at);
                flyToHeroTween.Play();
                flyToHeroTweenMain.Play().onComplete += () =>
                    _poolFactory.Release(gameResource.gameObject, resourceType.ToString());
            };
        }

        public Sprite GetSprite(GameResourceType gameResourceType) => 
            FindPropertyByType(gameResourceType).Sprite;

        public int GetTMPSpriteIndexByResourceType(GameResourceType gameResourceType) => 
            FindPropertyByType(gameResourceType).TMPSpriteIndex;

        public int GetPriorityHUDIndex(GameResourceType gameResourceType) => 
            FindPropertyByType(gameResourceType).HUDPriorityIndex;

        public List<GameResourceType> GetPriorityHUDResources()
        {
            List<GameResourceType> list = new List<GameResourceType>();
            var values = Enum.GetValues(typeof(GameResourceType)).Cast<GameResourceType>().ToList();
            values.Remove(GameResourceType.None);
            for (int i = 0; i < values.Count; i++) 
                list.Add(FindPropertyByHUDPriority(i).GameResourceType);

            return list;
        }

        private ResourceSpriteProperty FindPropertyByType(GameResourceType gameResourceType)
        {
            foreach (ResourceSpriteProperty resourceSpriteProperty in _worldSettings.ResourceSpriteProperties)
                if (resourceSpriteProperty.GameResourceType == gameResourceType)
                    return resourceSpriteProperty;
            
            throw new ArgumentOutOfRangeException($"{gameResourceType} isn't correct");
        }

        private ResourceSpriteProperty FindPropertyByHUDPriority(int priorityIndex)
        {
            foreach (ResourceSpriteProperty resourceSpriteProperty in _worldSettings.ResourceSpriteProperties)
                if (resourceSpriteProperty.HUDPriorityIndex == priorityIndex)
                    return resourceSpriteProperty;
            
            throw new ArgumentOutOfRangeException($"{priorityIndex} isn't correct");
        }

        private void InitSpawnedPropses()
        {
            _spawnedPropses = new Dictionary<string, Stack<Vector3>>(10);
            CommonTools.EnumNamesLoopAction(typeof(GameResourceType),
                name =>
                {
                    if (name != GameResourceType.None.ToString()) 
                        _spawnedPropses.Add(name, new Stack<Vector3>(10));
                });
        }

        public void AddSpawnedPropsPosition(IInteractable props, Vector3 point)
        {
            _spawnedPropses[props.GetInteractionResourceType().ToString()].Push(point);
        }

        public Vector3 GetDirectionToNearestProps(GameResourceType resourceType, Vector3 myPosition, out float nearestDistance)
        {
            nearestDistance = 1000f;
            string key = resourceType.ToString();
            Vector3 nearestPropsPosition = _spawnedPropses[key].First();
            foreach (Vector3 propsPosition in _spawnedPropses[key])
            {
                var newDistance = Vector3.Distance(myPosition, propsPosition);
                if (newDistance < nearestDistance)
                {
                    nearestDistance = newDistance;
                    nearestPropsPosition = propsPosition;
                }
            }

            return nearestPropsPosition;
        }

        public bool HaveProps(GameResourceType resourceType) => 
            !_spawnedPropses[resourceType.ToString()].IsEmpty();
    }
}