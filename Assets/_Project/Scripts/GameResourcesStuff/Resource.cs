using UnityEngine;

namespace _Project.Scripts
{
    public class Resource : MonoBehaviour
    {
        [SerializeField] private Transform _model;

        public Transform Model => _model;

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}