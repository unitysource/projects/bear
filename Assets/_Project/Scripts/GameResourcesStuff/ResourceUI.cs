﻿using System;
using System.ComponentModel;
using Assets._Project.Scripts.Utilities;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts
{
    public class ResourceUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text _amountTMP;
        [SerializeField] private Image _image;

        private CanvasGroup _canvasGroup;
        private Vector3 _startPosition;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _startPosition = transform.position;

            Reset();
        }

        public void Reset()
        {
            _canvasGroup.alpha = 0f;
            transform.position = _startPosition;
        }

        public void SetAmount(int amount)
        {
            CommonTools.UpdateText(_amountTMP, $"+{amount}");
        }

        public void SetSprite(Sprite sprite) =>
            _image.sprite = sprite;

        public void Animate(Vector3? from, float yOffset)
        {
            const float animateDuration = 1.2f;
            transform.DOScale(1f, animateDuration).From(0f).SetEase(Ease.OutBack).Play();
            _canvasGroup.DOFade(1f, animateDuration * 0.7f).From(0f).Play().onComplete += () =>
                _canvasGroup.DOFade(0f, animateDuration * 0.3f).Play();
            if (from != null)
                transform.DOLocalMoveY(from.Value.y + yOffset, animateDuration * 0.7f).From(from.Value.y).Play();
            else
                transform.DOLocalMoveY(_startPosition.y + yOffset, animateDuration * 0.7f)
                    .From(_startPosition.y).Play();
        }
        
        public void SetSpriteColor(Color color)
        {
            _image.color = color;
        }
    }
}