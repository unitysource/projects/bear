﻿using UnityEngine;

namespace _Project.Scripts.WorldUIStuff
{
    public class TileWorldUI : MonoBehaviour
    {
        [SerializeField] private Transform _propsBlocksContainer;

        public void AddPropsBlock(PropsBlock propsBlock)
        {
            Transform cashedTransform = propsBlock.transform;
            cashedTransform.SetParent(_propsBlocksContainer);
        }

        public void HideAllBlocks()
        {
            _propsBlocksContainer.gameObject.SetActive(false);
        }
        
        public void ShowAllBlocks()
        {
            _propsBlocksContainer.gameObject.SetActive(true);
        }
    }
}