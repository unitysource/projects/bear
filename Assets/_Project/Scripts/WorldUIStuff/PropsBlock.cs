using System;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using static Assets._Project.Scripts.Utilities.CommonTools;

namespace _Project.Scripts.WorldUIStuff
{
    public class PropsBlock : MonoBehaviour
    {
        [SerializeField] private TMP_Text _currentAmountText;
        [SerializeField] private TMP_Text _requiredAmountText;
        [SerializeField] private Image _propsImage;
        private GameResourceType _gameResourceType;

        // private int _requiredAmount;
        private TileLogic _tileLogic;

        public void Setup(TileLogic tileLogic, GameResourceType gameResourceType)
        {
            _tileLogic = tileLogic;
            _gameResourceType = gameResourceType;
            
            Init();

            _tileLogic.ChangePropsAmount += OnChangePropsAmount;
        }

        private void Init()
        {
            var _requiredAmount = _tileLogic.MaxPropsesValues.GetPropsValueByType(_gameResourceType).Amount;
            UpdateText(_requiredAmountText, $"{_requiredAmount.ToString()}");
            _propsImage.sprite = _tileLogic.GetSpriteByResourceType(_gameResourceType);
        }

        private void OnDestroy()
        {
            _tileLogic.ChangePropsAmount -= OnChangePropsAmount;
        }

        private void OnChangePropsAmount(PropsValue propsValue)
        {
            if (propsValue.GameResourceType == _gameResourceType)
            {
                UpdateAmount(propsValue.Amount);
            }
        }

        public void UpdateAmount(int amount) => UpdateText(_currentAmountText, $"{amount.ToString()}");
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}