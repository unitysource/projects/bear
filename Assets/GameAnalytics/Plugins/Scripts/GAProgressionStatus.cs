namespace GameAnalyticsSDK
{
    public enum GAProgressionStatus
    {
        //Undefined progression
        Undefined = 0,
        // User started progression
        Start = 1,
        // User succesfully ended a progression
        Complete = 2,
        // User failed a progression
        Fail = 3,
        Close,
        Restart
    }
}