﻿using UnityEngine;
using System.Collections;

namespace GameAnalyticsSDK
{
	public enum GAErrorSeverity
	{
		Undefined = 0,
		Debug = 1,
		Info = 2,
		Warning = 3,
		Error = 4,
		Critical = 5
	}
}
