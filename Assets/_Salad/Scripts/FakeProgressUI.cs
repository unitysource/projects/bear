using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Salad.Scripts
{
    public class FakeProgressUI : MonoBehaviour
    {
        [SerializeField] private Image _progressImage;
        [SerializeField] private float _duration;

        private void Start() => _progressImage.DOFillAmount(1f, _duration).From(0f).Play().onComplete += () =>
        {
            // Audio.Instance.PlayMusic();
            SceneManager.LoadScene("Game");
        };
    }
}