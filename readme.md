# Bear (Hyper-Casual Game)

![Game Logo](/images/logo.png)

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)
- [License](#license)

## About the Game

Collect all the artifacts and open new sweet worlds! The game is made using Unity and C#.

![Game Screenshot 1](/images/screenshot1.png)
![Game Screenshot 2](/images/screenshot2.png)

## Gameplay

Bear gameplay is simple. You have to collect all the artifacts and open new sweet worlds! The game is made using Unity and C#.


![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:
- Feature 1: 3D graphics in shape of low poly.
- Feature 2: Physics based gameplay with a lot of fun around spheres.
- Feature 3: Easy to play. You can start playing right away.

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/bear`
2. Navigate to the game directory: `cd bear`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Swipe left/right to move.
- Tap to jump.

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
